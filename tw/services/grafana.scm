(define-module (tw services grafana)
  #:use-module (gnu)
  #:use-module ((gnu packages admin) #:select (shadow))
  #:use-module (gnu services)
  #:use-module (gnu services configuration)
  #:use-module (gnu services databases)
  #:use-module (guix records)
  #:use-module (tw services web)
  #:use-module (tw services docker)
  #:export (grafana-service-type
            grafana-configuration))

;; TODO: Mimir for long-term Prometheus metrics storage?
;; TODO: Store Grafana data in Postgres instead of SQLite?

(define %grafana-user "grafana")
(define %grafana-uid 472)  ; to match container

(define-configuration/no-serialization grafana-configuration
  ;; https://hub.docker.com/r/grafana/grafana-oss/tags
  (container (string "docker.io/grafana/grafana-oss:11.5.2") "Container image to run.")
  (domain (string "localhost") "The external domain which will resolve to this
Grafana instance.")
  (bind-address (string "127.0.0.1") "The host IP to bind to.")
  (host-port (integer 3000) "The port to bind to on the host.")
  (data-path (string "/var/lib/grafana") "The path to store data in, on the host.")
  (metrics-credentials-file (string "/etc/grafana/metrics-credentials") "The
file name containing the user name and password to use for basic
authentication to Grafana's metrics endpoint.  These are specified as the
GF_METRICS_BASIC_AUTH_USERNAME and GF_METRICS_BASIC_AUTH_PASSWORD environment
variables."))

(define (grafana-accounts config)
  (list (user-account
         (name %grafana-user)
         (uid %grafana-uid)
         (group "nogroup")
         (comment "Grafana server user")
         (system? #t)
         (home-directory (grafana-configuration-data-path config))
         (shell (file-append shadow "/sbin/nologin")))))

(define (grafana-environment config)
  (match-record config <grafana-configuration> (domain bind-address host-port)
    (plain-file "grafana.env" (string-append "\
# https://grafana.com/docs/grafana/latest/setup-grafana/configure-docker/
GF_SERVER_PROTOCOL=http
GF_SERVER_DOMAIN=" domain "
GF_SERVER_ROOT_URL=https://" domain "/
GF_SERVER_HTTP_ADDR=" bind-address "
GF_SERVER_HTTP_PORT=" (number->string host-port) "
# gzip compression is recommended by docs, but is not the default
GF_SERVER_ENABLE_GZIP=true
GF_ANALYTICS_REPORTING_ENABLED=false
GF_ANALYTICS_CHECK_FOR_UPDATES=false
GF_ANALYTICS_CHECK_FOR_PLUGIN_UPDATES=false
# disable publishing dashboard snapshots to the internet
GF_SNAPSHOTS_ENABLED=false
# use sensible date and time formats
GF_DATE_FORMATS_INTERVAL_HOUR=DD.MM. HH:mm
GF_DATE_FORMATS_INTERVAL_DAY=DD.MM.
# TODO: https://grafana.com/docs/grafana/latest/setup-grafana/configure-grafana/#smtp
"))))

(define (grafana-docker-service config)
  (match-record config <grafana-configuration> (container data-path metrics-credentials-file)
    (list (docker-container-configuration
           (name "grafana")
           (user %grafana-user)
           (image container)
           (volumes `((,data-path "/var/lib/grafana" #t)
                      ;; The host's Postgres database only allows connections via its socket.
                      ("/var/run/postgresql" "/var/run/postgresql" #t)))
           (environment-files
            (list (grafana-environment config) metrics-credentials-file))
           (network-type "host")))))

(define (grafana-reverse-proxy config)
  (match-record config <grafana-configuration> (domain bind-address host-port)
    (if (string=? domain "localhost") (list)
        (list (https-reverse-proxy-configuration
               (domains (list domain))
               (destination-port host-port)
               (destination-ip
                (if (string=? bind-address "0.0.0.0")
                    "127.0.0.1"
                    bind-address))
               (websocket-uri "/api/live/"))))))

(define grafana-service-type
  (service-type
   (name 'grafana)
   (extensions
    (list (service-extension docker-container-service-type grafana-docker-service)
          (service-extension account-service-type grafana-accounts)
          (service-extension https-reverse-proxy-service-type grafana-reverse-proxy)))
   (default-value (grafana-configuration))
   (description "Grafana server, running under Docker.")))
