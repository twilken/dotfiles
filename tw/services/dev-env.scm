(define-module (tw services dev-env)
  #:use-module (gnu)
  #:use-module (gnu home services)
  #:use-module (gnu home services shepherd)
  #:use-module ((gnu packages commencement) #:select (gcc-toolchain))
  #:use-module ((gnu packages databases) #:select (emacs-rec-mode))
  #:use-module ((gnu packages elf) #:select (patchelf elfutils))
  #:use-module (gnu packages emacs)
  #:use-module (gnu packages emacs-xyz)
  #:use-module ((gnu packages finance) #:select (emacs-ledger-mode))
  #:use-module ((gnu packages haskell-apps) #:select (shellcheck))
  #:use-module ((gnu packages llvm) #:select (clang))
  #:use-module ((gnu packages python-xyz) #:select (python-lsp-server python-yamllint))
  #:use-module ((gnu packages sqlite) #:select (sqlite))
  #:use-module (gnu packages tree-sitter)
  #:use-module (gnu packages vim)
  #:use-module (gnu services configuration)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module ((tw packages alice) #:select (emacs-alidist-mode))
  #:use-module ((tw theme) #:select (catppuccin-vim)))



(define (vim-environment config)
  '(("EDITOR" . "vim")))   ; we define no ASYNC_EDITOR

(define (vim-xdg-config config)
  `((".vim/vimrc" ,(local-file "files/vimrc"))
    (".vim/catppuccin.vim" ,catppuccin-vim)))

;; In non-graphical environments, install vim as an editor.  Neovim might be
;; better, but doesn't have an equivalent to `vim-surround' packaged.
(define (vim-packages config)
  (list vim vim-surround))

(define (emacs-environment config)
  '(("EDITOR" . "emacsclient -qc")
    ;; Tell emacsclient to return immediately after opening the file.  I
    ;; can't put this in $EDITOR as many programs expect $EDITOR to exit
    ;; only when the user is done editing.
    ("ASYNC_EDITOR" . "emacsclient -qcn")))

(define (emacs-xdg-config config)
  `(("emacs/include" ,(local-file "files/emacs-packages" #:recursive? #t))
    ("emacs/init.el" ,(local-file "files/emacs-init.el"))))

(define (emacs-files config)
  `((".local/share/applications/emacsclient.desktop"
     ,(local-file "files/emacsclient.desktop"))))

(define (emacs-daemon config)
  (list (shepherd-service
         (documentation "Emacs server; connect using emacsclient.")
         (provision '(emacs))
         (start #~(make-forkexec-constructor
                   (list #$(file-append emacs-pgtk "/bin/emacs") "--fg-daemon")))
         (stop #~(make-kill-destructor)))))

(define (emacs-packages config)
  (list
   ;; Development & language servers
   gnu-make python-lsp-server python-yamllint shellcheck gcc-toolchain binutils patchelf elfutils
   clang           ; for clangd
   glibc           ; for ldd

   ;; Emacs general
   emacs-pgtk  ; for Wayland and better fractional scaling support
   emacs-use-package
   emacs-gcmh
   emacs-eglot
   emacs-counsel
   emacs-counsel-dash sqlite ; emacs-counsel-dash requires the sqlite3 binary
   emacs-ivy
   emacs-company emacs-company-quickhelp emacs-company-posframe
   emacs-undo-tree
   emacs-aggressive-indent
   emacs-which-key
   emacs-smart-mode-line
   emacs-diminish
   emacs-rainbow-mode
   emacs-form-feed
   emacs-guix
   emacs-org emacs-org-modern
   emacs-catppuccin-theme

   ;; Emacs Evil
   emacs-evil
   emacs-evil-collection
   emacs-evil-expat ; for :reverse, :remove, :rename, :colo, :g*, ... ex commands
   emacs-evil-surround
   emacs-evil-args
   emacs-evil-numbers
   emacs-evil-multiedit
   emacs-evil-goggles
   emacs-evil-traces
   emacs-evil-commentary
   emacs-evil-replace-with-register
   emacs-evil-cleverparens
   emacs-evil-org
   emacs-evil-markdown
   emacs-evil-tex
   emacs-evil-text-object-python

   ;; Emacs language modes
   emacs-flymake-collection emacs-flymake-guile
   emacs-geiser emacs-geiser-guile
   emacs-sly
   emacs-alidist-mode
   emacs-gnuplot
   emacs-graphviz-dot-mode
   emacs-haskell-mode
   emacs-hcl-mode
   emacs-ledger-mode
   emacs-mmm-mode
   emacs-puppet-mode
   emacs-rec-mode
   emacs-vcard-mode
   emacs-web-mode
   emacs-yaml-mode

   ;; Tree sitter libraries, for Emacs' built-in X-ts-modes.
   tree-sitter-bash
   tree-sitter-c
   tree-sitter-cmake
   tree-sitter-cpp
   tree-sitter-css
   tree-sitter-dockerfile
   tree-sitter-javascript
   tree-sitter-json
   tree-sitter-python
   tree-sitter-ruby))

(define-public home-basic-dev-env-service-type
  (service-type
   (name 'basic-dev-env)
   (description "Set up a basic development environment using vim.")
   (default-value #f)
   (extensions
    (list (service-extension home-profile-service-type vim-packages)
          (service-extension home-xdg-configuration-files-service-type vim-xdg-config)
          (service-extension home-environment-variables-service-type vim-environment)))))

(define-public home-full-dev-env-service-type
  (service-type
   (name 'full-dev-env)
   (description "Set up a full-featured, interactive development environment using Emacs.")
   (default-value #f)
   (extensions
    (list (service-extension home-profile-service-type emacs-packages)
          (service-extension home-xdg-configuration-files-service-type emacs-xdg-config)
          (service-extension home-files-service-type emacs-files)
          (service-extension home-environment-variables-service-type emacs-environment)
          (service-extension home-shepherd-service-type emacs-daemon)))))
