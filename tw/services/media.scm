(define-module (tw services media)
  #:use-module (gnu)
  #:use-module (gnu packages certs)
  #:use-module (gnu packages python)
  #:use-module (gnu packages video)
  #:use-module (gnu services)
  #:use-module (gnu services configuration)
  #:use-module (gnu services shepherd)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix records)
  #:export (yt-dlp-service-type
            yt-dlp-configuration
            get-iplayer-service-type
            get-iplayer-configuration))

(define (package-or-false? value)
  (or (package? value) (eq? #f value)))

(define (random-minute hour)
  #~(calendar-event #:hours '(#$hour) #:minutes '(#$(random 60))))



(define-configuration/no-serialization yt-dlp-configuration
  (media-directory string "The directory in which to store downloaded media.
The service expects a @code{.yt-dlp} config directory inside this one.")
  (yt-dlp (package-or-false yt-dlp) "The yt-dlp package to use; or
@code{#false} to use the yt-dlp script inside the config directory.")
  (schedule (gexp (random-minute 4)) "The timer schedule on which to run the
download script.  By default, picks a random time between 04:00 and 05:00
every night.")
  (user (string "root") "The Unix user name to run the script as."))

(define (yt-dlp-cronjob config)
  (match-record config <yt-dlp-configuration> (media-directory yt-dlp schedule user)
    (let ((executable
           (if yt-dlp (file-append yt-dlp "/bin/yt-dlp")
               (program-file "yt-dlp-command"
                 #~(let ((executable #$(string-append media-directory "/.yt-dlp/yt-dlp")))
                     ;; Auto-update if we're using yt-dlp from the media directory.
                     (system* #$(file-append python-3 "/bin/python3")
                              executable "--ignore-config" "--update")
                     (apply execl #$(file-append python-3 "/bin/python3")
                            "python3" executable (cdr (command-line))))))))

      (list (shepherd-service
             (provision '(yt-dlp))
             (requirement '(user-processes file-systems networking))
             (documentation "Download newest videos from YouTube.")
             (modules '((shepherd service timer)))
             (start #~(make-timer-constructor
                       #$schedule
                       (command (list #$executable "--ignore-config" "--config-location" ".yt-dlp")
                                #:user #$user #:directory #$media-directory #:environment-variables
                                (cons* (format #f "SSL_CERT_DIR=~a/etc/ssl/certs" #$nss-certs)
                                       (default-environment-variables)))))
             (stop #~(make-timer-destructor))
             (actions (list (shepherd-action
                             (name 'trigger) (procedure #~trigger-timer)
                             (documentation "Download videos now.")))))))))

(define yt-dlp-service-type
  (service-type
   (name 'yt-dlp)
   (extensions
    (list (service-extension shepherd-root-service-type yt-dlp-cronjob)))
   (description
    "Trigger yt-dlp on a schedule to download videos from YouTube.")))



(define-configuration/no-serialization get-iplayer-configuration
  (config-directory string "The directory in which to look for configuration files.")
  (get-iplayer (package get-iplayer) "The get-iplayer package to use.")
  (schedule (gexp (random-minute 1)) "The timer schedule on which to run the
download script.  By default, picks a random time between 01:00 and 02:00
every night.")
  (user (string "root") "The Unix user name to run the script as."))

(define (get-iplayer-cronjob config)
  (match-record config <get-iplayer-configuration> (config-directory get-iplayer schedule user)
    (list (shepherd-service
           (provision '(get-iplayer))
           (requirement '(user-processes file-systems networking))
           (documentation "Download newest episodes from BBC Sounds.")
           (modules '((shepherd service timer)))
           (start #~(make-timer-constructor
                     #$schedule
                     (command (list #$(file-append get-iplayer "/bin/get_iplayer")
                                    "--pvr" "--profile-dir" #$config-directory)
                              #:user #$user #:environment-variables
                              (cons* (format #f "SSL_CERT_DIR=~a/etc/ssl/certs" #$nss-certs)
                                     (default-environment-variables)))))
           (stop #~(make-timer-destructor))
           (actions (list (shepherd-action
                           (name 'trigger) (procedure #~trigger-timer)
                           (documentation "Download episodes now."))))))))

(define get-iplayer-service-type
  (service-type
   (name 'get-iplayer)
   (extensions
    (list (service-extension shepherd-root-service-type get-iplayer-cronjob)))
   (description
    "Trigger get_iplayer on a schedule to download radio shows from the BBC.")))
