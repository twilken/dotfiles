(define-module (tw services matrix)
  #:use-module (gnu services)
  #:use-module (tw services web))

(define-public %matrix-services
  (list (simple-service 'synapse-reverse-proxy https-reverse-proxy-service-type
          ;; Synapse can't access certbot certs, but nginx can, so proxy HTTPS
          ;; access through. Also, it's good to have Synapse available on :443.
          (list (https-reverse-proxy-configuration
                 (domains '("matrix.twilken.net"))
                 (destination-port 48448))))

        ;; TODO: Postgres for Synapse
        ;; (service postgresql-service-type
        ;;   (postgresql-configuration
        ;;    (postgresql postgresql-15)
        ;;    (data-directory "/var/lib/postgresql/data")))

        ;; (service postgresql-role-service-type
        ;;   (postgresql-role-configuration
        ;;    (roles (list (postgresql-role
        ;;                  (name "synapse")  ; TODO
        ;;                  (create-database? #t))))))

        ;; TODO: Matrix/Synapse
        ;; TODO: Matrix bridges
        ))
