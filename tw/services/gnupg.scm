(define-module (tw services gnupg)
  #:use-module (gnu)
  #:use-module (gnu home services)
  #:use-module (gnu home services shepherd)
  #:use-module (gnu packages gnupg)
  #:use-module ((gnu packages image-viewers)
                #:select (imv))
  #:use-module (gnu services configuration)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module ((guix records) #:select (match-record))
  #:export (home-gnupg-configuration
            home-gnupg-service-type))

(define-configuration/no-serialization home-gnupg-configuration
  (default-key string "The user's own key.  Always encrypt to this key, and
use it by default.")
  (gui-pinentry? boolean "Use pinentry-rofi if true, else pinentry-tty.")
  (keyserver (string "hkps://keys.openpgp.org") "The default keyserver to use.")
  (gnupg (package gnupg) "The GnuPG package to use.")
  (image-viewer (file-like (file-append imv "/bin/imv")) "A gexp returning a
string, specifying the command to call in order to view images.")
  (gnupghome (string "$XDG_DATA_HOME/gnupg") "The value of $GNUPGHOME in the
environment."))

(define (gnupg-xdg config)
  `(("GNUPGHOME" . ,(home-gnupg-configuration-gnupghome config))))

(define (gnupg-files config)
  (match-record config <home-gnupg-configuration>
                (default-key gui-pinentry? keyserver image-viewer)
    `(;; GnuPG config files must be in ~/.local/share/gnupg, not ~/.config,
      ;; so we can't use `home-xdg-configuration-files-service-type'.
      (".local/share/gnupg/dirmngr.conf"
       ,(mixed-text-file "dirmngr.conf"
          "keyserver " keyserver "\n"))
      (".local/share/gnupg/gpg.conf"
       ,(mixed-text-file "gpg.conf" "\
# This options file can contain any long options to GnuPG.
# See the gpg man page for a list of options.
# Comments can only be at the start of a line, not after options.

default-key " default-key "
default-recipient-self
use-agent
# Get rid of the copyright notice.
no-greeting
# Always encrypt to my key as well, in addition to any recipient.
encrypt-to " default-key "
auto-key-import
auto-key-retrieve
photo-viewer \"" image-viewer " %i\"

# Because some mailers change lines starting with 'From ' to '>From '
# it is good to handle such lines in a special way when creating
# cleartext signatures; all other PGP versions do it this way too.
# To enable full OpenPGP compliance you may want to use this option.
#no-escape-from-lines
"))
      (".local/share/gnupg/gpg-agent.conf"
       ,(mixed-text-file "gpg-agent.conf" "\
pinentry-program " (if gui-pinentry?
                       (file-append pinentry-rofi "/bin/pinentry-rofi")
                       (file-append pinentry-tty "/bin/pinentry-tty")) "
# Keep passphrase cached for longer, so that Shepherd timers (e.g. restic,
# vdirsyncer) can access the password store. Vdirsyncer should run every half
# hour to extend the default-cache-ttl.
default-cache-ttl 2100  # 35 min
max-cache-ttl 43200     # 12 h
# Needed if spawning lots of parallel gpg --decrypt processes. https://dev.gnupg.org/T3530
auto-expand-secmem
")))))

(define (gnupg-shepherd config)
  (match-record config <home-gnupg-configuration> (gnupg)
    (list (shepherd-service
           (documentation "GPG agent; caches key passwords.")
           (provision '(gpg-agent))
           (start #~(lambda _
                      (invoke #$(file-append gnupg "/bin/gpg-agent")
                              "--daemon" "--no-detach")))
           (stop #~(lambda _
                     (invoke #$(file-append gnupg "/bin/gpg-connect-agent")
                             "killagent" "/bye")))))))

(define home-gnupg-service-type
  (service-type
   (name 'gnupg)
   (extensions
    (list (service-extension home-shepherd-service-type gnupg-shepherd)
          (service-extension home-files-service-type gnupg-files)
          (service-extension home-environment-variables-service-type gnupg-xdg)))
   (description
    "Install GnuPG configuration files and run the agent.")))
