(define-generic-mode 'ifm-mode
  '("#")
  '("title" "map" "require" "room" "join" "to" "dir" "exit" "go" "oneway"
    "tag" "from" "link" "nolink" "item" "in" "note" "score" "need" "after"
    "before" "leave" "all" "except" "cmd" "length" "start" "finish" "nodrop"
    "nopath" "style" "hidden" "keep" "with" "until" "ignore" "give" "lost"
    "do" "get" "drop" "until" "safe" "ignore" "goto" "endstyle")
  '(("\\<\\(\\(north\\|south\\)\\(east\\|west\\)?\\|[ns][ew]?\\|east\\|west\\|[ew]\\)\\>"
     . 'font-lock-builtin-face)
    ("\\<\\([du]\\|down\\|up\\|in\\|out\\|last\\|it\\|them\\)\\>"
     . 'font-lock-builtin-face)
    ("\\<[0-9]+" . 'font-lock-constant-face)
    ("\\<[_a-zA-Z][_0-9A-Za-z]*\\>" . 'font-lock-variable-name-face))
  '("\\.ifm\\'")
  nil
  "A mode for interactive fiction manager files")

(provide 'ifm-mode)
