;;; environmentd-mode.el --- Major mode for environment.d(5) files.

;;; Commentary:

;; This major mode font-locks files including /etc/environment and
;; ~/.config/environment.d/*.conf. Their format is specified by the
;; environment.d(5) man page.

;;; Code:

(defconst environmentd-mode/font-lock-defaults
  '((("^[[:blank:]]+[^[:blank:]]+" . font-lock-warning-face)   ; stray leading whitespace
     ("^#+[[:blank:]]*" . font-lock-comment-delimiter-face)
     ("^#+[[:blank:]]*\\(.*\\)$" 1 font-lock-comment-face)
     ("\\\\[$\\]" . font-lock-string-face)                       ; escaped $ \
     ("^\\([A-Za-z_][A-Za-z0-9_]*\\)\\(=\\)"
      (1 font-lock-variable-name-face)
      (2 font-lock-keyword-face))
     ("\\(\\${\\)\\([A-Za-z_][A-Za-z0-9_]*\\)\\(:[+-]\\)[^}]*\\(}\\)"
      (1 font-lock-keyword-face)
      (2 font-lock-variable-name-face)
      (3 font-lock-keyword-face)
      (4 font-lock-keyword-face))                  ; ${X:-default}-variable references
     ("\\(\\${\\)\\([A-Za-z_][A-Za-z0-9_]*\\)\\(}\\)"
      (1 font-lock-keyword-face)
      (2 font-lock-variable-name-face)
      (3 font-lock-keyword-face))                  ; ${X}-variable references
     ("\\(\\$\\)\\([A-Za-z_][A-Za-z0-9_]*\\)"
      (1 font-lock-keyword-face)
      (2 font-lock-variable-name-face)))           ; $X-variable references
    t nil ((?\' . "w") (?\" . "w")))
  "Font lock settings for Environment.d mode. See `font-lock-defaults' for documentation.")

(define-derived-mode environmentd-mode prog-mode "Environment.d"
  "Environment.d mode is used for environment.d(5) files."
  (setq-local comment-start "#"
              comment-start-skip "#"
              comment-end ""
              font-lock-defaults environmentd-mode/font-lock-defaults))

(add-to-list 'auto-mode-alist
             '("/environment\\.d/[^/]+\\.conf\\'\\|\\`/etc/environment\\'"
               . environmentd-mode))

(provide 'environmentd-mode)
;;; environmentd-mode.el ends here
