;;; pam-env.el --- Major mode for pam_env.conf(5) files.

;;; Commentary:

;; This major mode font-locks files including ~/.pam_environment and
;; /etc/security/pam_env.conf, but notably not /etc/environment. Their format is
;; specified by the pam_env.conf(5) man page.

;; TODO: Only apply font-lock-variable-name-face to variable declarations if
;; the previous line didn't end with a backslash. The following case didn't
;; work (some declarations that should've been font-locked weren't):
;; '("\\(?:^$\\|[^\\\\]\\)[\r\n]\\([^[:blank:]]+\\)"
;;   1 font-lock-variable-name-face keep)

;; pam_env does not support escaped double quotes ("). Single-quoted strings are
;; not used as string delimiters. We can only match against word chars in
;; `pam-env-mode/font-lock-defaults', so make double quotes word chars.

;;; Code:

(defconst pam-env-mode/font-lock-defaults
  '((("^#+" . font-lock-comment-delimiter-face)
     ("^#+[[:blank:]]*\\(.*\\)$" 1 font-lock-comment-face)
     ("\\\\[@$\\]" . font-lock-string-face)          ; escaped $ @ \
     ("@{[^}]+}" . font-lock-builtin-face)           ; @{}-variable references
     ("\\${[^}]+}" . font-lock-variable-name-face)   ; ${}-variable references
     ("\"[^\"]*\"" 0 font-lock-string-face keep)     ; double-quoted strings; escaped " not supported
     ("\\<\\(DEFAULT\\|OVERRIDE\\)=" . font-lock-keyword-face) ; DEFAULT= and OVERRIDE=
     ("^[^[:blank:]]+" . font-lock-variable-name-face)         ; variable declarations
     ("[[:blank:]]+[^[:blank:]]+" . font-lock-warning-face))   ; stray whitespace
    t nil ((?\' . "w") (?\" . "w")))
  "Font lock settings for PAM-Environment mode. See `font-lock-defaults' for documentation.")

(define-derived-mode pam-env-mode prog-mode "PAM-Environment"
  "PAM-environment mode is used for pam_env.conf(5) files."
  (set (make-local-variable 'comment-start) "#")
  (set (make-local-variable 'comment-start-skip) "^#+[[:blank:]]*")
  (set (make-local-variable 'comment-end) "")
  (set (make-local-variable 'font-lock-defaults) pam-env-mode/font-lock-defaults))

(let ((regexp "\\(\\`\\|/\\)\\(pam_env\\.conf\\|\\.pam_environment\\)\\'"))
  (add-to-list 'auto-mode-alist `(,regexp . pam-env-mode)))

(provide 'pam-env-mode)
;;; pam-env.el ends here
