;;; bemscript-mode.el --- Syntax highlighting for MERRILL BEMScript files.
;;; Commentary:
;;; Based on the MERRILL manual. Some commands may have been missed.
;;; Code:

(defconst bemscript-mode-keywords
  '("set" "setsubdomain" "magnetite" "iron" "tm54" "resize" "cubic anisotropy"
    "uniaxial anisotropy" "cubicrotation" "easy axis" "external field strength"
    "external field direction" "readmesh" "loadmesh" "readmagnetization"
    "uniform magnetization" "randomize magnetization" "randomize all moments"
    "remesh" "conjugategradient" "steepestdescent" "minimize" "energylog"
    "closelogfile" "writeloopdata" "writemagnetization" "writedemag" "writehyst"
    "writeboxdata" "appenddemagzone" "magnetizationtopath" "pathtomagnetization"
    "renewpath" "refinepathto" "writetecplotpath" "readtecplotpath"
    "readtecplotzone" "keypause" "makeinitialpath" "pathminimize" "pathlogfile"
    "systemcommand" "pathstructureenergies" "reportenergy" "stop" "end" "loop"
    "endloop" "define" "addto" "undefine" "generatecubemesh" "zonename")
  "List of keywords for BEMScript mode. Intended for case folding.")

(defconst bemscript-mode-builtins
  '("Aex" "CurvatureWeight" "ExchangeCalculator" "K1" "K2" "Ls" "Ms" "mu"
    "MaxEnergyEvaluations" "MaxPathEvaluations" "MaxRestart" "MaxMeshNumber"
    "NEBSpring" "PathN" "Zone" "ZoneIncrement")
  "List of built-in variable names for BEMScript.")

(defconst bemscript-mode-special
  '("patran" "tecplot" "POINT" "BLOCK" "SD" "muT" "mT" "T" "C")
  "Variables with special meanings and units in BEMScript.")

;; Available font-lock-*-faces: doc type string builtin comment keyword warning
;; constant (reference) preprocessor syntactic-function function-name
;; negation-char variable-name comment-delimiter
(defconst bemscript-mode-font-lock-defaults
  `((;; See font-lock-keywords docs. Earlier lines seem to override later ones,
     ;; except if both have OVERRIDE? t.
     ;; Format: (REGEXP (GROUPNUM FACENAME OVERRIDE? LAXMATCH?)...)
     ;; Comments
     ("!\\(.*\\)" 1 font-lock-comment-face t t)
     ("!" 0 font-lock-comment-delimiter-face t)
     ("!!.*$" 0 font-lock-doc-face t)

     ;; Equals signs need spaces around them.
     ("\\s-=\\s-" 0 font-lock-type-face t)    ; there is no "operator" etc face
     ("=" . font-lock-warning-face)

     ;; Numbers and variables
     ("\\<[0-9]*\\.?[0-9]+\\(e[-+]?\\)?[0-9]*\\>" . font-lock-constant-face)
     ("\\(\\<[#%][A-Z_a-z][0-9A-Z_a-z]*\\>\\|\\$[A-Z_a-z][0-9A-Za-z_]*\\$\\)"
      . font-lock-variable-name-face)

     ;; Preprocessor (&-substitution)
     (,(concat "\\([^&]\\|^\\)\\(&&\\)*"  ; && escapes &
               "\\(&\\([_a-zA-Z][_a-zA-Z0-9]*\\|{[_a-zA-Z][_a-zA-Z0-9]*}\\)\\)")
      3 font-lock-preprocessor-face)
     (,(concat "\\([^&]\\|^\\)\\(&&\\)*"       ; && escapes &
               "\\(&\\($"                      ; bare &
               "\\|[^&{_a-zA-Z]\\|{[^_a-zA-Z]" ; invalid char following & or &{
                                               ; invalid name or unclosed {
               "\\|{[_a-zA-Z][_0-9a-zA-Z]*\\([^_0-9a-zA-Z}]\\|$\\)\\)\\)")
      3 font-lock-warning-face t)

     ;; Variable definitions
     ("\\<\\(loop\\|define\\)\\s-+\\([_a-zA-Z][_a-zA-Z0-9]*\\)\\>"
      2 font-lock-function-name-face)
     ("\\<\\(addto\\|undefine\\)\\s-+\\([_a-zA-Z][_a-zA-Z0-9]*\\)\\>"
      2 font-lock-variable-name-face)

     ;; Keywords
     (,(regexp-opt bemscript-mode-special 'words) . font-lock-string-face)
     (,(regexp-opt bemscript-mode-keywords 'words) . font-lock-keyword-face)
     (,(regexp-opt bemscript-mode-builtins 'words) . font-lock-builtin-face))

    ;; KEYWORDS-ONLY: if t, no syntactic fontification (strings and comments)
    nil
    ;; CASE-FOLD: if t, make keywords case-insensitive.
    t)
  "Font lock settings for BEMScript mode.")

(define-derived-mode bemscript-mode prog-mode "BEMScript"
  "BEMScript-mode is used for editing MERRILL scripts."
  (setq comment-start "!"
        comment-end ""
        tab-width 2
        font-lock-defaults bemscript-mode-font-lock-defaults)
  ;; Only word syntax entries are highlighted; add needed chars.
  (modify-syntax-entry ?# "w")
  ;; Strings in BEMScript are not quoted.
  (modify-syntax-entry ?\" "w"))

(add-to-list 'auto-mode-alist '("\\.bem\\'" . bemscript-mode))
(provide 'bemscript-mode)
;;; bemscript-mode.el ends here
