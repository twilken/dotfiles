font_family Hermit
font_size 10.0

focus_follows_mouse yes

# The value of kitty_mod is used as the modifier for all default shortcuts,
# you can change it in your kitty.conf to change the modifiers for all the
# default shortcuts.
# kitty_mod ctrl+shift

open_url_modifiers shift

# map kitty_mod+up        scroll_line_up
# map kitty_mod+k         scroll_line_up
# map kitty_mod+down      scroll_line_down
# map kitty_mod+j         scroll_line_down
map shift+page_up         scroll_page_up
map shift+page_down       scroll_page_down
map shift+home            scroll_home
map shift+end             scroll_end
# map kitty_mod+h         show_scrollback

# Number of lines of history to keep in memory for scrolling back. Memory is
# allocated on demand. Negative numbers are (effectively) infinite
# scrollback. Note that using very large scrollback is not recommended as it
# can slow down performance of the terminal and also use large amounts of
# RAM. Instead, consider using scrollback_pager_history_size.
scrollback_lines 10000

# Program with which to view scrollback in a new window. The scrollback buffer
# is passed as STDIN to this program. If you change it, make sure the program
# you use can handle ANSI escape sequences for colors and text
# formatting. INPUT_LINE_NUMBER in the command line above will be replaced by
# an integer representing which line should be at the top of the
# screen. Similarly CURSOR_LINE and CURSOR_COLUMN will be replaced by the
# current cursor position.
# scrollback_pager less --chop-long-lines --RAW-CONTROL-CHARS +INPUT_LINE_NUMBER

# Separate scrollback history size, used only for browsing the scrollback
# buffer (in MB). This separate buffer is not available for interactive
# scrolling but will be piped to the pager program when viewing scrollback
# buffer in a separate window. The current implementation stores the data in
# UTF-8, so approximatively 10000 lines per megabyte at 100 chars per line,
# for pure ASCII text, unformatted text. A value of zero or less disables this
# feature. The maximum allowed size is 4GB.
scrollback_pager_history_size 1024

scrollback_fill_enlarged_window yes
