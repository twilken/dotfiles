(define-module (tw services games)
  #:use-module ((gnu packages admin) #:select (shadow))
  #:use-module (gnu services)
  #:use-module (gnu services certbot)
  #:use-module (gnu services configuration)
  #:use-module (gnu services shepherd)
  #:use-module (gnu services web)
  #:use-module (gnu system shadow)
  #:use-module (guix gexp)
  #:use-module (guix records)
  #:use-module (ice-9 format)  ; for ~d format specifier
  #:use-module (tw packages games)
  #:use-module (tw packages monitoring)
  #:use-module (tw services restic)
  #:use-module (tw services secrets)
  #:use-module ((tw services web) #:select (%nginx-cert-deploy-hook))
  #:export (minecraft-server-service-type
            minecraft-server-configuration))

(define-configuration/no-serialization minecraft-server-configuration
  (data-location (string "/var/lib/minecraft-server") "Path on disk where
server data will be stored.")
  (port (integer 25565) "The port to bind the main server to.")
  (map-location (string "/var/lib/mcmap") "Path on disk where map tiles will
be stored.")
  (map-domain string "The domain name where maps are hosted.")
  (metrics-port (integer 26565) "The port to bind the @code{mc-status}
Prometheus exporter to.")
  (user-name (string "minecraft") "UNIX username to create for the server.")
  (group-name (string "minecraft") "UNIX group to create for the server."))

(define (minecraft-server-shepherd config)
  (match-record config <minecraft-server-configuration>
                (data-location port map-location metrics-port user-name group-name)
    (list (shepherd-service
           (provision '(minecraft-server))
           (requirement '(networking))
           (documentation "Run a Minecraft server.")
           (start #~(make-forkexec-constructor
                     (list #$(file-append minecraft-server "/bin/minecraft-server")
                           "--nogui" "--port" #$(number->string port))
                     #:user #$user-name #:group #$group-name
                     #:directory #$data-location))
           (stop #~(make-kill-destructor)))

          (shepherd-service
           (provision '(mc-monitor))
           (requirement '(networking minecraft-server))
           (documentation "Monitor the Minecraft server and export results for Prometheus.")
           (start #~(make-forkexec-constructor
                     (list #$(file-append mc-monitor "/bin/mc-monitor") "export-for-prometheus"
                           ;; The metrics are public info, so no need to bind to WireGuard interface only.
                           "-port" #$(number->string metrics-port)
                           "-servers" #$(format #f "127.0.0.1:~d" port))
                     #:user #$user-name #:group #$group-name))
           (stop #~(make-kill-destructor)))

          (shepherd-service
           (provision '(mcmap))
           (requirement '(user-processes file-systems))
           (documentation "Regenerate the Minecraft overview map.")
           (modules '((shepherd service timer)))
           (start #~(make-timer-constructor
                     (calendar-event #:hours '(5) #:minutes '(47))
                     (command (list #$(file-append mcmap "/bin/mcmap")
                                    "-centre" "0" "0" "-radius" "8192"
                                    "-tile" "1024" "-file" #$map-location
                                    (string-append #$data-location "/world"))
                              #:user #$user-name #:group #$group-name)
                     #:wait-for-termination? #t
                     #:max-duration (* 4 60 60)))
           (stop #~(make-timer-destructor))
           (actions (list (shepherd-action (name 'trigger) (procedure #~trigger-timer)
                                           (documentation "Regenerate the map tiles now."))))))))

(define (minecraft-server-activation config)
  (match-record config <minecraft-server-configuration>
                (data-location map-location user-name group-name)
    #~(begin
        (with-output-to-file (string-append #$data-location "/eula.txt")
          (lambda () (display "eula=true") (newline)))
        ;; Set up `map-location'.
        (or (false-if-exception (mkdir #$map-location #o755))
            (directory-exists? #$map-location))
        (chown #$map-location (passwd:uid (getpw #$user-name)) (group:gid (getgr #$group-name)))
        (false-if-exception   ; continue if file doesn't exist
         (delete-file (string-append #$map-location "/index.html")))
        (symlink #$(local-file "files/mcmap-index.html")
                 (string-append #$map-location "/index.html")))))

(define (minecraft-server-accounts config)
  (match-record config <minecraft-server-configuration> (data-location user-name group-name)
    (list (user-account
           (name user-name)
           (group group-name)
           (comment "Minecraft server user")
           (system? #t)
           (home-directory data-location)
           (shell (file-append shadow "/sbin/nologin")))
          (user-group
           (name group-name)
           (system? #t)))))

(define (mcmap-nginx-server config)
  (match-record config <minecraft-server-configuration> (map-location map-domain)
    (list (nginx-server-configuration
           ;; The certbot service redirects everything on port 80 to
           ;; port 443 by default, modulo its own /.well-known paths.
           (listen '("443 ssl"))
           (server-name (list map-domain))
           (root map-location)
           (index '("index.html"))
           (raw-content '("http2 on;"))
           (ssl-certificate (string-append "/etc/certs/" map-domain "/fullchain.pem"))
           (ssl-certificate-key (string-append "/etc/certs/" map-domain "/privkey.pem"))
           (server-tokens? #f)))))

(define (mcmap-certificate config)
  (match-record config <minecraft-server-configuration> (map-domain)
    (list (certificate-configuration
           (domains (list map-domain))
           (deploy-hook %nginx-cert-deploy-hook)))))

(define minecraft-backup-repository
  (restic-local-repository
   (path "/var/backups/minecraft")))

(define minecraft-backup-password-file
  "/etc/restic/lud-minecraft")

(define minecraft-backup-password
  (restic-password-source
   (type 'file)
   (name minecraft-backup-password-file)))

(define (minecraft-backup-secrets config)
  (list (secret
         (encrypted-file (local-file "../system/files/restic/lud-minecraft.enc"))
         (destination minecraft-backup-password-file))))

(define (minecraft-restic-backups config)
  (match-record config <minecraft-server-configuration> (data-location)
    (list (restic-scheduled-backup
           (schedule #~(calendar-event #:hours '(3) #:minutes '(30)))
           (paths (list data-location))
           (repo minecraft-backup-repository)
           (password minecraft-backup-password)))))

(define (minecraft-backup-cleanups config)
  (list (restic-scheduled-cleanup
         (schedule #~(calendar-event #:hours '(4) #:minutes '(30)))
         (repo minecraft-backup-repository)
         (password minecraft-backup-password)
         (keep-daily 30)
         (keep-monthly -1))))

(define minecraft-server-service-type
  (service-type
   (name 'minecraft-server)
   (extensions (list (service-extension shepherd-root-service-type minecraft-server-shepherd)
                     (service-extension activation-service-type minecraft-server-activation)
                     (service-extension account-service-type minecraft-server-accounts)
                     (service-extension nginx-service-type mcmap-nginx-server)
                     (service-extension certbot-service-type mcmap-certificate)
                     (service-extension secrets-service-type minecraft-backup-secrets)
                     (service-extension restic-backup-service-type minecraft-restic-backups)
                     (service-extension restic-cleanup-service-type minecraft-backup-cleanups)))
   (description "Run a Minecraft server.")))
