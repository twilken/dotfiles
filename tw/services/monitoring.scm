(define-module (tw services monitoring)
  #:use-module (gnu services)
  #:use-module (gnu services configuration)
  #:use-module (gnu services shepherd)
  #:use-module (guix gexp)
  #:use-module (guix records)
  #:use-module (tw packages monitoring)
  #:export (syncthing-exporter-service-type
            syncthing-exporter-configuration))

(define-configuration/no-serialization syncthing-exporter-configuration
  (user-name (string "syncthing") "The local user name to run the exporter as.
This user must have access to Syncthing's configuration, since the API key and
possibly folder IDs are scraped from this file.")
  (group-name (string "syncthing") "The local UNIX group to run as.")
  (listen-address (string ":9093") "The network interface (IP and port) to
which the exporter should bind.")
  (syncthing-uri (string "http://127.0.0.1:8384") "URI where Syncthing's GUI
is hosted.  The API key to access the GUI will be parsed from Syncthing's
configuration.")
  (folder-stats? (boolean #t) "Should Syncthing's configuration be searched
for folder definitions before startup, and the exporter told to expose
folder-specific metrics as well?  This has a performance impact."))

(define (syncthing-exporter-wrapper config)
  (match-record config <syncthing-exporter-configuration>
                (user-name listen-address syncthing-uri folder-stats?)
    (program-file "syncthing-exporter-wrapper"
      #~(begin
          (use-modules ((ice-9 rdelim) #:select (read-line))
                       ((srfi srfi-1) #:select (find))
                       ((srfi srfi-26) #:select (cut))
                       ((sxml simple) #:select (xml->sxml))
                       ((sxml match) #:select (sxml-match)))
          (define syncthing-config-path
            (format #f "~a/syncthing/config.xml"
                    (or (getenv "XDG_STATE_HOME")
                        (string-append (passwd:dir (getpw #$user-name)) "/.local/state"))))
          (define syncthing-config
            (call-with-input-file syncthing-config-path
              (cut xml->sxml <> #:trim-whitespace? #t)))
          (setenv "SYNCTHING_TOKEN"
                  (sxml-match syncthing-config
                    ((*TOP* (configuration ,(settings) ...))
                     (find string? settings))      ; find the <gui> tag
                    ((gui ,(gui-settings) ...)
                     (find string? gui-settings))  ; find the <apikey> tag
                    ((apikey ,api-key) api-key)    ; extract API key
                    (,other #f)))   ; handle any tags not described above
          (when #$folder-stats?
            (setenv "SYNCTHING_FOLDERSID"
                    (sxml-match syncthing-config
                      ((*TOP* (configuration ,(folder-ids) ...))
                       ;; Remove #f's from non-<folder> tags and join folder IDs.
                       (string-join (filter string? folder-ids) ","))
                      ((folder (@ (id ,folder-id)) . ,rest)
                       folder-id)
                      (,other #f))))
          (execl #$(file-append syncthing-exporter "/bin/syncthing_exporter") "syncthing_exporter"
                 (string-append "--web.listen-address=" #$listen-address)
                 (string-append "--syncthing.uri=" #$syncthing-uri))))))

(define (syncthing-exporter-daemon config)
  (match-record config <syncthing-exporter-configuration> (user-name group-name)
    (list (shepherd-service
           (provision '(syncthing-exporter))
           (requirement '(networking))   ; syncthing need not be running for this to start
           (documentation "Monitor Syncthing and export results for Prometheus.")
           (start #~(make-forkexec-constructor
                     (list #$(syncthing-exporter-wrapper config))
                     #:user #$user-name #:group #$group-name))
           (stop #~(make-kill-destructor))))))

(define syncthing-exporter-service-type
  (service-type
   (name 'syncthing-exporter)
   (extensions (list (service-extension shepherd-root-service-type syncthing-exporter-daemon)))
   (default-value (syncthing-exporter-configuration))
   (description "Monitor Syncthing and export results for Prometheus.")))
