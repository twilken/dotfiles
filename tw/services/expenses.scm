(define-module (tw services expenses)
  #:use-module (gnu)
  #:use-module ((gnu packages admin) #:select (shadow))
  #:use-module ((gnu packages certs) #:select (nss-certs))
  #:use-module (gnu services)
  #:use-module (gnu services configuration)
  #:use-module (gnu services shepherd)
  #:use-module (guix packages)
  #:use-module (guix records)
  #:use-module (tw packages finance)
  #:use-module (tw services web)
  #:export (expenses-service-type
            expenses-configuration))

(define-configuration/no-serialization expenses-configuration
  (expenses (package sbcl-expenses) "The Expenses package to use.")
  (domain (string "localhost") "The external domain which will resolve to this
web app instance.")
  (bind-address (string "::1") "The IP address to bind the web server to.")
  (port (integer 5000) "The port to bind the web server to.")
  (data-path (string "/var/lib/expenses") "The directory to store data.  The
service will create a user to own and manage this directory."))

(define (expenses-accounts config)
  (list (user-account
         (name "expenses")
         (group "nogroup")
         (comment "Expenses web app user")
         (system? #t)
         (home-directory (expenses-configuration-data-path config))
         (shell (file-append shadow "/sbin/nologin")))))

(define (expenses-shepherd-service config)
  (match-record config <expenses-configuration> (expenses data-path bind-address port)
    (list (shepherd-service
           (provision '(expenses))
           (requirement '(user-processes file-systems networking))
           (documentation "Run the Expenses web app server.")
           (start #~(make-forkexec-constructor
                     (list #$(file-append expenses "/bin/expenses")
                           "-f" #$(string-append data-path "/expenses.db")
                           "-a" #$bind-address "-p" #$(number->string port))
                     #:user "expenses" #:group "nogroup" #:directory #$data-path))
           (stop #~(make-kill-destructor)))

          (shepherd-service
           (provision '(expenses-exchange-rates))
           (requirement '(user-processes file-systems networking))
           (documentation "Update exchange rates in Expenses' database.")
           (modules '((shepherd service timer)))
           (start #~(make-timer-constructor
                     ;; Run after markets close on weekdays. There is no data on weekends.
                     (calendar-event #:hours '(22) #:minutes '(35)
                                     #:days-of-week '(monday tuesday wednesday thursday friday))
                     (command (list #$(file-append expenses "/bin/expenses") "-u"
                                    "-f" #$(string-append data-path "/expenses.db"))
                              #:user "expenses" #:group "nogroup" #:directory #$data-path
                              #:environment-variables
                              (cons* (string-append "SSL_CERT_DIR=" #$nss-certs "/etc/ssl/certs")
                                     (default-environment-variables)))))
           (stop #~(make-timer-destructor))
           (actions (list (shepherd-action
                           (name 'trigger) (procedure #~trigger-timer)
                           (documentation "Update exchange rates now."))))))))

(define (expenses-reverse-proxy config)
  (match-record config <expenses-configuration> (domain bind-address port)
    (if (string=? domain "localhost") (list)
        (list (https-reverse-proxy-configuration
               (domains (list domain))
               (destination-port port)
               (destination-ip bind-address))))))

(define expenses-service-type
  (service-type
   (name 'expenses)
   (extensions
    (list (service-extension shepherd-root-service-type expenses-shepherd-service)
          (service-extension account-service-type expenses-accounts)
          (service-extension https-reverse-proxy-service-type expenses-reverse-proxy)))
   (default-value (expenses-configuration))
   (description "Expenses web app server.")))
