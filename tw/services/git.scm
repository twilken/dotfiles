(define-module (tw services git)
  #:use-module (gnu)
  #:use-module (gnu home services)
  #:use-module (gnu packages terminals)
  #:use-module (gnu packages version-control)
  #:use-module (gnu services configuration)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module ((guix records) #:select (match-record))
  #:use-module (srfi srfi-1)
  #:export (home-git-identity
            home-git-configuration
            home-git-service-type))

(define-configuration/no-serialization home-git-identity
  (name string "Short slug identifying this identity.  Only used to name config files.")
  (root-directory string "Directory under which this identity should apply.")
  (email string "Author commits as this email for this identity.")
  (signing-key string "Sign commits using this key for this identity."))

(define (list-of-identities? thing)
  (and (list? thing) (every home-git-identity? thing)))

(define-configuration/no-serialization home-git-configuration
  (default-email string "Author git commits as this email.")
  (default-signing-key string "Sign git commits using this email.")
  (identities (list-of-identities '()) "Override default configuration values
under specific directories, e.g. to use a work identity in a directory
containing work projects."))

(define (gitconfig-includes identities)
  (if (null? identities) '()
      (cons (match-record (car identities) <home-git-identity> (name root-directory)
              ;; A relative path is relative to the gitconfig file.
              (format #f "[includeIf \"gitdir:~a/**\"]\n\tpath = ~aconfig"
                      (string-trim-right root-directory #\/) name))
            (gitconfig-includes (cdr identities)))))

(define (gitconfig-otherfiles identities)
  (if (null? identities) '()
      (cons (match-record (car identities) <home-git-identity> (name email signing-key)
              (list
               (string-append "git/" name "config")
               (plain-file (string-append "git" name "config")
                 (string-append
                  "[user]\n"
                  (if email (string-append "\temail = " email "\n") "")
                  (if signing-key (string-append "\tsigningkey = " signing-key "\n") "")))))
            (gitconfig-otherfiles (cdr identities)))))

(define (gitconfig config)
  (match-record config <home-git-configuration> (default-email default-signing-key identities)
    `(,@(gitconfig-otherfiles identities)
      ("git/config" ,(mixed-text-file "gitconfig" "\
# This is Git's per-user configuration file.
[user]
	name = Timo Wilken
	email = " default-email "
	signingkey = " default-signing-key
        (string-join (gitconfig-includes identities) "\n" 'prefix) "
[commit]
	gpgsign = true
	verbose = true
[url \"ssh://git@gitlab.cern.ch:7999/\"]
	insteadOf = https://gitlab.cern.ch/
[url \"ssh://git@ssh.github.com/\"]
	insteadOf = gh:
[gui]
	fontui = -family \\\"Fira Sans\\\" -size 10 -weight normal -slant roman -underline 0 -overstrike 0
	fontdiff = -family \\\"Hermit\\\" -size 10 -weight normal -slant roman -underline 0 -overstrike 0
	tabsize = 4
[merge]
	summary = true
	conflictstyle = zdiff3
[rebase]
	autoSquash = true
	autoStash = true
	updateRefs = true
[color]
	ui = auto
[column]
	ui = auto
[branch]
	sort = -committerdate
[tag]
	sort = version:refname
[diff]
	algorithm = histogram
	renames = true
	colorMoved = plain
	colorMovedWS = allow-indentation-change
	# Use 'i/', 'w/' and 'c/' prefixes instead of 'a/' and 'b/' in diffs
	# (for index, workdir and commit).
	mnemonicPrefix = true
[fetch]
	prune = true
[push]
	autoSetupRemote = true
[pull]
	rebase = false
	ff = only
[alias]
	glog = log --decorate --graph --oneline
	plog = log --decorate --graph --oneline --pretty=tformat:\\\"%C(yellow)%h %Cgreen%as %Cblue%<(10,trunc)%an%Cred%d%Creset %s\\\"
[init]
	defaultBranch = master
[advice]
	detachedHead = false
	addEmptyPathspec = false
")))))

(define home-git-service-type
  (service-type
   (name 'git)
   (extensions
    (list (service-extension home-xdg-configuration-files-service-type gitconfig)))
   (description "Configure Git.")))
