(define-module (tw services nextcloud)
  #:use-module (ice-9 match)
  #:use-module (gnu)
  #:use-module (gnu packages backup)
  #:use-module (gnu packages certs)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages databases)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages php)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages web)
  #:use-module (gnu services)
  #:use-module (gnu services certbot)
  #:use-module (gnu services configuration)
  #:use-module (gnu services databases)
  #:use-module (gnu services shepherd)
  #:use-module (gnu services web)
  #:use-module (guix gexp)
  #:use-module ((guix packages) #:select (package? package-version))
  #:use-module ((guix records) #:select (match-record))
  #:use-module ((guix utils) #:select (version-major))
  #:use-module (tw packages php)
  #:use-module (tw services restic)
  #:use-module (tw services web)
  #:export (%nextcloud-php.ini
            %nextcloud-php-fpm-service
            nextcloud-configuration
            nextcloud-service-type))

(define %nextcloud-php.ini
  (computed-file "nextcloud-php.ini"
    #~(begin
        (use-modules (ice-9 popen) (ice-9 rdelim))
        (let* ((php-config #$(file-append php "/bin/php-config"))
               (pipe (open-pipe* OPEN_READ php-config "--extension-dir"))
               (php-extdir (read-line pipe)))
          (unless (zero? (status:exit-val (close-pipe pipe)))
            (error "Failed to get PHP extension dir"))
          (with-output-to-file #$output
            ;; Guix's PHP comes with the following extensions built-in,
            ;; so no extension= line necessary:
            ;; pdo_mysql, pdo_pgsql, pgsql, bcmath, bz2, exif, gd, iconv, intl
            ;; The default pgsql.* settings are fine.
            (lambda () (display (string-append "\
; https://docs.nextcloud.com/server/30/admin_manual/installation/php_configuration.html#ini-values
memory_limit=512M
; From Nextcloud's .htaccess file (grep for php_value).
mbstring.func_overload=0
default_charset=UTF-8
output_buffering=0
; https://docs.nextcloud.com/server/30/admin_manual/configuration_files/big_file_upload_configuration.html
; Match nginx' client_max_body_size setting.
upload_max_filesize=512M
post_max_size=512M
max_input_time=3600
max_execution_time=3600
; Find extensions installed in the Guix system profile
extension_dir=/run/current-system/profile/lib/php/extensions/" (basename php-extdir) "
extension=imagick
extension=redis
; Caching extensions for Nextcloud
extension=apcu
apc.enable_cli=1
zend_extension=opcache
; https://www.php.net/manual/en/opcache.configuration.php
opcache.enable=1
opcache.interned_strings_buffer=32
opcache.max_accelerated_files=10000
opcache.memory_consumption=256
opcache.save_comments=1
; It will take up to revalidate_freq seconds for changes to config.php to be applied.
opcache.revalidate_freq=120
; https://docs.nextcloud.com/server/latest/admin_manual/configuration_database/linux_database_configuration.html#db-config-postgresql-label
[PostgreSQL]
pgsql.allow_persistent=On
pgsql.auto_reset_persistent=Off
pgsql.max_persistent=-1
pgsql.max_links=-1
pgsql.ignore_notice=0
pgsql.log_notice=0
"))))))))

(define %nextcloud-php-fpm-service
  (service (@ (tw services php-fpm) php-fpm-service-type)
    ((@ (tw services php-fpm) php-fpm-configuration)
     (environment-variables   ; Nextcloud News needs this.
      `(("SSL_CERT_DIR" . ,(file-append nss-certs "/etc/ssl/certs"))
        ("PATH" . "/run/privileged/bin:/run/current-system/profile/bin:/run/current-system/profile/sbin")))
     (php-ini-file %nextcloud-php.ini))))

(define-configuration/no-serialization nextcloud-configuration
  (domain string "The domain where the Nextcloud instance is hosted.  Subpaths
are not currently supported by @code{nextcloud-service-type}.")
  (postgresql (package postgresql) "The PostgresQL package to use for backing
up the database.  Must match the running server version.")
  (backup-repository string "The path on disk where the restic backup
repository is located.  This repository must be encrypted using the password
in @code{backup-password-file} and must be created manually.")
  (backup-password-file string "The file on disk containing the password for
the @code{backup-repository}."))

(define (nextcloud-backup-program config)
  (match-record config <nextcloud-configuration> (postgresql backup-repository backup-password-file)
    (program-file "nextcloud-backup-command"
      #~(begin
          (use-modules (srfi srfi-1)
                       (srfi srfi-26)
                       (ice-9 popen)
                       (ice-9 receive)
                       (ice-9 textual-ports))

          (define nextcloud-dir "/var/www/nextcloud")
          (define nextcloud-data-partition "/var/data")  ; mountpoint of the partition containing Nextcloud data dir
          (define nextcloud-data-path "nextcloud")  ; relative to `nextcloud-data-partition'
          (define snapshot (string-append nextcloud-data-partition "/tmp-nextcloud-backup"))
          (define btrfs #$(file-append btrfs-progs "/bin/btrfs"))
          (define restic #$(file-append restic "/bin/restic"))
          (setenv "RESTIC_REPOSITORY" #$backup-repository)
          (setenv "RESTIC_PASSWORD_FILE" #$backup-password-file)

          (define (nc-maintenance enable?)
            (let ((child-pid (primitive-fork)))
              (if (zero? child-pid)
                  (begin  ; this is the child
                    (setgid (group:gid (getgr "php-fpm")))  ; while still root
                    (setuid (passwd:uid (getpw "php-fpm")))
                    (execl #$(file-append php "/bin/php")
                           "php" "-c" #$%nextcloud-php.ini
                           (string-append nextcloud-dir "/occ")
                           "maintenance:mode"
                           (if enable? "--on" "--off")))
                  (zero? (status:exit-val (cdr (waitpid child-pid)))))))

          (define* (cleanup #:optional (recv-signal #f) #:key (rethrow #f))
            (nc-maintenance #f)
            (when (and (file-exists? snapshot)
                       (file-is-directory? snapshot))
              (system* btrfs "subvolume" "delete" "-c" snapshot))
            (cond
             (recv-signal (exit (- recv-signal)))
             (rethrow (raise-exception rethrow))))

          (define (run-pipeline . commands)
            (receive (from to pids) (pipeline commands)
              (close to)
              (do ((char (read-char from) (read-char from)))
                  ((eof-object? char))
                (display char))
              (close from)
              (let ((failing-index
                     (list-index (compose not zero? status:exit-val cdr waitpid)
                                 (reverse pids))))
                (when failing-index
                  (apply error "Command exited with error status"
                         (list-ref commands failing-index))))))

          (define (read-file name)
            (string-trim-right (call-with-input-file name get-string-all) #\newline))

          (define (main)
            (unless (nc-maintenance #t)
              (error "Could not enter maintenance mode"))

            ;; Backup the database. This can only be done offline.
            (run-pipeline
             '(#$(file-append postgresql "/bin/pg_dump") "-wU" "nextcloud" "nextcloud")
             `(,restic "backup" "--no-cache" "--stdin" "--stdin-filename=nextcloud.sql"))

            ;; These shouldn't be copied while Nextcloud is online. They're also
            ;; not in the data folder, so they won't be in the snapshot below.
            (run-pipeline
             (list restic "backup" "--no-cache"
                   (string-append nextcloud-dir "/config")
                   (string-append nextcloud-dir "/themes")))

            ;; Make sure everything is synced to disk so it's in our snapshot.
            (run-pipeline
             (list btrfs "filesystem" "sync"
                   (string-append nextcloud-data-partition "/" nextcloud-data-path)))
            (run-pipeline
             (list btrfs "subvolume" "snapshot" "-r" nextcloud-data-partition snapshot))

            ;; At this point, the data directory is in the snapshot, so Nextcloud
            ;; can be turned on again.
            (nc-maintenance #f)

            ;; We don't need files under preview/, as those are thumbnails from
            ;; the Previews "app" and can be regenerated using `php -f occ
            ;; preview:pre-generate`.
            (run-pipeline
             (list restic "backup" "--no-cache"
                   "--exclude=appdata_*/preview"
                   "--exclude=appdata_*/passwords/*Cache"
                   (string-append snapshot "/" nextcloud-data-path))))

          (for-each (cut sigaction <> cleanup) (list SIGHUP SIGINT SIGTERM))
          (with-exception-handler (cut cleanup #:rethrow <>) main)
          (cleanup)))))

(define nextcloud-default-headers
  ;; HTTP response headers borrowed from Nextcloud `.htaccess`
  (map (match-lambda
         ((hdr . value)
          `("add_header " ,hdr " \"" ,value "\" always;")))
       '(("Referrer-Policy" . "no-referrer")
         ("Strict-Transport-Security" . "max-age=15552000")
         ("X-Content-Type-Options" . "nosniff")
         ("X-Frame-Options" . "SAMEORIGIN")
         ("X-Permitted-Cross-Domain-Policies" . "none")
         ("X-Robots-Tag" . "noindex, nofollow")
         ("X-XSS-Protection" . "1; mode=block"))))

(define (nextcloud-https-server config)
  ;; https://docs.nextcloud.com/server/latest/admin_manual/installation/nginx.html
  (match-record config <nextcloud-configuration> (domain)
    (list (nginx-server-configuration
           ;; The certbot service redirects everything on port 80 to
           ;; port 443 by default, modulo its own /.well-known paths.
           (listen '("443 ssl"))
           (server-name (list domain))
           (root "/var/www/nextcloud")
           (index '("index.php" "index.html" "/index.php$request_uri"))
           (try-files '("$uri" "$uri/" "/index.php$request_uri"))
           (ssl-certificate (string-append "/etc/certs/" domain "/fullchain.pem"))
           (ssl-certificate-key (string-append "/etc/certs/" domain "/privkey.pem"))
           (server-tokens? #f)
           (raw-content
            `("http2 on;"
              ;; Set max upload size and increase upload timeout
              "client_max_body_size 512M;"
              "client_body_timeout 300s;"
              "fastcgi_buffers 64 4K;"

              ;; Enable gzip but do not remove ETag headers
              "gzip on;"
              "gzip_vary on;"
              "gzip_comp_level 4;"
              "gzip_min_length 256;"
              "gzip_proxied expired no-cache no-store private no_last_modified no_etag auth;"
              ("gzip_types application/atom+xml text/javascript application/javascript "
               "application/json application/ld+json application/manifest+json "
               "application/rss+xml application/vnd.geo+json application/vnd.ms-fontobject "
               "application/wasm application/x-font-ttf application/x-web-app-manifest+json "
               "application/xhtml+xml application/xml font/opentype image/bmp image/svg+xml "
               "image/x-icon text/cache-manifest text/css text/plain text/vcard text/vtt "
               "text/vnd.rim.location.xloc text/x-component text/x-cross-domain-policy;")

              ,@nextcloud-default-headers

              ;; Remove X-Powered-By, which is an information leak
              "fastcgi_hide_header X-Powered-By;"

              ;; Add .mjs as a file extension for JavaScript
              ("include " ,nginx "/share/nginx/conf/mime.types;")
              "types { text/javascript mjs; }"))

           (locations
            (list
             (nginx-location-configuration  ; Handle Microsoft DAV clients
              (uri "= /")
              (body '("if ( $http_user_agent ~ ^DavClnt ) { return 302 /remote.php/webdav/$is_args$args; }")))
             (nginx-location-configuration
              (uri "= /robots.txt")
              (body '("allow all;" "log_not_found off;" "access_log off;")))

             ;; The rules in these blocks are an adaptation of the rules
             ;; in `.htaccess` that concern `/.well-known`.
             (nginx-location-configuration
              (uri "= /.well-known/carddav")
              (body '("return 301 /remote.php/dav/;")))
             (nginx-location-configuration
              (uri "= /.well-known/caldav")
              (body '("return 301 /remote.php/dav/;")))
             (nginx-location-configuration
              (uri "= /.well-known/acme-challenge")
              (body '("try_files $uri $uri/ =404;")))
             (nginx-location-configuration
              (uri "= /.well-known/pki-validation")
              (body '("try_files $uri $uri/ =404;")))
             ;; Let Nextcloud's API for `/.well-known` URIs handle all other
             ;; requests by passing them to the front-end controller.
             (nginx-location-configuration
              (uri "^~ /.well-known")
              (body '("return 301 /index.php$request_uri;")))

             ;; Rules borrowed from `.htaccess` to hide certain paths from clients
             (nginx-location-configuration
              (uri "~ ^/(?:build|tests|config|lib|3rdparty|templates|data)(?:$|/)")
              (body '("return 404;")))
             (nginx-location-configuration
              ;; Exclude .well-known to avoid overriding rules above (regexes take precedence).
              (uri "~ ^/(?:\\.(?!well-known)|autotest|occ|issue|indie|db_|console)")
              (body '("return 404;")))

             ;; Ensure this block, which passes PHP files to the PHP process, is above the blocks
             ;; which handle static assets (as seen below). If this block is not declared first,
             ;; then Nginx will encounter an infinite rewriting loop when it prepends `/index.php`
             ;; to the URI, resulting in a HTTP 500 error response.
             (nginx-location-configuration
              (uri "~ \\.php(?:$|/)")
              (body
               (let ((phpver (version-major (package-version php))))
                 `(("rewrite ^/(?!index|remote|public|cron|core\\/ajax\\/update|status|ocs\\/v[12]|"   ; Legacy support
                    "updater\\/.+|ocs-provider\\/.+|.+\\/richdocumentscode(_arm64)?\\/proxy) /index.php$request_uri;")
                   "fastcgi_request_buffering off;"
                   "fastcgi_split_path_info ^(.+?\\.php)(/.*)$;"
                   "try_files $fastcgi_script_name =404;"
                   ("include " ,nginx "/share/nginx/conf/fastcgi.conf;")
                   "fastcgi_param HTTP_PROXY \"\";"               ; Mitigate https://httpoxy.org/
                   "fastcgi_param modHeadersAvailable true;"      ; Avoid sending the security headers twice
                   "fastcgi_param front_controller_active true;"  ; Enable pretty urls
                   ("fastcgi_pass unix:/var/run/php" ,phpver "-fpm.sock;")))))  ; Match php-fpm-configuration

             (nginx-location-configuration
              (uri "~ \\.(?:css|js|mjs|svg|gif|ico|png|jpg|png|webp|wasm|tflite|map|ogg|flac)$")
              (body `("try_files $uri /index.php$request_uri;"
                      ;; The upstream configuration adds an "immutable"
                      ;; to this header if the original request had a
                      ;; ?v= flag, but then we would need to add a map{}
                      ;; to the nginx-configuration's extra-content.
                      "add_header Cache-Control \"public, max-age=15778463\";"
                      ;; Nginx resets any headers in a response block,
                      ;; if it contains even a single add_header line.
                      ;; We need to re-add the default headers.
                      ,@nextcloud-default-headers)))
             (nginx-location-configuration
              (uri "~ \\.woff2?$")
              (body '("try_files $uri /index.php$request_uri;" "expires 7d;")))
             (nginx-location-configuration
              (uri "/remote")
              (body '("return 301 /remote.php$request_uri;")))))))))

(define (nextcloud-pg-role config)
  (list (postgresql-role
         (name "nextcloud")
         (permissions '(login))   ; don't need `createdb' if we set `create-database?'
         (create-database? #t))))

(define (nextcloud-packages config)
  ;; PHP modules must be installed in system profile, as that's referred to in `%nextcloud-php.ini'.
  (list php php-apcu php-imagick php-redis openssl curl))

(define (nextcloud-certificates config)
  (match-record config <nextcloud-configuration> (domain)
    (list (certificate-configuration
           (domains (list domain))
           (deploy-hook %nginx-cert-deploy-hook)))))

(define (nextcloud-backup-cleanup config)
  (match-record config <nextcloud-configuration> (backup-repository backup-password-file)
    (list (restic-scheduled-cleanup
           (schedule #~(calendar-event #:hours '(7) #:minutes '(0)))
           (repo (restic-local-repository
                  (path backup-repository)))
           (password (restic-password-source
                      (type 'file)
                      (name backup-password-file)))
           (keep-daily 30)
           (keep-monthly -1)))))

(define (nextcloud-cron config)
  (list (shepherd-service
         (provision '(nextcloud-cron))
         (requirement '(user-processes file-systems networking))
         (documentation "Trigger Nextcloud on a schedule to allow it to do background work.")
         (modules '((shepherd service timer)))
         (start #~(make-timer-constructor
                   (calendar-event #:minutes '(0 5 10 15 20 25 30 35 40 45 50 55))
                   (command (list #$(file-append php "/bin/php") "-c" #$%nextcloud-php.ini
                                  "/var/www/nextcloud/cron.php")
                            #:user "php-fpm" #:environment-variables
                            ;; Nextcloud News needs this to fetch HTTPS feeds.
                            (cons* (format #f "SSL_CERT_DIR=~a/etc/ssl/certs" #$nss-certs)
                                   (default-environment-variables)))))
         (stop #~(make-timer-destructor))
         (actions (list (shepherd-action
                         (name 'trigger) (procedure #~trigger-timer)
                         (documentation "Trigger the Nextcloud background worker now.")))))

        (shepherd-service
         (provision '(nextcloud-backup))
         (requirement '(user-processes file-systems))
         (documentation "Backup Nextcloud user data and settings.")
         (modules '((shepherd service timer)))
         (start #~(make-timer-constructor
                   (calendar-event #:hours '(6) #:minutes '(0))
                   (command (list #$(nextcloud-backup-program config)))))
         (stop #~(make-timer-destructor))
         (actions (list (shepherd-action
                         (name 'trigger) (procedure #~trigger-timer)
                         (documentation "Backup Nextcloud files now.")))))))

(define nextcloud-service-type
  (service-type
   (name 'nextcloud)
   (extensions
    (list (service-extension nginx-service-type nextcloud-https-server)
          (service-extension postgresql-role-service-type nextcloud-pg-role)
          (service-extension profile-service-type nextcloud-packages)
          (service-extension certbot-service-type nextcloud-certificates)
          (service-extension restic-cleanup-service-type nextcloud-backup-cleanup)
          (service-extension shepherd-root-service-type nextcloud-cron)))
   (description "Run a Nextcloud instance.")))
