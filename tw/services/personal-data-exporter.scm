(define-module (tw services personal-data-exporter)
  #:use-module (gnu)
  #:use-module ((gnu packages backup)
                #:select (restic))
  #:use-module ((gnu packages guile)
                #:select (guile-json-4))
  #:use-module ((gnu packages guile-xyz)
                #:select (guile-squee))
  #:use-module ((gnu packages tls)
                #:select (guile-gnutls))
  #:use-module (gnu services)
  #:use-module (gnu services configuration)
  #:use-module (gnu services databases)
  #:use-module (gnu services shepherd)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module ((guix records)
                #:select (match-record))
  #:use-module (tw packages finance)
  #:use-module (tw services restic)
  #:use-module (tw services secrets)
  #:export (personal-data-exporter-configuration
            personal-data-exporter-service-type))

(define-configuration/no-serialization personal-data-exporter-configuration
  (user string "The UNIX user name to run as locally.  The database user and
database itself will be named after this user.")
  (group string "The UNIX group of the @code{user} configured here.  Used to
run daemons.")
  (postgresql package "The PostgresQL package to use.  This must match the
database server configured on the system as it is used to dump and backup the
existing database.")
  (gps-data-directory string "The directory where recorded GPS data can be
found as CSV files.  These will be read as the configured @code{user}.")
  (ledger-file string "The location on disk where @code{ledgerplot} can expect
the main ledger file.  It is expected that this file is synced externally, for
example using Syncthing.")
  (ledger-locale (string "en_US.utf8") "A locale definition present on the
system, passed to @code{ledgerplot} so that it can read a UTF-8 ledger
file.")
  (conso-config-file (string "/etc/conso.json") "Configuration file for the
electricity consumption fetcher, which stores the required secrets.  This file
is completely managed by @code{personal-data-exporter-service-type}.")
  (conso-backup-repo (string "/var/backups/electricity-conso-db") "Location of
the backup repository where electricity consumption data will be archived.
The repository must already exist and be owned by the configured user.")
  (conso-backup-encrypted-password file-like "The store item containing the
encrypted password for the @code{conso-backup-repo}.  Will be decrypted and
installed as @code{conso-backup-password}.")
  (conso-backup-password string "The file storing the password for the
@code{conso-backup-repo}.  This file is completely managed by
@code{personal-data-exporter-service-type}."))

(define (conso-fetch-command config)
  (match-record config <personal-data-exporter-configuration> (conso-config-file)
    (program-file "conso-fetch-command"
      (with-extensions (list guile-squee guile-json-4 guile-gnutls)   ; guile-gnutls needed by (web client)
        #~(begin
            (use-modules (ice-9 match)
                         (ice-9 receive)
                         (srfi srfi-19)             ; dates
                         (web client)
                         (web response)
                         (squee)
                         (json))

            (define-json-type <settings> (prm) (api-token))
            (define-json-type <data-point> (date) (value))
            (define-json-type <data>
              (points "interval_reading" #(<data-point>)))

            (define %settings (call-with-input-file #$conso-config-file json->settings))

            (define (last-date-with-data db table)
              "Find the latest date that has some data stored in the given DB TABLE."
              (match (exec-query db (format #f "select max(\"time\") from \"~a\";" table))
                (((#f)) (make-date 0 0 0 0 1 1 2000 0))   ; no data: fetch as much as possible
                (((dt)) (string->date dt "~Y-~m-~d"))))   ; trailing time is ignored, if any

            (define (clamp-date date max-age-days)
              "Return a date that is as close as possible to DATE, but at most MAX-AGE-DAYS days before now."
              (let ((today (current-julian-day)))
                (if (> (- today (date->julian-day date)) max-age-days)
                    (julian-day->date (- today max-age-days))
                    date)))

            (define (conso-request endpoint start-date)
              "Fetch data from the given conso.boris.sh API ENDPOINT."
              ;; Load curve data is only available max 1 week back; other data is available up to 2 years back.
              (let ((max-age (if (string=? "consumption_load_curve" endpoint) 7 (* 2 365))))
                (receive (response body)
                    (http-request
                     (string-append "https://conso.boris.sh/api/" endpoint
                                    "?prm=" (settings-prm %settings)
                                    "&start=" (date->string (clamp-date start-date max-age) "~1")
                                    "&end=" (date->string (current-date) "~1"))
                     #:headers `((authorization . (basic . ,(settings-api-token %settings)))
                                 (user-agent . "conso.scm/0.1") (from . "abuse@twilken.net"))
                     #:streaming? #t)
                  (match (response-code response)
                    (200 (data-points (json->data body)))
                    (err (error "Got error response from server:" err (response-reason-phrase response)))))))

            (define (import-data db table endpoint)
              "Fetch data from the API ENDPOINT and store it in the DB TABLE."
              ;; If a value already exists for any given point in time, replace it.
              (let ((query (format #f "insert into \"~a\" values ($1, $2) on conflict (\"time\") do update set \"value\" = EXCLUDED.\"value\";" table)))
                (for-each (match-lambda
                            (($ <data-point> date value)
                             (exec-query db query (list date value))))
                          (conso-request endpoint (last-date-with-data db table)))))

            (let ((db (connect-to-postgres-paramstring "")))
              (exec-query db "
set client_min_messages = warning;  -- silence notices from existing tables
create table if not exists \"conso_daily\" (\"time\" date primary key, \"value\" real not null);
create table if not exists \"conso_load\" (\"time\" timestamp primary key, \"value\" real not null);
create table if not exists \"conso_max_power\" (\"time\" timestamp primary key, \"value\" real not null);
")
              (import-data db "conso_daily" "daily_consumption")
              (import-data db "conso_max_power" "consumption_max_power")
              (import-data db "conso_load" "consumption_load_curve")
              (pg-conn-finish db)))))))

(define (conso-backup-command config)
  (match-record config <personal-data-exporter-configuration>
                (postgresql user conso-backup-repo conso-backup-password)
    (program-file "conso-backup-command"
      #~(begin
          (use-modules (srfi srfi-1) (ice-9 popen) (ice-9 receive))

          (setenv "RESTIC_REPOSITORY" #$conso-backup-repo)
          (setenv "RESTIC_PASSWORD_FILE" #$conso-backup-password)

          (receive (from to pids)
              (pipeline
               ;; Match postgres version of the running server here.
               '((#$(file-append postgresql "/bin/pg_dump")
                  "-wU" #$user "-t" "conso_*" #$user)
                 (#$(file-append restic "/bin/restic") "backup" "-q"
                  "--no-cache" "--stdin" "--stdin-filename=conso.sql")))

            (close to)
            (do ((char (read-char from) (read-char from)))
                ((eof-object? char))
              (display char))
            (close from)
            (exit (every (compose (lambda (ev) (and ev (zero? ev)))
                                  status:exit-val cdr waitpid)
                         pids)))))))

(define (conso-backup-cleanup config)
  (match-record config <personal-data-exporter-configuration>
                (user conso-backup-repo conso-backup-password)
    (list (restic-scheduled-cleanup
           (schedule #~(calendar-event #:hours '(10) #:minutes '(0)))
           (repo (restic-local-repository (path conso-backup-repo)))
           (password (restic-password-source (type 'file) (name conso-backup-password)))
           (user user)
           (keep-daily 14)
           (keep-monthly -1)))))

(define (conso-secrets config)
  (match-record config <personal-data-exporter-configuration>
                (user group conso-config-file conso-backup-password conso-backup-encrypted-password)
    (list (secret
           (encrypted-file (local-file "files/conso.json.enc"))
           (destination conso-config-file)
           (user user) (group group))
          (secret
           (encrypted-file conso-backup-encrypted-password)
           (destination conso-backup-password)
           (user user) (group group)))))

(define (gpsplot-command config)
  (match-record config <personal-data-exporter-configuration> (postgresql gps-data-directory)
    (program-file "gpsplot-command"
      #~(begin
          (use-modules (ice-9 ftw) (ice-9 popen) (srfi srfi-26))
          (define pipe (open-pipe* OPEN_WRITE #$(file-append postgresql "/bin/psql") "-q"))
          (display "DROP TABLE IF EXISTS \"gps\";" pipe)
          (display "CREATE UNLOGGED TABLE \"gps\" (
            \"time\" TIMESTAMP WITH TIME ZONE NOT NULL,
            \"latitude\" DOUBLE PRECISION NOT NULL,
            \"longitude\" DOUBLE PRECISION NOT NULL,
            \"elevation\" DOUBLE PRECISION DEFAULT NULL,
            \"accuracy\" DOUBLE PRECISION DEFAULT NULL,
            \"bearing\" DOUBLE PRECISION DEFAULT NULL,
            \"speed\" DOUBLE PRECISION DEFAULT NULL,
            \"satellites\" INTEGER DEFAULT NULL,
            \"provider\" TEXT DEFAULT NULL,
            \"hdop\" REAL DEFAULT NULL,
            \"vdop\" REAL DEFAULT NULL,
            \"pdop\" REAL DEFAULT NULL,
            \"geoidheight\" REAL DEFAULT NULL,
            \"ageofdgpsdata\" REAL DEFAULT NULL,
            \"dgpsid\" INTEGER DEFAULT NULL,
            \"activity\" TEXT DEFAULT NULL,
            \"battery\" INTEGER DEFAULT NULL,
            \"annotation\" TEXT DEFAULT NULL,
            \"unix_timestamp\" BIGINT DEFAULT NULL,
            \"time_with_timezone\" TIMESTAMP WITH TIME ZONE DEFAULT NULL,
            \"distance\" DOUBLE PRECISION DEFAULT NULL,
            \"start_time\" BIGINT DEFAULT NULL,
            \"profile\" TEXT DEFAULT NULL,
            \"battery_charging\" BOOLEAN DEFAULT NULL
          );" pipe)
          (display "BEGIN TRANSACTION;" pipe)
          (display "TRUNCATE TABLE \"gps\";" pipe)  ; needed to use \COPY ... FREEZE
          (newline pipe)
          ;; Need to use \COPY, not COPY, as the Postgres server does not have access to the source files.
          (for-each (cut format pipe "\\COPY \"gps\" FROM '~a/~a' (FORMAT CSV, FREEZE, HEADER 1);\n"
                         #$(string-trim-right gps-data-directory #\/) <>)
                    (scandir #$gps-data-directory (cut string-suffix? ".csv" <>)))
          (display "COMMIT;" pipe)
          (exit (status:exit-val (close-pipe pipe)))))))

(define (personal-data-shepherd-services config)
  (match-record config <personal-data-exporter-configuration> (user group ledger-file ledger-locale)
    (list (shepherd-service
           (provision '(ledgerplot))
           (requirement (list 'postgresql (string->symbol (string-append "syncthing-" user))))
           (documentation
            "Monitor a ledger file and keep a database in sync with it.")
           (start #~(make-forkexec-constructor
                     ;; Limit parallel hledger processes as they use too much memory on lud.
                     (list #$(file-append ledgerplot "/bin/ledgerplot") "-j" "2"
                           ;; Use local socket auth so that we don't have to supply a password.
                           "-wd" #$user "-U" #$user "-H" "/var/run/postgresql")
                     #:user #$user #:group #$group
                     #:environment-variables
                     (cons*
                      (string-append "LEDGER_FILE=" #$ledger-file)
                      ;; Use an appropriate locale so that ledgerplot
                      ;; can read the UTF-8 ledger file.
                      (string-append "LC_ALL=" #$ledger-locale)
                      (default-environment-variables))))
           (stop #~(make-kill-destructor)))

          ;; Ledgerplot uses the Boerse Frankfurt API, so run after markets close there.
          ;; According to https://www.boerse.de/handelszeiten/, it's 22:00 CET/CEST.
          (shepherd-service
           (provision '(ledgerplot-exchange-rates))
           (requirement '(user-processes file-systems networking))
           (documentation "Download the latest exchange rates.")
           (modules '((shepherd service timer)))
           (start #~(make-timer-constructor
                     (calendar-event #:hours '(22) #:minutes '(5)
                                     #:days-of-week '(monday tuesday wednesday thursday friday))
                     (command (list #$(file-append ledgerplot "/bin/ledgerplot") "-em")
                              #:user #$user #:environment-variables
                              (cons* (string-append "LEDGER_FILE=" #$ledger-file)
                                     (default-environment-variables)))))
           (stop #~(make-timer-destructor))
           (actions (list (shepherd-action
                           (name 'trigger) (procedure #~trigger-timer)
                           (documentation "Fetch exchange rates now.")))))

          ;; Update GPS data every quarter hour.  This takes approx. 10
          ;; seconds, so it should be fairly safe to run often.
          (shepherd-service
           (provision '(gpsplot))
           (requirement '(user-processes file-systems networking))
           (documentation "Process location history data.")
           (modules '((shepherd service timer)))
           (start #~(make-timer-constructor
                     (calendar-event #:minutes '(0 15 30 45))
                     (command (list #$(gpsplot-command config)) #:user #$user)))
           (stop #~(make-timer-destructor))
           (actions (list (shepherd-action
                           (name 'trigger) (procedure #~trigger-timer)
                           (documentation "Process location history data now.")))))

          ;; Process the previous day's data during the night.
          ;; Don't do this too early, else we get a 404 status from the server.
          (shepherd-service
           (provision '(conso-fetch))
           (requirement '(user-processes file-systems networking))
           (documentation "Fetch the previous day's electricity consumption data.")
           (modules '((shepherd service timer)))
           (start #~(make-timer-constructor
                     (calendar-event #:hours '(7) #:minutes '(0))
                     (command (list #$(conso-fetch-command config)) #:user #$user)))
           (stop #~(make-timer-destructor))
           (actions (list (shepherd-action
                           (name 'trigger) (procedure #~trigger-timer)
                           (documentation "Fetch yesterday's electricity consumption data now.")))))

          ;; Back up electricity consumption database.  The other data can
          ;; be easily recreated from the source data, but this data
          ;; becomes inaccessible after 2 years via the API.
          (shepherd-service
           (provision '(conso-backup))
           (requirement '(user-processes file-systems networking))
           (documentation "Back up electricity consumption database.")
           (modules '((shepherd service timer)))
           (start #~(make-timer-constructor
                     (calendar-event #:hours '(7) #:minutes '(15))
                     (command (list #$(conso-backup-command config)) #:user #$user)))
           (stop #~(make-timer-destructor))
           (actions (list (shepherd-action
                           (name 'trigger) (procedure #~trigger-timer)
                           (documentation "Back up electricity consumption database now."))))))))

(define (personal-data-db-roles config)
  (match-record config <personal-data-exporter-configuration> (user)
    (list (postgresql-role
           (name user)
           (create-database? #t)
           (permissions '(login))))))

(define personal-data-exporter-service-type
  (service-type
   (name 'personal-data)
   (extensions
    (list (service-extension shepherd-root-service-type personal-data-shepherd-services)
          (service-extension postgresql-role-service-type personal-data-db-roles)
          (service-extension restic-cleanup-service-type conso-backup-cleanup)
          (service-extension secrets-service-type conso-secrets)))
   (description "Sync various personal data to a database, for displaying in Grafana.")))
