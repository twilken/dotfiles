(define-module (tw deploy)
  #:use-module (gnu machine)
  #:use-module (gnu machine ssh)
  #:use-module (gnu system)
  #:use-module (tw system)
  #:use-module (tw system lud)
  #:use-module (tw system vin))

(define-public %host-keys
  '(("lud.twilken.net" . "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGqXbxv3a2bZyGjnEirVCMtRBeLKW/ha8ULSR9Xye4Z1 root@(none)")
    ("vin.twilken.net" . "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEEpdfKxzoCwg53TKPF5YxgUwhGF+bELAyBGdxagQroJ root@(none)")))

(define-public (operating-system->machine system)
  (machine
   (operating-system system)
   (environment managed-host-environment-type)
   (configuration
    (machine-ssh-configuration
     (host-name (operating-system-host-name system))
     (port (assoc-ref %ssh-ports host-name))
     (host-key (assoc-ref %host-keys host-name))
     (user "guixdeploy")
     (identity "~/.local/share/ssh-keys/guixdeploy_id_ed25519")
     (system "x86_64-linux")
     (build-locally? #f)))))

(map operating-system->machine
     (list %lud-system %vin-system))
