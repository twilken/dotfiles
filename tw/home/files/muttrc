# -*- mode: conf-space; -*-
# Colour scheme
source `echo "\$XDG_CONFIG_HOME/mutt/catppuccin.muttrc"`
color index color2 default ~O   # unread, but not new, messages are green, like new messages

# Vim-like keybindings
source ~/.guix-home/profile/share/doc/neomutt/vim-keys/vim-keys.rc
# I have a Return key, not an Enter key, and I want to use it to display the
# message, not reply to it.
bind index '<Return>' display-message
bind index '<Enter>' list-reply
# group-chat-reply = reply to all recipients preserving To/Cc.
bind index a group-chat-reply
bind pager a group-chat-reply

set abort_nosubject = ask-yes
set abort_unmodified = ask-yes
set auto_edit edit_headers fast_reply reverse_name noreverse_real_name
set sig_dashes sig_on_top
set pager_stop pager_index_lines=10 pager_context=2 pager_skip_quoted_context=3

set allow_ansi   # needed for catimg and the like
unset help       # hide keybind hints in top row
# Don't ask for confirmation.
set quit
unset confirmappend

# Use khard for contact info.
set query_command = "khard email --parsable"

set sort = "reverse-date"  # newest first, threads grouped
set use_threads = "threads"
unset mark_old    # always keep unread messages as "new"
set tilde nomarkers
set sleep_time = 0  # no delay when switching mailboxes to display info
set auto_tag   # commands automatically apply to tagged messages (if any), without having to press ";"
set delete   # auto-purge deleted messages when syncing

# show IMAP mailboxes in the sidebar
set imap_check_subscribed

set sidebar_visible sidebar_format = "%B%* %?F? %F!?%?N? %N?"
set sidebar_folder_indent sidebar_short_path

set index_format = "%4C %Z  %[%_d %b %H:%M]  %-15.15L  %s%*   %?l?%4l&%4c?"
set status_format = " %r %f (%?M?%M/?%m#%?n? %nN?%?o? %oO?%?d? %dD?%?F? %FF?%?t? %t*?%?p? %pP?%?b? %bE?%?l? %l?) %> %s/%S   %P   "
set sidebar_format = "%D%* %!%?N? %N?"
set attach_format = "%u%D%I %t%4n %T%.40d%> %.9m/%.12M, %.6e%?C?, %C?, %4s "

set mail_check_stats
set pgp_decrypt_command = "gpg --status-fd=2 %?p?--passphrase-fd 0? --no-verbose --quiet --batch --output - %f"
set smime_encrypt_self smime_default_key = 681F69B0
unset arrow_cursor
set ssl_force_tls

set new_mail_command = "if [ %u -gt 0 ]; then dunstify -a neomutt -i mail-unread 'New mail' 'You have %u unread messages.'; fi"

set history = 1000
set history_file = `echo "\$XDG_DATA_HOME/neomutt/history"`
set save_history = 1000

# The $XDG_DATA_HOME/neomutt directory must exist, else neomutt will
# silently fail to save the certificate file.
set certificate_file = `echo "\$XDG_DATA_HOME/neomutt/certificates"`
set debug_file = `echo "\${XDG_LOG_HOME:-$XDG_DATA_HOME/neomutt}/neomutt-debug.log"`

set smime_certificates = `echo "\$XDG_DATA_HOME/neomutt/smime/certificates"`
set smime_keys = `echo "\$XDG_DATA_HOME/neomutt/smime/keys"`

# For using both, header and body caching, $header_cache and
# $message_cachedir can be safely set to the same value.
set message_cachedir = `echo "\$XDG_CACHE_HOME/neomutt/"`
set header_cache = `echo "\$XDG_CACHE_HOME/neomutt/"`

# I don't care about these headers; hide them by default.
ignore x-mailer user-agent

alternative_order text/plain text/enriched text/html text image
auto_view text/html text/calendar text/x-diff text/x-patch image/jpeg image/png

set my_mythic = "imaps://oncilla.mythic-beasts.com/"
account-hook "$my_mythic" 'set folder = "$my_mythic"'
account-hook "$my_mythic" 'set smtp_url = "smtps://smtp-auth.mythic-beasts.com"'
account-hook "$my_mythic" 'set imap_user = "timo@twilken.net"'
account-hook "$my_mythic" 'set smtp_user = "timo@twilken.net"'
account-hook "$my_mythic" 'set imap_pass = `pass www/mythic-beasts/email/timo | head -1`'
account-hook "$my_mythic" 'set smtp_pass = `pass www/mythic-beasts/email/timo | head -1`'
account-hook "$my_mythic" 'set spool_file = +INBOX'
account-hook "$my_mythic" 'set postponed = +Drafts'
account-hook "$my_mythic" 'set record = +Sent'
account-hook "$my_mythic" 'set trash = +Rubbish'
account-hook "$my_mythic" 'set from = "$imap_user"'
account-hook "$my_mythic" 'reset ssl_use_tlsv1 ssl_use_tlsv1_1 imap_authenticators smtp_authenticators imap_oauth_refresh_command smtp_oauth_refresh_command'

set my_cern = "imaps://imap.cern.ch/"
account-hook "$my_cern" 'set folder = "$my_cern"'
account-hook "$my_cern" 'set smtp_url = "smtp://smtp.cern.ch:587"'   # we need to rely on STARTTLS
account-hook "$my_cern" 'set imap_user = "twilken"'
account-hook "$my_cern" 'set smtp_user = "twilken"'
account-hook "$my_cern" 'set imap_pass = `pass cern/sso | head -1`'
account-hook "$my_cern" 'set smtp_pass = `pass cern/sso | head -1`'
account-hook "$my_cern" 'set smtp_authenticators = "login"'   # only LOGIN supported, not even PLAIN
account-hook "$my_cern" "alternates '^timo\.wilken@cern\.ch$' '^twilken@cern\.ch$'"
account-hook "$my_cern" 'set spool_file = +INBOX'
account-hook "$my_cern" 'set postponed = +Drafts'
account-hook "$my_cern" 'set record = "+Sent Items"'
account-hook "$my_cern" 'set trash = "+Deleted Items"'
account-hook "$my_cern" 'set from = "$imap_user"'
account-hook "$my_cern" 'set ssl_use_tlsv1 ssl_use_tlsv1_1'   # imap.cern.ch needs this; make sure to reset for other accounts!
account-hook "$my_cern" 'reset imap_authenticators imap_oauth_refresh_command smtp_oauth_refresh_command'

set my_gmail = "imaps://imap.gmail.com/"
account-hook "$my_gmail" 'set folder = "$my_gmail"'
account-hook "$my_gmail" 'set smtp_url = "smtps://smtp.gmail.com"'
account-hook "$my_gmail" 'set imap_user = "timo.21.wilken@gmail.com"'
account-hook "$my_gmail" 'set smtp_user = "timo.21.wilken@gmail.com"'
account-hook "$my_gmail" 'set imap_pass = `pass www/google/app-passwords/mutt | head -1`'
account-hook "$my_gmail" 'set smtp_pass = `pass www/google/app-passwords/mutt | head -1`'
account-hook "$my_gmail" 'set spool_file = +INBOX'
account-hook "$my_gmail" 'set postponed = "+[Gmail]/Drafts"'
account-hook "$my_gmail" 'set record = "+[Gmail]/Sent Mail"'
account-hook "$my_gmail" 'set trash = "+[Gmail]/Bin"'
account-hook "$my_gmail" 'set from = "$imap_user"'
account-hook "$my_gmail" 'reset ssl_use_tlsv1 ssl_use_tlsv1_1 imap_authenticators smtp_authenticators imap_oauth_refresh_command smtp_oauth_refresh_command'

# ExOl needs OAUTH now!
# https://neomutt.org/guide/optionalfeatures.html#oauth
set my_cantab = "imaps://tw466@cantab.ac.uk@outlook.office365.com/"
account-hook "$my_cantab" 'set folder = "$my_cantab"'
account-hook "$my_cantab" 'set smtp_url = "smtp://smtp-mail.outlook.com:587"'
account-hook "$my_cantab" 'set imap_user = "tw466@cantab.ac.uk"'
account-hook "$my_cantab" 'set smtp_user = "tw466@cantab.ac.uk"'
account-hook "$my_cantab" 'set imap_pass = `pass cambridge/raven | head -1`'
account-hook "$my_cantab" 'set smtp_pass = `pass cambridge/raven | head -1`'
account-hook "$my_cantab" 'set spool_file = +INBOX'
account-hook "$my_cantab" 'set postponed = +Drafts'
account-hook "$my_cantab" 'set record = "+Sent Items"'
account-hook "$my_cantab" 'set trash = "+Deleted Items"'
account-hook "$my_cantab" 'set from = "$imap_user"'
account-hook "$my_cantab" 'reset ssl_use_tlsv1 ssl_use_tlsv1_1'
account-hook "$my_cantab" 'set imap_authenticators=xoauth2 smtp_authenticators=xoauth2'
account-hook "$my_cantab" 'set imap_oauth_refresh_command = "mutt_oauth2.py --encryption-pipe=\"gpg --encrypt --recipient timo@twilken.net\" $XDG_DATA_HOME/neomutt/cantab.gpg"'
account-hook "$my_cantab" 'set smtp_oauth_refresh_command = $imap_oauth_refresh_command'

set my_outlook = "imaps://timo_wilken@live.co.uk@outlook.office365.com/"
account-hook "$my_outlook" 'set folder = "$my_outlook"'
account-hook "$my_outlook" 'set smtp_url = "smtp://smtp-mail.outlook.com:587"'
account-hook "$my_outlook" 'set imap_user = "timo_wilken@live.co.uk"'
account-hook "$my_outlook" 'set smtp_user = "timo_wilken@live.co.uk"'
account-hook "$my_outlook" 'set imap_pass = `pass www/microsoft | head -1`'
account-hook "$my_outlook" 'set smtp_pass = `pass www/microsoft | head -1`'
account-hook "$my_outlook" 'set spool_file = +INBOX'
account-hook "$my_outlook" 'set postponed = +Drafts'
account-hook "$my_outlook" 'set record = +Sent'
account-hook "$my_outlook" 'set trash = +Deleted'
account-hook "$my_outlook" 'set from = "$imap_user"'
account-hook "$my_outlook" 'reset ssl_use_tlsv1 ssl_use_tlsv1_1'
account-hook "$my_outlook" 'set imap_authenticators=xoauth2 smtp_authenticators=xoauth2'
account-hook "$my_outlook" 'set imap_oauth_refresh_command = "mutt_oauth2.py --encryption-pipe=\"gpg --encrypt --recipient timo@twilken.net\" $XDG_DATA_HOME/neomutt/outlook.gpg"'
account-hook "$my_outlook" 'set smtp_oauth_refresh_command = $imap_oauth_refresh_command'

macro index,pager gm "<sync-mailbox><enter-command>unmailboxes *<enter><enter-command>unalternates *<enter><enter-command>set folder=$my_mythic<enter><change-folder>+INBOX<enter>" "Switch to Mythic Beasts account"
macro index,pager gG "<sync-mailbox><enter-command>unmailboxes *<enter><enter-command>unalternates *<enter><enter-command>set folder=$my_gmail<enter><change-folder>+INBOX<enter>" "Switch to Gmail account"
macro index,pager gw "<sync-mailbox><enter-command>unmailboxes *<enter><enter-command>unalternates *<enter><enter-command>set folder=$my_cern<enter><change-folder>+INBOX<enter>" "Switch to CERN account"
macro index,pager gc "<sync-mailbox><enter-command>unmailboxes *<enter><enter-command>unalternates *<enter><enter-command>set folder=$my_cantab<enter><change-folder>+INBOX<enter>" "Switch to Cantab account"
macro index,pager go "<sync-mailbox><enter-command>unmailboxes *<enter><enter-command>unalternates *<enter><enter-command>set folder=$my_outlook<enter><change-folder>+INBOX<enter>" "Switch to Outlook account"

# Open the Mythic inbox by default.
set folder = "$my_mythic"
set spool_file = "+INBOX"
