;; This "home-environment" file can be passed to 'guix home reconfigure'
;; to reproduce the content of your profile.  This is "symbolic": it only
;; specifies package names.  To reproduce the exact same profile, you also
;; need to capture the channels being used, as returned by "guix describe".
;; See the "Replicating Guix" section in the manual.

(define-module (tw home lap)
  #:use-module (gnu home)
  #:use-module (gnu home services)
  #:use-module (gnu home services desktop)
  #:use-module (gnu home services guix)
  #:use-module (gnu home services pm)
  #:use-module (gnu home services ssh)
  #:use-module (gnu packages finance)
  #:use-module (gnu services)
  #:use-module (guix gexp)
  #:use-module (tw home)
  #:use-module (tw services desktop)
  #:use-module (tw services dev-env)
  #:use-module (tw services git)
  #:use-module (tw services gnupg)
  #:use-module (tw services restic))

(define-public %lap-home
  (home-environment
    (packages
     ;; These packages will show up in the home profile, under ~/.guix-home/profile.
     ;; Graphical applications
     (list electrum))

    ;; To search for available home services, run 'guix home search KEYWORD'.
    (services
     (cons*
      ;; Batsignal: battery level notifications.
      (service home-batsignal-service-type)

      ;; For `nvidia-service-monitor'.
      (simple-service 'qnvsm-config home-xdg-configuration-files-service-type
        `(("congard/NVSM.conf" ,(local-file "files/NVSM.conf"))))

      (service home-restic-backup-service-type
        (list (restic-scheduled-backup
               (schedule "0 0-23/2 * * *")
               (paths '(;; important user data
                        "~/src"
                        "~/.local/share/zsh/history"
                        ;; secrets
                        "~/.local/share/ssh-keys"
                        "~/.local/share/gnupg"
                        "~/.config/cern-ca-bundle.crt"
                        "~/.config/grid-personal-cert.pem"
                        "~/.config/grid-personal-key.pem"
                        "~/.config/syncthing"
                        ;; games
                        "~/savegames"
                        "~/.local/share/0ad"
                        "~/.local/share/simutrans"
                        "~/.local/share/warzone2100"
                        "~/.local/share/widelands"
                        "~/.local/share/guix-sandbox-home/.local/share/Colossal Order/Cities_Skylines"
                        "~/.local/share/guix-sandbox-home/.local/share/Surviving Mars"
                        "~/.local/share/ksp-overlay/upper"
                        "~/.pioneer"))
               (repo (restic-vin.wg-repo "timo/laptop"))
               (password (restic-pass-key "computers/vin/restic-repos/timo-laptop")))

              (restic-scheduled-backup
               (schedule "10 0-23/2 * * *")   ; try to avoid lock contention
               (paths '("~/documents"
                        "~/sync"
                        "~/audiobooks"
                        "~/music"
                        "~/pictures"
                        "~/videos/youtube/.yt-dlp"
                        "~/videos/youtube/.config"))
               (repo (restic-vin.wg-repo "timo/sync"))
               (password (restic-pass-key "computers/vin/restic-repos/timo-sync")))))

      ;; Redshift: make the screen turn redder at night.
      (service home-redshift-service-type
        (home-redshift-configuration
         (location-provider 'manual)
         ;; Approximate location
         (latitude 46.0)
         (longitude 6.0)
         ;; (location-provider 'geoclue2)  ; TODO: currently waits forever for a location -- not sure why geoclue doesn't work
         (daytime-brightness 1.0)
         (nighttime-brightness 0.7)
         (extra-content "fade=0")))  ; with fade=1, restarting redshift causes flickering for a few secs

      (service home-dbus-service-type)

      (service tw-home-service-type)

      (service home-desktop-service-type
        (home-desktop-configuration
         (nvidia-driver? #t)
         (battery-name "BAT0")
         (ac-adapter-name "AC")
         (monitors
          (list (home-monitor-configuration
                 (name "eDP-1-1")
                 (xrandr-options '("--auto")))))))

      (service home-gaming-service-type
        (home-gaming-configuration
         (nvidia-driver? #t)))

      (service home-full-dev-env-service-type)

      ;; On my private machine, I want to use my private PGP key normally, and
      ;; my work key only for work repositories.
      (service home-git-service-type
        (home-git-configuration
         (default-email "git@twilken.net")
         (default-signing-key "53EC3C06856883DD92355BC22FC78504681F69B0")
         (identities
          (list (home-git-identity
                 (name "alice")
                 (root-directory "~/src/alice")
                 (email "timo.wilken@cern.ch")
                 (signing-key "C2249BBE5E8761C943A0CFA1B7B3914BF63ACD7C"))
                (home-git-identity
                 (name "atlas")
                 (root-directory "~/src/atlas")
                 (email "timo.wilken@cern.ch")
                 (signing-key "C2249BBE5E8761C943A0CFA1B7B3914BF63ACD7C"))))))

      (service home-openssh-service-type
        (tw-openssh-configuration))

      (service home-gnupg-service-type
        (home-gnupg-configuration
         (default-key "53EC3C06856883DD92355BC22FC78504681F69B0")
         (gui-pinentry? #t)))

      %base-home-services))))

%lap-home
