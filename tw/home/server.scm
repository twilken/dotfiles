(define-module (tw home server)
  #:use-module (gnu home)
  #:use-module (gnu services)
  #:use-module (tw home)
  #:use-module (tw services dev-env))

(define-public %server-home
  (home-environment
    (services (cons* (service tw-home-service-type)
                     (service home-basic-dev-env-service-type)
                     %base-home-services))))

%server-home
