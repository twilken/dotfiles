;; This "home-environment" file can be passed to 'guix home reconfigure'
;; to reproduce the content of your profile.  This is "symbolic": it only
;; specifies package names.  To reproduce the exact same profile, you also
;; need to capture the channels being used, as returned by "guix describe".
;; See the "Replicating Guix" section in the manual.

(define-module (tw home frm)
  #:use-module (gnu home)
  #:use-module (gnu home services)
  #:use-module (gnu home services desktop)
  #:use-module (gnu home services guix)
  #:use-module (gnu home services pm)
  #:use-module (gnu home services ssh)
  #:use-module (gnu packages finance)
  #:use-module (gnu services)
  #:use-module (guix gexp)
  #:use-module (tw home)
  #:use-module (tw services desktop)
  #:use-module (tw services dev-env)
  #:use-module (tw services git)
  #:use-module (tw services gnupg)
  #:use-module (tw services restic))

(define-public %frm-home
  (home-environment
    (packages
     ;; These packages will show up in the home profile, under ~/.guix-home/profile.
     ;; Graphical applications
     (list electrum))

    ;; To search for available home services, run 'guix home search KEYWORD'.
    (services
     (cons*
      ;; Batsignal: battery level notifications.
      (service home-batsignal-service-type)

      (service home-dbus-service-type)

      (service tw-home-service-type)

      (service home-wayland-desktop-service-type
        (home-wayland-desktop-configuration
         (num-cores 12)))

      (service home-full-dev-env-service-type)

      (service home-pim-service-type)

      ;; On my private machine, I want to use my private PGP key normally, and
      ;; my work key only for work repositories.
      (service home-git-service-type
        (home-git-configuration
         (default-email "git@twilken.net")
         (default-signing-key "53EC3C06856883DD92355BC22FC78504681F69B0")
         (identities
          (list (home-git-identity
                 (name "alice")
                 (root-directory "~/src/alice")
                 (email "timo.wilken@cern.ch")
                 (signing-key "C2249BBE5E8761C943A0CFA1B7B3914BF63ACD7C"))
                (home-git-identity
                 (name "atlas")
                 (root-directory "~/src/atlas")
                 (email "timo.wilken@cern.ch")
                 (signing-key "C2249BBE5E8761C943A0CFA1B7B3914BF63ACD7C"))))))

      (service home-openssh-service-type
        (tw-openssh-configuration))

      (service home-gnupg-service-type
        (home-gnupg-configuration
         (default-key "53EC3C06856883DD92355BC22FC78504681F69B0")
         (gui-pinentry? #t)))

      (service home-restic-backup-service-type
        (list (restic-scheduled-backup
               (schedule "0 0-23/2 * * *")
               (paths '(;; important user data
                        "~/src"
                        "~/.local/share/zsh/history"
                        ;; secrets
                        "~/.local/share/ssh-keys"
                        "~/.local/share/gnupg"
                        "~/.config/cern-ca-bundle.crt"
                        "~/.config/grid-personal-cert.pem"
                        "~/.config/grid-personal-key.pem"
                        "~/.config/syncthing"))
               (repo (restic-vin.wg-repo "timo/framework"))
               (password (restic-pass-key "computers/vin/restic-repos/timo-framework")))

              (restic-scheduled-backup
               (schedule "30 0-23/2 * * *")   ; try to avoid lock contention
               (paths '("~/documents" "~/sync" "~/pictures"))
               (repo (restic-vin.wg-repo "timo/sync"))
               (password (restic-pass-key "computers/vin/restic-repos/timo-sync")))))

      %base-home-services))))

%frm-home
