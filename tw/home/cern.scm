;; This "home-environment" file can be passed to 'guix home reconfigure'
;; to reproduce the content of your profile.  This is "symbolic": it only
;; specifies package names.  To reproduce the exact same profile, you also
;; need to capture the channels being used, as returned by "guix describe".
;; See the "Replicating Guix" section in the manual.

(define-module (tw home cern)
  #:use-module (gnu home)
  #:use-module (gnu home services)
  #:use-module (gnu home services desktop)
  #:use-module (gnu home services guix)
  #:use-module (gnu home services pm)
  #:use-module (gnu home services ssh)
  #:use-module (gnu packages databases)
  #:use-module (gnu services)
  #:use-module (guix gexp)
  #:use-module (tw home)
  #:use-module (tw gexp)
  #:use-module (tw services desktop)
  #:use-module (tw services dev-env)
  #:use-module (tw services git)
  #:use-module (tw services gnupg))

(define-public %cern-home
  (home-environment
    (packages (list mysql))
    ;; To search for available home services, run 'guix home search KEYWORD'.
    (services
     (cons*
      (simple-service 'remote-desktop-config home-files-service-type
        `((".local/share/vinagre/vinagre-bookmarks.xml" ,(local-file "files/vinagre-bookmarks.xml"))))

      ;; Batsignal: battery level notifications.
      (service home-batsignal-service-type)

      (service home-dbus-service-type)

      (service tw-home-service-type)

      (service home-wayland-desktop-service-type
        (home-wayland-desktop-configuration
         (num-cores 12)))

      (service home-full-dev-env-service-type)

      (service home-pim-service-type
        (home-pim-configuration (work? #t)))

      ;; On my work machine, Git must always use my work PGP key.
      (service home-git-service-type
        (home-git-configuration
         (default-email "timo.wilken@cern.ch")
         (default-signing-key "C2249BBE5E8761C943A0CFA1B7B3914BF63ACD7C")))

      (service home-openssh-service-type
        (tw-openssh-configuration
         ;; There is no separate "private" SSH key.
         #:default-ssh-key "~/.local/share/ssh-keys/cern_id_rsa"))

      (service home-gnupg-service-type
        (home-gnupg-configuration
         (default-key "C2249BBE5E8761C943A0CFA1B7B3914BF63ACD7C")
         (gui-pinentry? #t)))

      %base-home-services))))

%cern-home
