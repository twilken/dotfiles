(define-module (tw packages whiskers)
  #:use-module (gnu packages crates-apple)
  #:use-module (gnu packages crates-check)
  #:use-module (gnu packages crates-compression)
  #:use-module (gnu packages crates-io)
  #:use-module (gnu packages crates-graphics)
  #:use-module (gnu packages crates-gtk)
  #:use-module (gnu packages crates-web)
  #:use-module (gnu packages crates-windows)
  #:use-module (guix build-system cargo)
  #:use-module (guix download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages))

;; Upstream provides 0.9.30, but catppuccin-whiskers version-locks 0.9.34.
(define-public rust-serde-yaml-0.9.34
  (package
    (name "rust-serde-yaml")
    (version "0.9.34+deprecated")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "serde_yaml" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0isba1fjyg3l6rxk156k600ilzr8fp7crv82rhal0rxz5qd1m2va"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-indexmap" ,rust-indexmap-2)
                       ("rust-itoa" ,rust-itoa-1)
                       ("rust-ryu" ,rust-ryu-1)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-unsafe-libyaml" ,rust-unsafe-libyaml-0.2))))
    (home-page "https://github.com/dtolnay/serde-yaml")
    (synopsis "YAML data format for Serde")
    (description "This package provides YAML data format for Serde.")
    (license (list license:expat license:asl2.0))))

(define-public rust-detect-newline-style-0.1
  (package
    (name "rust-detect-newline-style")
    (version "0.1.2")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "detect-newline-style" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0j9pcjk2ab21f36fqybz69whd1c4xy60hy7qd5v59aqm6rfg490i"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-regex" ,rust-regex-1))))
    (home-page "https://github.com/busticated/rusty")
    (synopsis "Determine a string's preferred newline character")
    (description
     "This package provides Determine a string's preferred newline character.")
    (license (list license:expat license:asl2.0))))

(define-public rust-clap-stdin-0.5
  (package
    (name "rust-clap-stdin")
    (version "0.5.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "clap-stdin" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0gw0hpi8d99yqs71c40jjfw4q7l94a25jnyssgkw3grkcs4zf7a7"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-clap" ,rust-clap-4)
                       ("rust-thiserror" ,rust-thiserror-1)
                       ("rust-tokio" ,rust-tokio-1))))
    (home-page "https://github.com/thepacketgeek/clap-stdin")
    (synopsis "Provides a type for easily accepting Clap arguments from stdin")
    (description
     "This package provides a type for easily accepting Clap arguments from stdin.")
    (license (list license:expat license:asl2.0))))

(define-public rust-css-colors-1
  (package
    (name "rust-css-colors")
    (version "1.0.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "css-colors" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0dljfdw4p54drjy9a5m6h5qnvz8lkdllxfkln0vk9wh8azybphi2"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t))
    (home-page "https://github.com/vaidehijoshi/css-colors")
    (synopsis "Rust converter to transform CSS colors.")
    (description
     "This package provides a Rust converter to transform CSS colors.")
    (license license:isc)))

(define-public rust-catppuccin-2
  (package
    (name "rust-catppuccin")
    (version "2.4.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "catppuccin" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1x1vccgzc690g8cxd84dm6xw971ypxdhfkcsgc2qzy6m47m9h5xa"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-ansi-term" ,rust-ansi-term-0.12)
                       ("rust-css-colors" ,rust-css-colors-1)
                       ("rust-itertools" ,rust-itertools-0.13)
                       ("rust-ratatui" ,rust-ratatui-0.26)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-serde-json" ,rust-serde-json-1))))
    (home-page "https://github.com/catppuccin/rust")
    (synopsis "Soothing pastel theme for Rust")
    (description "This package provides Soothing pastel theme for Rust.")
    (license license:expat)))

;; This contains the final binary, so no "rust-" prefix.
(define-public catppuccin-whiskers
  (package
    (name "catppuccin-whiskers")
    (version "2.5.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "catppuccin-whiskers" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0jjh8qkc7a0vn9wshfi6qmn8b03694kdz9r95fgxmw0fpw6vpnjn"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-anyhow" ,rust-anyhow-1)
                       ("rust-base64" ,rust-base64-0.22)
                       ("rust-catppuccin" ,rust-catppuccin-2)
                       ("rust-clap" ,rust-clap-4)
                       ("rust-clap-stdin" ,rust-clap-stdin-0.5)
                       ("rust-css-colors" ,rust-css-colors-1)
                       ("rust-detect-newline-style" ,rust-detect-newline-style-0.1)
                       ("rust-encoding-rs-io" ,rust-encoding-rs-io-0.1)
                       ("rust-indexmap" ,rust-indexmap-2)
                       ("rust-itertools" ,rust-itertools-0.13)
                       ("rust-lzma-rust" ,rust-lzma-rust-0.1)
                       ("rust-rmp-serde" ,rust-rmp-serde-1)
                       ("rust-semver" ,rust-semver-1)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-serde-json" ,rust-serde-json-1)
                       ("rust-serde-yaml" ,rust-serde-yaml-0.9.34)
                       ("rust-tempfile" ,rust-tempfile-3)
                       ("rust-tera" ,rust-tera-1)
                       ("rust-thiserror" ,rust-thiserror-1))
       #:cargo-development-inputs (("rust-assert-cmd" ,rust-assert-cmd-2)
                                   ("rust-predicates" ,rust-predicates-3))))
    (home-page "https://github.com/catppuccin/whiskers/tree/main/whiskers")
    (synopsis "Soothing port creation tool for the high-spirited!")
    (description
     "This package provides Soothing port creation tool for the high-spirited!")
    (license license:expat)))
