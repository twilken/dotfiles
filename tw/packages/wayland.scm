(define-module (tw packages wayland)
  #:use-module (gnu packages freedesktop)
  #:autoload (gnu packages man) (scdoc)
  #:use-module (guix build utils)
  #:use-module (guix build-system meson)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix gexp)
  #:use-module (guix packages))

(define-public xdg-desktop-portal-wlr-latest
  (let ((commit "53aeeba5e5d8c4022b16d9b86ff1558cc73f5391"))
    (package
      (inherit xdg-desktop-portal-wlr)
      (version (git-version "0.7.1" "20" commit))
      (source (origin
                (inherit (package-source xdg-desktop-portal-wlr))  ; for patches
                (method git-fetch)
                (uri (git-reference
                      (url "https://github.com/emersion/xdg-desktop-portal-wlr")
                      (commit commit)))
                (file-name (git-file-name (package-name xdg-desktop-portal-wlr) version))
                (sha256 (base32 "1d9hnqha5i4661k64b3ldf9m6r699ixfpjfwhm19a1gdq8kwx700"))))
      (native-inputs
       (modify-inputs (package-native-inputs xdg-desktop-portal-wlr)
         (append scdoc))))))   ; for manpage
