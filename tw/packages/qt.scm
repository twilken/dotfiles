(define-module (tw packages qt)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages xdisorg)   ; libxkbcommon
  #:use-module (guix build-system qt)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages))

(define-public qt6ct
  (package
    (name "qt6ct")
    (version "0.9")
    (source
     (origin
       (method url-fetch)
       (uri
        (string-append
         "https://github.com/trialuser02/qt6ct/releases/download/"
         version "/qt6ct-" version ".tar.xz"))
       (sha256 (base32 "1kcj6fyvvl75z1xvjychm58bz0x4yb5ia5xd7yrh684wgzdh17qa"))))
    (build-system qt-build-system)
    (arguments
     (list
      #:tests? #f                      ; No target
      #:qtbase qtbase
      #:phases
      #~(modify-phases %standard-phases
          (add-after 'unpack 'patch
            (lambda _
              (substitute* '("src/qt6ct-qtplugin/CMakeLists.txt"
                             "src/qt6ct-style/CMakeLists.txt")
                (("\\$\\{PLUGINDIR\\}")
                 (string-append #$output "/lib/qt6/plugins"))))))))
    (native-inputs (list qttools))
    (inputs (list qtbase qtsvg libxkbcommon))
    (synopsis "Qt6 Configuration Tool")
    (description "Qt6CT is a program that allows users to configure Qt6
settings (such as icons, themes, and fonts) in desktop environments or
window managers that don't provide Qt integration by themselves.")
    (home-page "https://github.com/trialuser02/qt6ct")
    (license license:bsd-2)))
