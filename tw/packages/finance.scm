(define-module (tw packages finance)
  #:use-module (gnu packages databases)
  #:use-module (gnu packages finance)
  #:use-module (gnu packages haskell-xyz)
  #:use-module (gnu packages lisp-check)
  #:use-module (gnu packages lisp-xyz)
  #:use-module (guix build-system haskell)
  #:use-module (guix build-system asdf)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module ((guix licenses) #:prefix license:))

(define-public hledger-dashboard
  (package
    (name "hledger-dashboard")
    (version "1.2.1.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "git://lud.wg/~timo/hledger-dashboard.git")
                    (commit version)))
              (sha256 (base32 "080j3bjf04ypbxkb7gsbpa6nhzsf0yig2jajxivnzwx25k6l5rn1"))))
    (build-system haskell-build-system)
    (inputs (list ghc-postgresql-simple ghc-fsnotify ghc-hledger-lib))
    (home-page #f)
    (synopsis "Feed hledger data into a database")
    (description "Maintain a database containing financial data extracted from
hledger, for use in Grafana, e.g. to create nice graphs.")
    (license license:gpl3+)))

;; Upstream binary-types declares "bt" as a package nickname, but
;; bordeaux-threads also uses that, so there's a conflict at load time:
;; https://github.com/Ferada/cl-inotify/issues/3
;; https://github.com/frodef/binary-types/issues/1
;; Instead, use a fixed version of binary-types.
(define sbcl-binary-types/fix
  (let ((commit "a17caf8890f11fdc56f6ea3a21260bf0fb9c589c"))
    (package
      (inherit sbcl-binary-types)
      (version (git-version "1.2" "2" commit))
      (home-page "https://github.com/Ferada/binary-types")
      (source
       (origin
         (method git-fetch)
         (uri (git-reference (url home-page) (commit commit)))
         (file-name (git-file-name "binary-types" version))
         (sha256
          (base32 "1bh65p9vg2kgh4m8q1a4jiyncnp5prdzh0d0l4pzh3jvfhgbm0gh")))))))

(define sbcl-cl-inotify/fix
  (package
    (inherit sbcl-cl-inotify)
    (inputs (modify-inputs (package-inputs sbcl-cl-inotify)
              (replace "sbcl-binary-types" sbcl-binary-types/fix)))))

(define-public ledgerplot
  (package
    (name "ledgerplot")
    (version "1.2.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://git.twilken.net/ledgerplot")
                    (commit version)))
              (sha256 (base32 "10n8wkwcpb71yhkaqmhbpdfnfhygjfmy3ajl4s6g5gcid48hhxff"))))
    ;; Ledgerplot only works with SBCL.
    (build-system asdf-build-system/sbcl)
    (arguments `(#:phases
                 ,#~(modify-phases %standard-phases
                      (add-after 'unpack 'patch-executable-names
                        (lambda* (#:key inputs #:allow-other-keys)
                          (substitute* (find-files "src" "\\.lisp$")
                            (("\"hledger\"")
                             (string-append "\"" (search-input-file inputs "bin/hledger") "\"")))))
                      (add-after 'create-asdf-configuration 'build-program
                        (lambda* (#:key outputs #:allow-other-keys)
                          (build-program
                           (string-append (assoc-ref outputs "out") "/bin/ledgerplot")
                           outputs
                           #:entry-program '((ledgerplot:main) 0)))))))
    (inputs (list hledger
                  sbcl-bordeaux-threads
                  sbcl-cl-cpus
                  sbcl-local-time
                  sbcl-cl-ppcre
                  sbcl-trivia
                  sbcl-parse-float
                  sbcl-unix-opts
                  sbcl-cl-inotify/fix
                  sbcl-cl-json
                  sbcl-cl-csv
                  sbcl-drakma
                  sbcl-eazy-gnuplot
                  sbcl-md5
                  sbcl-postmodern))
    (native-inputs (list postgresql-15   ; tests bring up a temporary database
                         sbcl-fiveam))
    (home-page "https://cgit.twilken.net/ledgerplot/about/")
    (synopsis "Visualise data from hledger")
    (description "Ledgerplot reads financial data from the hledger plain text
accounting system and processes it into various useful plots.")
    (license license:gpl3+)))

(define sbcl-sxql-latest
  (let ((revision "23")
        (commit "2a9121b32d2745d622e97be45877348850f61f84"))
    (package
      (inherit sbcl-sxql)
      (version (git-version "0.1.0" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/fukamachi/sxql")
               (commit commit)))
         (file-name (git-file-name "cl-sqxl" version))
         (sha256
          (base32 "11x4qgdwbddbk0a8avrirp1ksmphfxlimirfwvmiwi0jc4zd5csa")))))))

;; In order to compile an executable with Woo (without compression), we need
;; to include the following fix: https://github.com/fukamachi/woo/pull/106
(define-public sbcl-woo-latest
  (let ((commit "7f5219c55d49190f5ae17b123a8729b31c5d706e")
        (revision "2"))
    (package
      (inherit sbcl-woo)
      (version (git-version "0.12.0" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/fukamachi/woo")
               (commit commit)))
         (file-name (git-file-name "woo" version))
         (sha256 (base32 "0nhxlb1qhkl20vknm44gx0cq5cks33rcljczfhgbnmpkzrdpdrrl"))))
      (inputs
       (modify-inputs (package-inputs sbcl-woo)
         (append sbcl-trivial-mimes)   ; new in commit cc39809
         (append sbcl-cl+ssl))))))     ; new in commit 223191e

(define-public sbcl-expenses
  (package
    (name "sbcl-expenses")
    (version "1.0.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://git.twilken.net/expenses")
                    (commit version)))
              (sha256 (base32 "1xp1a37n8s95a3nqhyg55j74sg3zn6fmy7l7fg56ysiwwzlscgzh"))))
    (build-system asdf-build-system/sbcl)
    (arguments '(#:phases
                 (modify-phases %standard-phases
                   (add-after 'create-asdf-configuration 'build-program
                     (lambda* (#:key outputs #:allow-other-keys)
                       (build-program
                        (string-append (assoc-ref outputs "out") "/bin/expenses")
                        outputs
                        #:dependencies '("expenses")
                        #:entry-program '((expenses:main))
                        ;; Compression defeats grafting, and Expenses needs
                        ;; the correct paths to refer to files under assets/.
                        #:compress? #f))))))
    (inputs (list sbcl-cl-sqlite
                  sbcl-sxql-latest
                  sbcl-drakma
                  sbcl-woo-latest
                  sbcl-cl-who
                  sbcl-quri
                  sbcl-parse-float
                  sbcl-trivia
                  sbcl-alexandria
                  sbcl-unix-opts
                  cl-ppcre
                  cl-trivial-utf-8))
    (native-inputs (list sbcl-fiveam))
    (home-page "https://cgit.twilken.net/expenses/about/")
    (synopsis "Settle up expenses with a group of people")
    (description "A web app to keep track of expenses for gruops of people
(e.g. for a trip, or a shared flat) and to work out efficient repayment.
Supports multiple currencies.")
    (license license:gpl3+)))
