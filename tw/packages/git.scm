(define-module (tw packages git)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module ((guix utils) #:select (substitute-keyword-arguments))
  #:use-module ((gnu packages version-control) #:select (git)))

(define-public git/unsafe-directories
  (hidden-package   ; Users shouldn't normally want to use this, e.g. for `guix shell'.
   (package
     (inherit git)
     (arguments
      (substitute-keyword-arguments (package-arguments git)
        ((#:tests? _ #t) #f)   ; disable tests since our change is trivial and they take forever
        ((#:phases phases)
         #~(modify-phases #$phases
             (add-after 'install 'override-gitconfig
               (lambda* (#:key outputs #:allow-other-keys)
                 (let* ((/etc (string-append (assoc-ref outputs "out") "/etc"))
                        (/etc/gitconfig (string-append /etc "/gitconfig")))
                   (mkdir-p /etc)
                   (call-with-output-file /etc/gitconfig
                     (lambda (port)
                       (display "[safe]\n\tdirectory = *\n" port)))))))))))))
