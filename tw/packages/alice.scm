(define-module (tw packages alice)
  #:use-module (gnu packages check)
  #:use-module ((gnu packages emacs-xyz)
                #:select (emacs-mmm-mode emacs-yaml-mode))
  #:use-module (gnu packages package-management)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages python-check)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages python-xyz)
  #:use-module (guix build-system emacs)
  #:use-module (guix build-system python)
  #:use-module (guix build-system pyproject)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages))

(define-public python-alibuild
  (package
    (name "python-alibuild")
    (version "1.17.0rc2")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "alibuild" version))
              (sha256 (base32 "0mk38z7pjs9nzanynf729v0i6azs54pn0z4fjj50hlrzqx3jfkxr"))))
    (build-system python-build-system)
    (inputs (list modules))   ; for alienv
    (propagated-inputs (list python-boto3 python-distro python-jinja2
                             python-pyyaml python-requests))
    (native-inputs (list python-setuptools-scm))
    (home-page "https://alisw.github.io/alibuild/")
    (synopsis "ALICE Build Tool")
    (description "ALICE Build Tool")
    (license license:gpl3)))

(define-public python-cerberus
  ;; For python-alidistlint
  (package
    (name "python-cerberus")
    (version "1.3.5")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "Cerberus" version))
              (sha256 (base32 "0hknsk83cjbfs63v73wzsqsb39aq28bn03nmqrp1pxvf4q81w0c1"))))
    (build-system pyproject-build-system)  ; there is no setup.py
    (arguments '(#:tests? #f))             ; pytest finds no tests, and build fails if #t
    (native-inputs (list python-pytest python-pytest-runner python-pytest-benchmark))
    (propagated-inputs (list python-setuptools))
    (home-page "http://docs.python-cerberus.org")
    (synopsis "Lightweight, extensible schema and data validation tool for Python dictionaries.")
    (description "Lightweight, extensible schema and data validation tool for Python dictionaries.")
    (license #f)))

(define-public python-alidistlint
  (package
    (name "python-alidistlint")
    (version "1.6.0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "alidistlint" version))
              (sha256 (base32 "159a3hrzn0pmgws46i3sn55jhkh6pzxjy3r1ycj8j2rc7aqr9j2w"))))
    (build-system pyproject-build-system)  ; we don't have setup.py, only pyproject.toml
    (arguments '(#:tests? #f))             ; there are no tests, and build fails if #t
    (propagated-inputs (list python-cerberus python-pyyaml))
    (native-inputs (list python-setuptools-scm))
    (home-page "https://github.com/TimoWilken/alidistlint")
    (synopsis "A code linter for alidist packages")
    (description "This package provides a code linter for alidist packages.")
    (license license:gpl3)))

(define-public emacs-alidist-mode
  (package
    (name "emacs-alidist-mode")
    (version "1.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/TimoWilken/alidist-mode")  ; TODO: use git.twilken.net
                    (commit (string-append "v" version))))
              (sha256 (base32 "0hs0zs2dm5av982x2ya8qy9n7yy43j4yzkwwf41lm641yigqiz7m"))))
    (build-system emacs-build-system)
    ;; The custom, flymake and sh-script packages are part of Emacs.
    (propagated-inputs (list emacs-mmm-mode emacs-yaml-mode))
    (home-page "https://cgit.twilken.net/alidist-mode/about/")
    (synopsis "Emacs major mode for ALICE alidist packages.")
    (description "This Emacs major mode nicely highlights the ALICE
experiment's @url{https://github.com/alisw/alidist, alidist} recipes nicely,
using @code{yaml-mode} for their metadata headers and @code{sh-mode} for any
embedded shell scripts.

It also integrates with flymake to show messages from
@url{https://github.com/TimoWilken/alidistlint, alidistlint}.")
    (license license:gpl3+)))
