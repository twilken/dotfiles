(define-module (tw packages zoom)
  #:use-module (ice-9 match)
  #:use-module (nongnu packages messaging)
  #:use-module (guix gexp)
  #:use-module (guix packages))

;; One Wayland, Zoom needs two extra environment variables to be set:
;; - XDG_CURRENT_DESKTOP=GNOME (to get past the refusal to try sharing the screen)
;; - QT_QPA_PLATFORM= (since QT_QPA_PLATFORM=wayland makes zoom fail to start)
;; The following package definition patches it to set those variables.

(define plist-delete
  (match-lambda*
    ((() _) '())
    (((unpaired-item) _)
     (error "Found unpaired item in plist" unpaired-item))
    (((key-to-delete _ . rest) key-to-delete)
     (plist-delete rest key-to-delete))
    (((key value . rest) key-to-delete)
     (cons* key value (plist-delete rest key-to-delete)))))

(define-public zoom/wayland
  (package
    (inherit zoom)
    (arguments
     (apply
      (lambda* (#:key phases #:allow-other-keys . args)
        (cons*
         #:phases
         #~(modify-phases #$phases
             (replace 'wrap-where-patchelf-does-not-work
               (lambda _
                 (wrap-program (string-append #$output "/lib/zoom/zopen")
                   `("LD_LIBRARY_PATH" prefix
                     ,(list #$@(map (lambda (pkg)
                                      (file-append (this-package-input pkg) "/lib"))
                                    '("fontconfig-minimal"
                                      "freetype"
                                      "gcc"
                                      "glib"
                                      "libxcomposite"
                                      "libxdamage"
                                      "libxkbcommon"
                                      "libxkbfile"
                                      "libxrandr"
                                      "libxrender"
                                      "zlib")))))
                 (wrap-program (string-append #$output "/lib/zoom/zoom")
                   '("XDG_CURRENT_DESKTOP" = ("GNOME")) ; tw: added
                   '("QT_QPA_PLATFORM" = ())            ; tw: added
                   '("QML2_IMPORT_PATH" = ())
                   '("QT_PLUGIN_PATH" = ())
                   '("QT_SCREEN_SCALE_FACTORS" = ())
                   `("FONTCONFIG_PATH" ":" prefix
                     (,(string-join
                        (list
                         (string-append #$(this-package-input "fontconfig-minimal") "/etc/fonts")
                         #$output)
                        ":")))
                   `("LD_LIBRARY_PATH" prefix
                     ,(list (string-append #$(this-package-input "nss") "/lib/nss")
                            #$@(map (lambda (pkg)
                                      (file-append (this-package-input pkg) "/lib"))
                                    ;; TODO: Reuse this long list as it is
                                    ;; needed for aomhost.  Or perhaps
                                    ;; aomhost has a shorter needed list,
                                    ;; but untested.
                                    '("alsa-lib"
                                      "atk"
                                      "at-spi2-atk"
                                      "at-spi2-core"
                                      "cairo"
                                      "cups"
                                      "dbus"
                                      "eudev"
                                      "expat"
                                      "gcc"
                                      "glib"
                                      "mesa"
                                      "mit-krb5"
                                      "nspr"
                                      "libxcb"
                                      "libxcomposite"
                                      "libxdamage"
                                      "libxext"
                                      "libxkbcommon"
                                      "libxkbfile"
                                      "libxrandr"
                                      "libxshmfence"
                                      "pango"
                                      "pulseaudio"
                                      "xcb-util"
                                      "xcb-util-image"
                                      "xcb-util-keysyms"
                                      "xcb-util-wm"
                                      "xcb-util-renderutil"
                                      "zlib")))))
                 (wrap-program (string-append #$output "/lib/zoom/aomhost")
                   `("FONTCONFIG_PATH" ":" prefix
                     (,(string-join
                        (list
                         (string-append #$(this-package-input "fontconfig-minimal") "/etc/fonts")
                         #$output)
                        ":")))
                   `("LD_LIBRARY_PATH" prefix
                     ,(list (string-append #$(this-package-input "nss") "/lib/nss")
                            #$@(map (lambda (pkg)
                                      (file-append (this-package-input pkg) "/lib"))
                                    '("alsa-lib"
                                      "atk"
                                      "at-spi2-atk"
                                      "at-spi2-core"
                                      "cairo"
                                      "cups"
                                      "dbus"
                                      "eudev"
                                      "expat"
                                      "gcc"
                                      "glib"
                                      "mesa"
                                      "mit-krb5"
                                      "nspr"
                                      "libxcb"
                                      "libxcomposite"
                                      "libxdamage"
                                      "libxext"
                                      "libxkbcommon"
                                      "libxkbfile"
                                      "libxrandr"
                                      "libxshmfence"
                                      "pango"
                                      "pulseaudio"
                                      "xcb-util"
                                      "xcb-util-image"
                                      "xcb-util-keysyms"
                                      "xcb-util-wm"
                                      "xcb-util-renderutil"
                                      "zlib"))))))))
         ;; ARGS still contains the old "#:phases ..." entry, so delete it.
         (plist-delete args #:phases)))
      (package-arguments zoom)))))
