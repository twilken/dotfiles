(define-module (tw packages monitoring)
  #:use-module (gnu packages golang)
  #:use-module (gnu packages golang-build)
  #:use-module (gnu packages golang-check)
  #:use-module (gnu packages golang-compression)
  #:use-module (gnu packages golang-crypto)
  #:use-module (gnu packages golang-web)
  #:use-module (gnu packages golang-xyz)
  #:use-module (gnu packages prometheus)
  #:use-module (gnu packages syncthing)
  #:use-module (guix build-system copy)
  #:use-module (guix build-system go)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix utils))


;;; Syncthing exporter

(define-public go-github-com-syncthing-syncthing
  (package
    (inherit syncthing)
    (name "go-github-com-syncthing-syncthing")
    (arguments
     (substitute-keyword-arguments (package-arguments syncthing)
       ((#:install-source? _ #f) #t)
       ((#:phases phases)
        #~(modify-phases #$phases
            (replace 'install
              (assoc-ref %standard-phases 'install))
            (add-after 'install 'cleanup-binaries  ; these are in the main syncthing package already
              (lambda _
                (let ((src (string-append #$output "/src/github.com/syncthing/syncthing")))
                  (delete-file-recursively (string-append src "/bin"))
                  (for-each (lambda (binary)
                              (delete-file (string-append src "/" binary)))
                            '("ursrv" "stupgrades" "strelaypoolsrv" "stcrashreceiver")))))
            (delete 'install-docs)))))))   ; don't need man pages

(define-public syncthing-exporter
  (package
    (name "syncthing-exporter")
    (version "0.3.8")
    (home-page "https://github.com/f100024/syncthing_exporter")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference (url home-page) (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256 (base32 "0igrb9jbb2fppasbsgn58krkw1v43yl7l0xskxl4mylwnmx1rrwk"))))
    (build-system go-build-system)
    (arguments `(#:go ,go-1.23
                 #:install-source? #f
                 #:import-path "github.com/f100024/syncthing_exporter"))
    (propagated-inputs (list go-github-com-syncthing-syncthing
                             go-github-com-prometheus-common
                             go-github-com-prometheus-client-golang
                             go-github-com-go-kit-log
                             go-github-com-alecthomas-kingpin-v2))
    (synopsis "Syncthing metrics exporter for Prometheus")
    (description "Scrape metrics from a Syncthing server, exposing them to
Prometheus.")
    (license license:expat)))


;;; Minecraft monitoring

(define-public go-github-com-raqbit-mc-pinger
  (package
    (name "go-github-com-raqbit-mc-pinger")
    (version "0.2.4")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/raqbit/mc-pinger")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256 (base32 "0jzqbg2yw9n0bnrby6lg9807cd41h5bcprxx2yjr6fcwz953vzdp"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/Raqbit/mc-pinger"))
    (propagated-inputs (list go-github-com-pires-go-proxyproto))
    (home-page "https://github.com/Raqbit/mc-pinger")
    (synopsis "Minecraft server pinger")
    (description "This package provides an interface for pinging Minecraft
servers via the @url{https://wiki.vg/Server_List_Ping,Server List Ping}
interface.")
    (license license:expat)))

(define-public go-github-com-itzg-go-flagsfiller
  (package
    (name "go-github-com-itzg-go-flagsfiller")
    (version "1.14.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/itzg/go-flagsfiller")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256 (base32 "1jwzkln0r3h0yf9m9xagd60b9jpibizrvisa1q2ghjl0w3dq1bl3"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/itzg/go-flagsfiller"))
    (propagated-inputs (list go-github-com-stretchr-testify
                             go-github-com-iancoleman-strcase))
    (home-page "https://github.com/itzg/go-flagsfiller")
    (synopsis "Map Go structs into flags")
    (description "Make Go's flag package pleasant to use by mapping the fields
of a given struct into flags in a @code{FlagSet}.")
    (license license:expat)))

(define-public go-github-com-influxdata-line-protocol
  (package
    (name "go-github-com-influxdata-line-protocol")
    (version "0.0.0-20210922203350-b1ad95c89adf")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/influxdata/line-protocol")
             (commit (go-version->git-ref version))))
       (file-name (git-file-name name version))
       (sha256 (base32 "001bprw364wp1rp81kd4kcd9289h5xr7byhs6989dmkk1mpvnn6g"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/influxdata/line-protocol"))
    (home-page "https://github.com/influxdata/line-protocol")
    (synopsis "InfluxDB line protocol encoder")
    (description "An encoder for the InfluxDB
@url{https://docs.influxdata.com/influxdb/latest/reference/syntax/line-protocol/,
line protocol.}.")
    (license license:expat)))

(define-public go-github-com-itzg-line-protocol-sender
  (package
    (name "go-github-com-itzg-line-protocol-sender")
    (version "0.1.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/itzg/line-protocol-sender")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256 (base32 "1rwifagrnnal3gqd4vahw6iwgin6sspvbz7fnlqcbklknm6lvd1p"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/itzg/line-protocol-sender"))
    (propagated-inputs (list go-github-com-stretchr-testify
                             go-github-com-influxdata-line-protocol))
    (home-page "https://github.com/itzg/line-protocol-sender")
    (synopsis "InfluxDB line protocol client")
    (description "A client that sends Influx line protocol metrics to a TCP
endpoint.  The client provides options for batching metrics by size and/or
timeout.")
    (license license:expat)))

(define-public go-github-com-itzg-zapconfigs
  (package
    (name "go-github-com-itzg-zapconfigs")
    (version "0.1.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/itzg/zapconfigs")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256 (base32 "0pi9p7b9xdx3ma4hmp8l98lnwb533cbkgm4vh1l7rp82q7s5szl2"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/itzg/zapconfigs"))
    (propagated-inputs (list go-go-uber-org-zap))
    (home-page "https://github.com/itzg/zapconfigs")
    (synopsis "Advanced zap logger configurations")
    (description "Provides a few more opinionated zap logger configurations
beyond the ones provided by their package.")
    (license license:expat)))

;; TODO: use upstream go-go-opentelemetry-io-otel@1.30.0 instead, if possible
;; Though the upstream package deletes a lot of subpackages and packages them
;; separately (in theory).
(define-public go-go-opentelemetry-io-otel
  (package
    (name "go-go-opentelemetry-io-otel")
    (version "1.31.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/open-telemetry/opentelemetry-go")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256 (base32 "0c0qzgi57nmzz4160c81x4m6wdjnsdi1lzm4rgasdx33f4hy3q55"))))
    (build-system go-build-system)
    (arguments `(#:go ,go-1.22
                 #:tests? #f   ; tests require a lot more packages
                 #:import-path "go.opentelemetry.io/otel"))
    (propagated-inputs (list go-golang-org-x-sys
                             go-github-com-stretchr-testify
                             go-github-com-google-go-cmp
                             go-github-com-google-uuid
                             go-github-com-go-logr-stdr
                             go-github-com-go-logr-logr
                             go-github-com-cenkalti-backoff-v4
                             go-google-golang-org-grpc
                             go-go-opentelemetry-io-proto-otlp))
    (home-page "https://go.opentelemetry.io/otel")
    (synopsis "OpenTelemetry API implementation")
    (description "Provides global access to the @code{OpenTelemetry} API. The
subpackages of the otel package provide an implementation of the
@code{OpenTelemetry} API.")
    (license license:asl2.0)))

(define-public go-go-opentelemetry-io-contrib-instrumentation-runtime
  (package
    (name "go-go-opentelemetry-io-contrib-instrumentation-runtime")
    (version "0.56.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/open-telemetry/opentelemetry-go-contrib")
             ;; https://github.com/open-telemetry/opentelemetry-go-contrib/releases/tag/v1.31.0
             (commit "v1.31.0")))   ; yes, this is silly
       (file-name (git-file-name name version))
       (sha256 (base32 "15gnwa593gp32yw4i1pqbir519pa0jcvwj1pd7gz048k3bdsmr12"))))
    (build-system go-build-system)
    (arguments `(#:go ,go-1.22
                 #:import-path "go.opentelemetry.io/contrib/instrumentation/runtime"
                 #:unpack-path "go.opentelemetry.io/contrib"))
    (propagated-inputs (list go-github-com-stretchr-testify
                             go-go-opentelemetry-io-otel))
    (home-page "https://go.opentelemetry.io/contrib/instrumentation/runtime")
    (synopsis "Runtime metrics implementation for OpenTelemetry")
    (description "Implements the conventional runtime metrics specified by
OpenTelemetry.")
    (license license:asl2.0)))

(define-public go-github-com-sandertv-go-raknet
  (package
    (name "go-github-com-sandertv-go-raknet")
    (version "1.14.2")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/Sandertv/go-raknet")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256 (base32 "0rcqdl988dlzi5l3cjf7dy6n6prfm91ssn9hza3945qkp9cjsi2g"))))
    (build-system go-build-system)
    (arguments `(#:go ,go-1.22
                 #:tests? #f   ; tests try to connect to hosts on the internet
                 #:import-path "github.com/sandertv/go-raknet"))
    (home-page "https://github.com/sandertv/go-raknet")
    (synopsis "RakNet protocol implementation")
    (description "This library implements a basic version of the @code{RakNet}
protocol, which is used for Minecraft (Bedrock Edition).  It implements
Unreliable, Reliable and @code{ReliableOrdered} packets and sends user packets
as @code{ReliableOrdered}.")
    (license license:expat)))

(define-public go-github-com-xrjr-mcutils
  (package
    (name "go-github-com-xrjr-mcutils")
    (version "1.5.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/xrjr/mcutils")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256 (base32 "0yhx9va7v5grg63qs9y4icxzybbfifqq78fk10r3vfxd9lngldf9"))))
    (build-system copy-build-system)
    (arguments `(#:install-plan
                 '(("./" "src/github.com/xrjr/mcutils/"
                    #:include-regexp ("\\.go$")
                    #:exclude-regexp ("_test\\.go$"))
                   ("LICENSE" ,(string-append "share/doc/" name "-" version "/")))))
    (home-page "https://github.com/xrjr/mcutils")
    (synopsis "Implementation of Minecraft protocols in Go")
    (description "All protocols are implemented in Go, without any external
dependency.  All protocols should be supported on any platform/architecture as
long as Go can compile them.")
    (license license:expat)))

(define-public go-github-com-census-instrumentation-opencensus-proto
  (package
    (name "go-github-com-census-instrumentation-opencensus-proto")
    (version "0.4.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/census-instrumentation/opencensus-proto")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256 (base32 "0zda7v8fqbc2hamnwajdwr9742nznajxgcw636n570v8k5ahrymw"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/census-instrumentation/opencensus-proto"))
    (propagated-inputs (list go-google-golang-org-protobuf
                             go-google-golang-org-grpc
                             go-github-com-grpc-ecosystem-grpc-gateway-v2))
    (home-page "https://github.com/census-instrumentation/opencensus-proto")
    (synopsis "Language Independent Interface Types For OpenCensus")
    (description "Provides a framework to define and collect stats against
metrics and to break those stats down across user-defined dimensions.")
    (license license:asl2.0)))

(define-public go-cel-dev-expr
  (package
    (name "go-cel-dev-expr")
    (version "0.17.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/google/cel-spec")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256 (base32 "1lvf8iz3g67gkwcrdhcc886r6nr59sc7yz3h7k5jqin2r5xq1kbb"))))
    (build-system go-build-system)
    (arguments '(#:import-path "cel.dev/expr"))
    (propagated-inputs (list go-google-golang-org-protobuf
                             go-google-golang-org-genproto-googleapis-rpc
                             go-google-golang-org-genproto-googleapis-api))
    (home-page "https://cel.dev/expr")
    (synopsis "Common Expression Language")
    (description "The Common Expression Language (CEL) implements common
semantics for expression evaluation, enabling different applications to more
easily interoperate.")
    (license license:asl2.0)))

(define-public go-github-com-cncf-xds-go
  (package
    (name "go-github-com-cncf-xds-go")
    (version "0.0.0-20240905190251-b4127c9b8d78")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/cncf/xds")
             (commit (go-version->git-ref version))))
       (file-name (git-file-name name version))
       (sha256 (base32 "04wg9722v7mgn4ndkwbahcpxkhx6hw842h2r1qfc6xrryp8l13hr"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/cncf/xds/go"
                 #:unpack-path "github.com/cncf/xds"))
    (propagated-inputs (list go-google-golang-org-protobuf
                             go-google-golang-org-grpc
                             go-google-golang-org-genproto-googleapis-api
                             go-github-com-envoyproxy-protoc-gen-validate
                             go-cel-dev-expr))
    (home-page "https://github.com/cncf/xds")
    (synopsis #f)
    (description #f)
    (license license:asl2.0)))

(define-public go-github-com-envoyproxy-protoc-gen-validate
  (package
    (name "go-github-com-envoyproxy-protoc-gen-validate")
    (version "1.1.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/bufbuild/protoc-gen-validate")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256 (base32 "0yw8r45ykziz3bkfxi8y15kdakip8rjr2r2mqyx8ld8c12mcr3j1"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/envoyproxy/protoc-gen-validate"))
    (propagated-inputs (list go-google-golang-org-protobuf go-golang-org-x-net
                             go-github-com-lyft-protoc-gen-star-v2
                             go-github-com-iancoleman-strcase))
    (home-page "https://github.com/envoyproxy/protoc-gen-validate")
    (synopsis "Generate polyglot protoc message validators")
    (description "PGV is a protoc plugin to generate polyglot message
validators.  While protocol buffers effectively guarantee the types of
structured data, they cannot enforce semantic rules for values.  This plugin
adds support to protoc-generated code to validate such constraints.")
    (license license:asl2.0)))

(define-public go-github-com-planetscale-vtprotobuf
  (package
    (name "go-github-com-planetscale-vtprotobuf")
    (version "0.6.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/planetscale/vtprotobuf")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256 (base32 "0bms8rrg8wrm3x9mspqrzzix24vjgi3p5zzbw108ydr1rnarwblf"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/planetscale/vtprotobuf"))
    (propagated-inputs (list go-google-golang-org-protobuf
                             go-google-golang-org-grpc
                             go-github-com-stretchr-testify))
    (home-page "https://github.com/planetscale/vtprotobuf")
    (synopsis "Vitess Protocol Buffers compiler")
    (description "This repository provides the @code{protoc-gen-go-vtproto}
plug-in for @code{protoc}, which is used by Vitess to generate optimized
marshall & unmarshal code.")
    (license license:bsd-3)))

(define-public go-github-com-rogpeppe-fastuuid
  (package
    (name "go-github-com-rogpeppe-fastuuid")
    (version "1.2.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/rogpeppe/fastuuid")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256 (base32 "028acdg63zkxpjz3l639nlhki2l0canr2v5jglrmwa1wpjqcfff8"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/rogpeppe/fastuuid"))
    (home-page "https://github.com/rogpeppe/fastuuid")
    (synopsis "Fast 192-bit UUID generation")
    (description "Package fastuuid provides fast UUID generation of 192 bit
universally unique identifiers.")
    (license license:bsd-3)))

(define-public go-github-com-grpc-ecosystem-grpc-gateway-v2
  (package
    (name "go-github-com-grpc-ecosystem-grpc-gateway-v2")
    (version "2.22.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/grpc-ecosystem/grpc-gateway")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256 (base32 "0ppavf2wzgi090xkmcwzygdias90iixzjjaw3cqrzs3lyn0kfp13"))))
    ;; (build-system go-build-system)
    ;; (arguments '(#:import-path "github.com/grpc-ecosystem/grpc-gateway/v2"))
    (build-system copy-build-system)
    (arguments `(#:install-plan
                 '(("./" "src/github.com/grpc-ecosystem/grpc-gateway/v2/"
                    #:include-regexp ("\\.go$")
                    #:exclude-regexp ("_test\\.go$"))
                   ("LICENSE" ,(string-append "share/doc/" name "-" version "/")))))
    (propagated-inputs (list go-gopkg-in-yaml-v3
                             go-google-golang-org-protobuf
                             go-google-golang-org-grpc
                             go-google-golang-org-genproto-googleapis-rpc
                             go-google-golang-org-genproto-googleapis-api
                             go-golang-org-x-text
                             go-golang-org-x-oauth2
                             go-github-com-rogpeppe-fastuuid
                             go-github-com-google-go-cmp
                             go-github-com-antihax-optional))
    (home-page "https://github.com/grpc-ecosystem/grpc-gateway")
    (synopsis "HTTP-to-gRPC converting reverse proxy")
    (description "The @code{gRPC-Gateway} is a plugin of the Google protocol
buffers compiler @url{https://github.com/protocolbuffers/protobuf,protoc}.  It
reads protobuf service definitions and generates a reverse-proxy server which
translates a RESTful HTTP API into @code{gRPC}.  This server is generated
according to the
@url{https://github.com/googleapis/googleapis/raw/master/google/api/http.proto#L46,
(code google.api.http)} annotations in your service definitions.")
    (license license:bsd-3)))

(define-public go-go-opentelemetry-io-proto-otlp
  (package
    (name "go-go-opentelemetry-io-proto-otlp")
    (version "1.3.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/open-telemetry/opentelemetry-proto-go")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256 (base32 "1hczl3xbkszf6rggbkail9z0ahm2vyfmc7i5ysp1v1whxpxgvy7j"))))
    (build-system copy-build-system)
    (arguments `(#:install-plan
                 '(("otlp/" "src/go.opentelemetry.io/proto/otlp/"
                    #:include-regexp ("\\.go$")
                    #:exclude-regexp ("_test\\.go$"))
                   ("LICENSE" ,(string-append "share/doc/" name "-" version "/")))))
    (propagated-inputs (list go-google-golang-org-protobuf
                             go-google-golang-org-grpc
                             go-github-com-grpc-ecosystem-grpc-gateway-v2))
    (home-page "https://go.opentelemetry.io/proto/otlp")
    (synopsis #f)
    (description #f)
    (license license:asl2.0)))

(define-public go-github-com-envoyproxy-go-control-plane
(package
(name "go-github-com-envoyproxy-go-control-plane")
(version "0.13.1")
(source
 (origin
   (method git-fetch)
   (uri (git-reference
         (url "https://github.com/envoyproxy/go-control-plane")
         (commit (string-append "v" version))))
   (file-name (git-file-name name version))
   (sha256 (base32 "0wq3v5w5svk2aqnz264sq9nl79x4ag2f9w8s2s74s6y0an74fshq"))))
(build-system go-build-system)
(arguments '(#:import-path "github.com/envoyproxy/go-control-plane"))
(propagated-inputs (list go-google-golang-org-protobuf
                         go-google-golang-org-grpc
                         go-google-golang-org-genproto-googleapis-rpc
                         go-google-golang-org-genproto-googleapis-api
                         go-go-uber-org-goleak
                         go-go-opentelemetry-io-proto-otlp
                         go-github-com-stretchr-testify
                         go-github-com-prometheus-client-model
                         go-github-com-planetscale-vtprotobuf
                         go-github-com-google-go-cmp
                         go-github-com-envoyproxy-protoc-gen-validate
                         go-github-com-cncf-xds-go
                         go-github-com-census-instrumentation-opencensus-proto))
(home-page "https://github.com/envoyproxy/go-control-plane")
(synopsis "Implementation of the data-plane-api server")
(description "A Go-based implementation of an API server that implements
the discovery service APIs defined in
@url{https://github.com/envoyproxy/data-plane-api, data-plane-api}.")
(license license:asl2.0)))

(define-public go-google-golang-org-genproto-googleapis-rpc
  (package
    (name "go-google-golang-org-genproto-googleapis-rpc")
    (version "0.0.0-20241021214115-324edc3d5d38")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/googleapis/go-genproto")
             (commit (go-version->git-ref version))))
       (file-name (git-file-name name version))
       (sha256 (base32 "1r75klcygk51r5nld1s0xib1sgk9v2fg4p2nzqsnx9vw6hrsjxrm"))))
    ;; (build-system go-build-system)
    ;; (arguments '(#:import-path "google.golang.org/genproto/googleapis/rpc"))
    (build-system copy-build-system)
    (arguments `(#:install-plan
                 '(("googleapis/rpc/" "src/google.golang.org/genproto/googleapis/rpc/"
                    #:include-regexp ("\\.go$")
                    #:exclude-regexp ("_test\\.go$"))
                   ("LICENSE" ,(string-append "share/doc/" name "-" version "/")))))
    (propagated-inputs (list go-google-golang-org-protobuf))
    (home-page "https://google.golang.org/genproto/googleapis/rpc")
    (synopsis #f)
    (description #f)
    (license license:asl2.0)))

(define-public go-google-golang-org-genproto-googleapis-api
  (package
    (name "go-google-golang-org-genproto-googleapis-api")
    (version "0.0.0-20241021214115-324edc3d5d38")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/googleapis/go-genproto")
             (commit (go-version->git-ref version))))
       (file-name (git-file-name name version))
       (sha256 (base32 "1r75klcygk51r5nld1s0xib1sgk9v2fg4p2nzqsnx9vw6hrsjxrm"))))
    ;; (build-system go-build-system)
    ;; (arguments '(#:import-path "google.golang.org/genproto/googleapis/api"))
    (build-system copy-build-system)
    (arguments `(#:install-plan
                 '(("googleapis/api/" "src/google.golang.org/genproto/googleapis/api/"
                    #:include-regexp ("\\.go$")
                    #:exclude-regexp ("_test\\.go$"))
                   ("LICENSE" ,(string-append "share/doc/" name "-" version "/")))))
    (propagated-inputs (list go-google-golang-org-protobuf
                             go-google-golang-org-grpc
                             go-google-golang-org-genproto-googleapis-rpc))
    (home-page "https://google.golang.org/genproto/googleapis/api")
    (synopsis #f)
    (description #f)
    (license license:asl2.0)))

(define-public go-google-golang-org-grpc
  (package
    (name "go-google-golang-org-grpc")
    (version "1.67.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/grpc/grpc-go")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256 (base32 "0fjnk31yxnhjwfh9lf593cdk49933xfqarqiyrsy43vm0ga3jvss"))))
    (build-system go-build-system)
    (arguments '(#:import-path "google.golang.org/grpc"
                 #:tests? #f))          ; tests need lots more complex packages
                                        ; incl. go-github-com-envoyproxy-go-control-plane,
                                        ; which is a dependency cycle
    (propagated-inputs (list go-google-golang-org-protobuf
                             go-google-golang-org-genproto-googleapis-rpc
                             go-golang-org-x-sys
                             go-golang-org-x-sync
                             go-golang-org-x-oauth2
                             go-golang-org-x-net
                             go-github-com-google-uuid
                             go-github-com-google-go-cmp
                             go-github-com-golang-glog
                             go-github-com-cespare-xxhash-v2))
    (home-page "https://google.golang.org/grpc")
    (synopsis "gRPC-Go")
    (description "Package grpc implements an RPC system called @code{gRPC}.")
    (license license:asl2.0)))

(define-public mc-monitor
  (package
    (name "mc-monitor")
    (version "0.14.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/itzg/mc-monitor")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256 (base32 "05vi7igfcajazrdskaskimsvns4dzl67zcsiikgn69g8r976k4hf"))))
    (build-system go-build-system)
    (arguments `(#:go ,go-1.22
                 #:import-path "github.com/itzg/mc-monitor"
                 #:install-source? #f))
    (propagated-inputs (list
                        go-github-com-raqbit-mc-pinger
                        go-github-com-avast-retry-go
                        go-github-com-google-subcommands
                        go-github-com-itzg-go-flagsfiller
                        go-github-com-itzg-line-protocol-sender
                        go-github-com-itzg-zapconfigs
                        go-github-com-prometheus-client-golang
                        go-github-com-sandertv-go-raknet
                        go-github-com-stretchr-testify
                        go-github-com-xrjr-mcutils
                        go-go-opentelemetry-io-contrib-instrumentation-runtime
                        go-go-opentelemetry-io-otel
                        go-go-uber-org-zap))
    (home-page "https://github.com/itzg/go-mc-status")
    (synopsis "Monitor the status of Minecraft servers.")
    (description "Command/agent to monitor the status of Minecraft servers.
Provides Telegraf, Prometheus and Open Telemetry integration.")
    (license license:expat)))
