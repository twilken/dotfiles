(define-module (tw packages php)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages fonts)
  #:use-module (gnu packages imagemagick)
  #:use-module (gnu packages pcre)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages php)
  #:use-module (gnu packages re2c)
  #:use-module (guix build-system gnu)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages))

(define-public php-apcu
  (package
    (name "php-apcu")
    (version "5.1.23")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "https://pecl.php.net/get/apcu-" version ".tgz"))
       (sha256 (base32 "10j4c1fh24va1ii24ygazr286vfgvf70pssazb1ka8xdrij79vk7"))))
    (build-system gnu-build-system)
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (add-before 'configure 'phpize
           (lambda _
             (invoke "phpize")))
         (add-before 'check 'set-env-vars-for-check
           (lambda _
             (setenv "REPORT_EXIT_STATUS" "1")
             (setenv "NO_INTERACTION" "1")
             (setenv "SKIP_ONLINE_TESTS" "1")
             (setenv "SKIP_SLOW_TESTS" "1")))
         (replace 'install
           (lambda* (#:key inputs outputs #:allow-other-keys)
             (use-modules (ice-9 popen)
                          (ice-9 rdelim))
             (let* ((out (assoc-ref outputs "out"))
                    (php-config (string-append (assoc-ref inputs "php")
                                               "/bin/php-config"))
                    (pipe (open-pipe* OPEN_READ php-config "--extension-dir"))
                    (php-extdir (read-line pipe)))
               (unless (zero? (status:exit-val (close-pipe pipe)))
                 (error "Failed to get PHP extension dir"))
               (invoke
                "make" "phpincludedir=include/php"
                ;; Stop make install from trying to install into /gnu/store/*-php-7.4.*.
                ;; In order to find this extension, PHP must be configured with
                ;; extension_dir = <profile>/lib/php/extensions/<dir> in php.ini.
                (string-append "INSTALL_ROOT=" out "/")
                (string-append "EXTENSION_DIR=lib/php/extensions/"
                               (basename php-extdir))
                "install")))))
       #:test-target "test"
       #:configure-flags '("--enable-apcu")))
    (inputs (list php pcre2))
    (native-inputs (list autoconf))  ; for phpize
    (home-page "https://pecl.php.net/package/apcu")
    (synopsis "A userland caching module for PHP")
    (description "APCu is an in-memory key-value store for PHP.  Keys are of
type string and values can be any PHP variables.

APCu only supports userland caching of variables.

APCu is APC stripped of opcode caching.  See
@url{https://github.com/krakjoe/apcu-bc, APCu Backwards Compatibility Module}
which provides a drop in replacement for APC.")
    (license (license:non-copyleft "file://LICENSE"))))

(define-public php-imagick
  (package
    (name "php-imagick")
    (version "3.7.0")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "https://pecl.php.net/get/imagick-" version ".tgz"))
       (sha256 (base32 "0gidg4qnbh156kk4azr286vfk2r4bghq4bmvphjd4ach21a46djs"))))
    (build-system gnu-build-system)
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (add-before 'configure 'phpize
           (lambda _
             (invoke "phpize")))
         (add-before 'check 'set-env-vars-for-check
           (lambda _
             (setenv "NO_INTERACTION" "1")
             ;; We need to patch fonts.conf to point to at least one writable
             ;; cache directory, else the test output will contain spurious
             ;; "Fontconfig error: No writable cache directories" lines, which
             ;; causes tests to fail.
             (copy-file (string-append
                         (assoc-ref %build-inputs "fontconfig-minimal")
                         "/etc/fonts/fonts.conf")
                        "fonts.conf")
             (substitute* "fonts.conf"
               (("~/\\.fontconfig")
                (string-append (getcwd) "/.fccache")))
             (setenv "FONTCONFIG_FILE" (string-append (getcwd) "/fonts.conf"))
             ;; https://github.com/Imagick/imagick/blob/master/docker/nixos/shell.nix
             ;; Needed to allow Imagick::getConfigureOptions() to have all info
             ;; https://github.com/Imagick/imagick/issues/399
             (setenv "MAGICK_CONFIGURE_PATH"
                     (dirname (car (find-files (assoc-ref %build-inputs "imagemagick")
                                               "^configure\\.xml$"))))))
         (replace 'install
           (lambda* (#:key inputs outputs #:allow-other-keys)
             (use-modules (ice-9 popen)
                          (ice-9 rdelim))
             (let* ((out (assoc-ref outputs "out"))
                    (php-config (string-append (assoc-ref inputs "php")
                                               "/bin/php-config"))
                    (pipe (open-pipe* OPEN_READ php-config "--extension-dir"))
                    (php-extdir (read-line pipe)))
               (unless (zero? (status:exit-val (close-pipe pipe)))
                 (error "Failed to get PHP extension dir"))
               (invoke
                "make" "phpincludedir=include/php"
                ;; Stop make install from trying to install into /gnu/store/*-php-7.4.*.
                ;; In order to find this extension, PHP must be configured with
                ;; extension_dir = <profile>/lib/php/extensions/<dir> in php.ini.
                (string-append "INSTALL_ROOT=" out "/")
                (string-append "EXTENSION_DIR=lib/php/extensions/"
                               (basename php-extdir))
                "install")))))
       #:parallel-build? #t
       #:test-target "test"
       #:configure-flags
       (list (string-append "--with-imagick="
                            (assoc-ref %build-inputs "imagemagick")))))
    (inputs (list php pcre2 imagemagick fontconfig))
    (native-inputs (list autoconf  ; for phpize
                         pkg-config
                         re2c))  ; according to https://github.com/Imagick/imagick/blob/master/docker/nixos/shell.nix
    (home-page "https://pecl.php.net/package/imagick")
    (synopsis "A PHP wrapper for the ImageMagick library")
    (description "Imagick is a native PHP extension to create and modify
images using the ImageMagick library.")
    (license (license:non-copyleft "file://LICENSE"))))

(define-public php-redis
  (package
    (name "php-redis")
    (version "6.0.2")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "https://pecl.php.net/get/redis-" version ".tgz"))
       (sha256 (base32 "00wskdlh4vamwjalxlzrb7a0mzwid5pbw285dzjpz2agw6qcrbh1"))))
    (build-system gnu-build-system)
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (add-before 'configure 'phpize
           (lambda _
             (invoke "phpize")))
         (replace 'install
           (lambda* (#:key inputs outputs #:allow-other-keys)
             (use-modules (ice-9 popen)
                          (ice-9 rdelim))
             (let* ((out (assoc-ref outputs "out"))
                    (php-config (string-append (assoc-ref inputs "php")
                                               "/bin/php-config"))
                    (pipe (open-pipe* OPEN_READ php-config "--extension-dir"))
                    (php-extdir (read-line pipe)))
               (unless (zero? (status:exit-val (close-pipe pipe)))
                 (error "Failed to get PHP extension dir"))
               (invoke
                "make" "phpincludedir=include/php"
                ;; Stop make install from trying to install into /gnu/store/*-php-7.4.*.
                ;; In order to find this extension, PHP must be configured with
                ;; extension_dir = <profile>/lib/php/extensions/<dir> in php.ini.
                (string-append "INSTALL_ROOT=" out "/")
                (string-append "EXTENSION_DIR=lib/php/extensions/"
                               (basename php-extdir))
                "install")))))
       ;; https://github.com/phpredis/phpredis?tab=readme-ov-file#running-the-unit-tests
       #:tests? #f))                      ; custom test suite that's annoying to figure out
    (inputs (list php))
    (native-inputs (list autoconf))  ; for phpize
    (home-page "https://pecl.php.net/package/redis")
    (synopsis "PHP extension for interfacing with Redis")
    (description "This extension provides a PHP API for communicating with
Redis servers.")
    (license (license:non-copyleft "file://LICENSE"))))
