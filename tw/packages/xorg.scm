(define-module (tw packages xorg)
  #:use-module (gnu packages xorg)
  #:use-module (guix build utils)
  #:use-module (guix build-system gnu)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages))

(define-public xcwd
  (package
    (name "xcwd")
    (version "1.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/schischi/xcwd")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "02i1149nn7ccshggqra6865jx1ayv6n4q4cdqypl1rs8m0gzbbrk"))))
    (build-system gnu-build-system)
    (arguments
     '(#:tests? #f ;no tests defined
       #:make-flags (list (string-append "prefix=" %output))
       #:phases (modify-phases %standard-phases
                  (delete 'configure) ;there is no ./configure
                  (add-before 'install 'create-bin-directory
                    (lambda* (#:key outputs #:allow-other-keys)
                      ;; The Makefile assumes that $(prefix)/bin already exists.
                      (mkdir-p (string-append (assoc-ref outputs "out") "/bin")))))))
    (inputs (list libx11))
    (home-page "https://github.com/schischi/xcwd")
    (synopsis
     "Print the current working directory of the currently focused window")
    (description
     "xcwd is a simple tool that prints the current working directory of the
currently focused window.  The main goal is to launch applications directly
into the same directory as the focused applications.  This is especially
useful to open a new terminal or a file explorer.")
    (license license:bsd-3)))
