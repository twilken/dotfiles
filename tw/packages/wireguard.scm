(define-module (tw packages wireguard)
  #:use-module (gnu packages crypto)
  #:use-module (guix build utils)
  #:use-module (guix build-system cmake)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages))

(define-public wgmine
  (let ((commit "8958c7e897f33d85959a888c673ef544e59dbaf5"))
    (package
      (name "wgmine")
      (version (git-version "0.0" "0" commit))
      (home-page "https://github.com/thatsed/wgmine")
      (source (origin
                (method git-fetch)
                (uri (git-reference (url home-page) (commit commit)))
                (file-name (git-file-name name version))
                (sha256 (base32 "16w0x8bw4ayrmy1xljnf6ld3867k080lijj0shxq98dlgaqd1r5m"))))
      (build-system cmake-build-system)
      (arguments
       '(#:tests? #f ;no tests defined
         #:configure-flags '("-DENABLE_O64=ON")  ;TODO: disable on non-64-bit systems
         #:phases (modify-phases %standard-phases
                    ;; Sources are in src/, so go there since `cmake-build-system'
                    ;; expects to find them in the working directory.
                    (add-before 'configure 'move-source
                      (lambda _
                        (chdir "src")))
                    ;; The upstream project does not define an `install' target.
                    (replace 'install
                      (lambda* (#:key outputs (out-of-source? #t)
                                #:allow-other-keys)
                        (let ((out (assoc-ref outputs "out"))
                              (builddir (if out-of-source? "../build" ".")))
                          (install-file (string-append builddir "/wgmine")
                                        (string-append out "/bin"))))))))
      (inputs (list libsodium))
      (synopsis "Generate vanity public keys for WireGuard")
      (description
       "Generate WireGuard private keys whose public keys have a prefix that
you specify, for instance a few characters of their hostname.  This lets you
recognise peers by their public key more easily.")
      (license license:expat))))
