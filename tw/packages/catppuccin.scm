(define-module (tw packages catppuccin)
  #:use-module (guile)
  #:use-module ((gnu packages base) #:select (coreutils gnu-make grep))
  #:use-module (gnu packages bash)
  #:use-module ((gnu packages compression) #:select (zip))
  ;; #:use-module ((gnu packages check) #:select (python-pytest))
  #:use-module (gnu packages gnome-xyz)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages image)
  #:use-module (gnu packages inkscape)
  #:use-module (gnu packages kde-frameworks)
  #:use-module (gnu packages kde-plasma)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages qt)
  #:use-module ((gnu packages version-control) #:select (git))
  #:use-module (gnu packages video)
  #:use-module (gnu packages web)
  #:use-module (gnu packages xorg)
  #:use-module (guix build utils)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system copy)
  #:use-module (guix build-system pyproject)
  #:use-module ((guix build-system python) #:select (pypi-uri))
  #:use-module (guix build-system qt)
  #:use-module (guix build-system trivial)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (tw packages whiskers))

(define-public python-catppuccin
  (package
    (name "python-catppuccin")
    (version "2.3.4")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "catppuccin" version))
       (sha256 (base32 "0gsiwrk87mgjrb9vk75l7mgnq7w25mmnjkcyz7x9l1asx22gyzp1"))))
    (build-system pyproject-build-system)
    (arguments '(#:tests? #f))  ; tests broken - pytest finds nothing
    (propagated-inputs (list python-matplotlib python-pygments python-rich python-tinycss2))
    (native-inputs (list python-poetry-core #;python-pytest))
    (home-page "https://github.com/catppuccin/python")
    (synopsis "Soothing pastel theme for Python, Matplotlib and Pygments.")
    (description "This package provides Catppuccin colours as Python data
structures, both for use by arbitrary Python programs, and specifically
providing Matplotlib and Pygments themes.")
    (license license:expat)))

(define-public (make-catppuccin-gtk-theme variant accent)
  (package
    (name (format #f "catppuccin-gtk-theme-~a-~a" variant accent))
    (version "1.0.3")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/catppuccin/gtk")
             (recursive? #t)   ; for vendored "colloid" theme
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256 (base32 "0rwca1jq6dq09lwq2jbsisimwp9kvkxsnwwyqd2g7pi6bdqdb7xb"))))
    (inputs (list gtk-engines))
    (native-inputs (list python python-catppuccin git sassc inkscape optipng))
    (build-system trivial-build-system)
    (arguments
     `(#:modules ((guix build utils))
       #:builder
       (begin
         (use-modules (guix build utils))
         (let* ((out (assoc-ref %outputs "out"))
                (themesdir (string-append out "/share/themes/")))
           (define (make-path subpath)
             (string-join
              (map (lambda (input)
                     (string-append (cdr input) subpath))
                   %build-inputs)
              ":"))
           (setenv "PATH" (make-path "/bin"))
           (setenv "GUIX_PYTHONPATH"
                   (make-path (format #f "/lib/python~a/site-packages"
                                      ,(version-major+minor
                                        (package-version python)))))
           (copy-recursively (assoc-ref %build-inputs "source") (getcwd))
           (invoke "python3" "build.py" ,variant "--accent" ,accent
                   "--tweaks" "rimless" "--dest" themesdir)
           ;; Instead of Tela-circle, use Papirus.
           ;; (for-each (lambda (file)
           ;;             (substitute* file
           ;;               (("Tela-circle") "Papirus")))
           ;;           (find-files themesdir "/share/themes/[^/]+/index\\.theme$"))
           #t))))
    (home-page "https://github.com/catppuccin/gtk")
    (synopsis "Soothing pastel theme for GTK")
    (description "Soothing pastel theme for GTK 4, GTK 3, GNOME-Shell
and other DEs (like XFCE) using the Catppuccin color palette.  This
theme is based on the Colloid theme made by Vinceliuice.")
    (license license:gpl3)))

(define-public (make-catppuccin-cursors variant accent)
  (package
    (name (format #f "catppuccin-~a-~a-cursors" variant accent))
    (version "1.0.2")
    (home-page "https://github.com/catppuccin/cursors")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference (url home-page) (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256 (base32 "01vg3irzpjycz24z7dw4fc89zry3n969ghjgwjxfk1fj3x31yv9j"))))
    (native-inputs (list bash-minimal coreutils gnu-make   ; for Makefile, main build script
                         grep zip inkscape                 ; for scripts/build-cursors
                         python python-pyside-6 xcursorgen ; for scripts/generate-metadata
                         catppuccin-whiskers))             ; for generating SVGs
    (build-system trivial-build-system)
    (arguments
     `(#:modules ((guix build utils))
       #:builder
       ,#~(begin
            (use-modules (guix build utils))
            (let ((source (assoc-ref %build-inputs "source"))
                  (out (assoc-ref %outputs "out")))
              ;; `trivial-build-system' doesn't set these paths up, so do it manually.
              (for-each (lambda (spec)
                          (setenv (car spec)
                                  (string-join (map (lambda (input)
                                                      (string-append (cdr input) (cdr spec)))
                                                    %build-inputs)
                                               ":")))
                        '(("PATH" . "/bin")
                          ("GUIX_PYTHONPATH"
                           . #+(format #f "/lib/python~a/site-packages"
                                       (version-major+minor
                                        (package-version python))))))
              (for-each (lambda (item)
                          (copy-recursively (string-append source "/" item) item))
                        '("AUTHORS" "LICENSE" "build" "scripts" "src"))
              (for-each make-file-writable (find-files (getcwd)))
              (for-each patch-shebang (cons* "build" (find-files "scripts")))
              (invoke "./build" "-f" #$variant "-a" #$accent)
              (chdir (format #f "dist/catppuccin-~a-~a-cursors" #$variant #$accent))
              (let ((docdir (string-append out "/share/doc/" #$name "-" #$version)))
                (install-file "AUTHORS" docdir)
                (install-file "LICENSE" docdir))
              (let ((destdir (format #f "~a/share/icons/Catppuccin-~a-~a-Cursors" out
                                     (string-titlecase #$variant)
                                     (string-titlecase #$accent))))
                (install-file "index.theme" destdir)
                (copy-recursively "cursors" (string-append destdir "/cursors"))
                (copy-recursively "cursors_scalable" (string-append destdir "/cursors_scalable")))))))
    (synopsis "Soothing pastel mouse cursors")
    (description "Soothing pastel cursor theme using the Catppuccin
color palette.  This project is just a modification of Volantes
Cursors with a Catppuccin palettes.")
    (license license:gpl2)))

(define* (catppuccin-theme-package
          #:key program commit (version "0.0.0") (revision "1")
          (license license:expat) repo-hash install-plan)
  (package
    (name (string-append "catppuccin-" program "-theme"))
    ;; See info '(guix)Version Numbers' for advice.
    (version (git-version version revision commit))
    (home-page (string-append "https://github.com/catppuccin/" program))
    (source
     (origin
       (method git-fetch)
       (uri (git-reference (url home-page) (commit commit)))
       (file-name (git-file-name name version))
       (sha256 (base32 repo-hash))))
    (build-system copy-build-system)
    (arguments
     `(#:install-plan ',install-plan))
    (synopsis (string-append "Soothing pastel theme for " program))
    (description (string-append "Soothing pastel " program " theme using the
Catppuccin color palette."))
    (license license)))

(define-public catppuccin-aerc-theme
  (catppuccin-theme-package
   #:program "aerc"
   #:revision "25"   ; total number of commits since repo beginning
   #:commit "ca404a9f2d125ef12db40db663d43c9d94116a05"
   #:repo-hash "0q9a818rwsqx5kvln5zzfan54xaw9yqbbm5hjbrwzdl5q8g28qir"
   #:install-plan '(("dist/" "share/catppuccin/aerc/"))))

(define-public catppuccin-dunst-theme
  (catppuccin-theme-package
   #:program "dunst"
   #:revision "9"   ; total number of commits since repo beginning
   #:commit "f02cd2894411c9b4caa207cfd8ed6345f97c0455"
   #:repo-hash "07lf6yz3sd0vrs0ls5cm2w6j71kp7snyn5brjkdzz6vms6m4v9qi"
   #:install-plan '(("themes/" "share/catppuccin/dunst/"))))

(define-public catppuccin-foot-theme
  (catppuccin-theme-package
   #:program "foot"
   #:commit "962ff1a5b6387bc5419e9788a773a080eea5f1e1"
   #:revision "47"   ; total number of commits since repo beginning
   #:repo-hash "0mhk1p3q7ki84bgdacrcygq5bn36r37p1bis78zysrczil2zflbr"
   #:install-plan '(("themes/" "share/catppuccin/foot/"))))

(define-public catppuccin-imv-theme
  (catppuccin-theme-package
   #:program "imv"
   #:commit "0317a097b6ec8122b1da6d02f61d0c5158019f6e"
   #:revision "4"   ; total number of commits since repo beginning
   #:repo-hash "12df3lvsbss433m938fbm9snxv324s8z2j37jjfj6mb2rv21palz"
   #:install-plan '(("themes/" "share/catppuccin/imv/"))))

(define-public catppuccin-kde-theme
  (catppuccin-theme-package
   #:program "kde"
   #:commit "494c8576b17626a7b2c7a43cec8e6133a5e9c482"
   #:repo-hash "07wizfbr0w23546n2skf8c33nayzxv044spbwrprjmh8sy7v6m7w"
   #:license license:gpl2
   #:install-plan '(("Frappe/CatppuccinFrappe.colors" "share/color-schemes/")
                    ("Latte/CatppuccinLatte.colors" "share/color-schemes/")
                    ("Macchiato/CatppuccinMacchiato.colors" "share/color-schemes/")
                    ("Mocha/CatppuccinMocha.colors" "share/color-schemes/"))))

(define-public catppuccin-kitty-theme
  (catppuccin-theme-package
   #:program "kitty"
   #:commit "b14e8385c827f2d41660b71c7fec1e92bdcf2676"
   #:revision "61"   ; total number of commits since repo beginning
   #:repo-hash "1h9zqc6gcz9rpn7p6ir3jy9glnj336jdcz5ildagd0fm5kn8vlz7"
   #:install-plan '(("themes/" "share/catppuccin/kitty/"))))

(define-public catppuccin-mpv-theme
  (catppuccin-theme-package
   #:program "mpv"
   #:commit "8d82ef42bde7cc7cc4fad7ce690aa90feab46f34"
   #:revision "23"   ; total number of commits since repo beginning
   #:repo-hash "0d7ycmfc12pk1ph3rryz97g0xbz8v7rns9q6jhh8x52d7ickv6wp"
   #:install-plan '(("themes/" "share/catppuccin/mpv/"))))

(define-public catppuccin-newsboat-theme
  (catppuccin-theme-package
   #:program "newsboat"
   #:revision "7"   ; total number of commits since repo beginning
   #:commit "be3d0ee1ba0fc26baf7a47c2aa7032b7541deb0f"
   #:repo-hash "04ib4lvma5959n943f7myzbc2blmb8n2dd7bkb0xgl2rnpfx2fvk"
   #:install-plan '(("themes/" "share/catppuccin/newsboat/"))))

(define-public catppuccin-obs-theme
  (if (eq? '< (version-compare (package-version obs) "30.2.0"))
      (catppuccin-theme-package
       #:program "obs"
       #:commit "pre-30.2.0"
       #:repo-hash "1mnhfhqgqzdqkl0gv2p7fv26yj2ib3gly3y3bl2hbm0sych215vm"
       #:install-plan '(("themes/" "share/catppuccin/obs/")))
      (catppuccin-theme-package
       #:program "obs"
       #:commit "d90002a5315db3a43c39dc52c2a91a99c9330e1f"
       #:revision "21"   ; total number of commits since repo beginning
       #:repo-hash "1d7qcca5928q3jqg5nwm7nrn566clx7jn7jhds7gy4xn7x71ckmd"
       #:install-plan '(("themes/" "share/catppuccin/obs/")))))

(define-public catppuccin-polybar-theme
  (catppuccin-theme-package
   #:program "polybar"
   #:revision "9"   ; total number of commits since repo beginning
   #:commit "989420b24e1f651b176c9d6083ad7c3b90a27f8b"
   #:repo-hash "0wvshs5f54h0nfzf7pjwdhmj6xcngxqqvaqzxa3w5j7sk89nmwyr"
   #:install-plan '(("themes/" "share/catppuccin/polybar/"))))

(define-public catppuccin-qt5ct-theme
  (catppuccin-theme-package
   #:program "qt5ct"
   #:commit "89ee948e72386b816c7dad72099855fb0d46d41e"
   #:revision "3"   ; total number of commits since repo beginning
   #:repo-hash "197yivsclcjxp2819vpxj5adqfyrsi29m75lqsmdxapv8lmv5yxp"
   #:install-plan '(("themes/" "share/qt5ct/colors/"))))

(define-public catppuccin-rofi-theme
  (catppuccin-theme-package
   #:program "rofi"
   #:revision "43"   ; total number of commits since repo beginning
   #:commit "5350da41a11814f950c3354f090b90d4674a95ce"
   #:repo-hash "15phrl9qlbzjxmp29hak3a5k015x60w2hxjif90q82vp55zjpnhc"
   #:install-plan '(("basic/.local/share/rofi/themes/" "share/catppuccin/rofi/"))))

(define-public catppuccin-swaylock-theme
  (catppuccin-theme-package
   #:program "swaylock"
   #:revision "41"
   #:commit "77246bbbbf8926bdb8962cffab6616bc2b9e8a06"
   #:repo-hash "02nql7ry71fxlhj0vsbsxi3jrmfajxmapr9gg0mzp0k0bxwqxa00"
   #:install-plan '(("themes/" "share/catppuccin/swaylock/"))))

(define-public catppuccin-vim-theme
  (catppuccin-theme-package
   #:program "vim"
   #:revision "39"   ; total number of commits since repo beginning
   #:commit "be4725cfc3fb6ed96f706d9d1bd5baa24d2b048c"
   #:repo-hash "1mhrch0ck3g1gs79c6mlbj2krhqqk7hp5g0v7ahap71bcfk5yxk7"
   #:install-plan '(("colors/" "share/catppuccin/vim/colors/")
                    ("autoload/" "share/catppuccin/vim/autoload/"))))

(define-public catppuccin-waybar-theme
  (catppuccin-theme-package
   #:program "waybar"
   #:version "1.1"
   #:commit "0830796af6aa64ce8bc7453d42876a628777ac68"
   #:repo-hash "0np88b9zi6zk21fy5w4kmgjg1clqp4ggw1hijlv9qvlka2zkwmpn"
   #:install-plan '(("themes/" "share/catppuccin/waybar/"))))

(define-public catppuccin-zathura-theme
  (catppuccin-theme-package
   #:program "zathura"
   #:revision "39"   ; total number of commits since repo beginning
   #:commit "0adc53028d81bf047461bc61c43a484d11b15220"
   #:repo-hash "1cj1z2bh1qw1sbgqmk4i450yv7rgwcz06yhar23ccadsx22gzw7y"
   #:install-plan '(("src/" "share/catppuccin/zathura/"))))
