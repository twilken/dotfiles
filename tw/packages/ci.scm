(define-module (tw packages ci)
  #:use-module (gnu packages base)
  #:use-module (gnu packages elf)
  #:use-module (guix build-system copy)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix modules)
  #:use-module (guix packages)
  #:use-module (guix utils))

(define-public (hashicorp-arch)
  (cond
   ((target-x86-64?) "amd64")
   ((target-x86-32?) "386")
   ((target-arm32?) "arm")
   ((target-aarch64?) "arm64")))

(define-public (hashicorp-download-url name version)
  (string-append "https://releases.hashicorp.com/" name "/" version "/"
                 name "_" version "_linux_" (hashicorp-arch) ".zip"))

(define-public hashicorp-packer-bin
  (package
    (name "hashicorp-packer")
    (version "1.12.0")
    (source
     (origin
       (method url-fetch/zipbomb)
       (uri (hashicorp-download-url "packer" version))
       (sha256 (base32 "1klsijsw85p6f1vw2rs5rp5bm4c0j1fnsfamz8liw3apb5kafng8"))))
    (build-system copy-build-system)
    (arguments '(#:install-plan '(("packer" "bin/"))))
    (home-page "https://packer.io/")
    (synopsis "Build automated machine images")
    (description "Hashicorp Packer is a tool for creating identical machine
images for multiple platforms from a single source configuration.")
    (license license:mpl2.0)))

(define-public hashicorp-consul-bin
  (package
    (name "hashicorp-consul")
    (version "1.17.0")
    (source
     (origin
       (method url-fetch/zipbomb)
       (uri (hashicorp-download-url "consul" version))
       (sha256 (base32 "0gp2jzzg4s8lhqfkl03vhlrrrs0vb9vfk7ql2k9p5z6x0lx6rfsg"))))
    (build-system copy-build-system)
    (arguments '(#:install-plan '(("consul" "bin/"))))
    (home-page "https://consul.io/")
    (synopsis "Datacenter service mesh tool")
    (description "Consul is a distributed, highly available, and data center
aware solution to connect and configure applications across dynamic,
distributed infrastructure.")
    (license license:mpl2.0)))

(define-public hashicorp-vault-bin
  (package
    (name "hashicorp-vault")
    (version "1.15.2")
    (source
     (origin
       (method url-fetch/zipbomb)
       (uri (hashicorp-download-url "vault" version))
       (sha256 (base32 "1576n8kn16bx81k1dim1qmpimpcnxjskclbsllxp24n27fa2022s"))))
    (build-system copy-build-system)
    (arguments '(#:install-plan '(("vault" "bin/"))))
    (home-page "https://vaultproject.io/")
    (synopsis "Secrets management, encryption as a service, and privileged
access management")
    (description "Vault is a tool for securely accessing secrets.  A secret is
anything that you want to tightly control access to, such as API keys,
passwords, certificates, and more.  Vault provides a unified interface to any
secret, while providing tight access control and recording a detailed audit
log.")
    (license license:mpl2.0)))

(define-public hashicorp-nomad-bin
  (package
    (name "hashicorp-nomad")
    (version "1.6.3")
    (source
     (origin
       (method url-fetch/zipbomb)
       (uri (hashicorp-download-url "nomad" version))
       (sha256 (base32 "1l7lnxdvkkhfjklqm2drwchnfmjb9y13xdwp9ddy5h6r60qghw8p"))))
    (build-system copy-build-system)
    (arguments
     `(#:install-plan '(("nomad" "bin/"))
       #:phases
       ,(with-imported-modules (source-module-closure
                                '((guix build utils)))
          #~(modify-phases %standard-phases
              (add-after 'unpack 'patch-interpreter
                ;; For some reason, the "nomad" binary has /lib64/ld-linux*.so
                ;; hardcoded, so fix that.
                (lambda* (#:key inputs #:allow-other-keys)
                  (use-modules (guix build utils))
                  (let ((patchelf
                         (search-input-file inputs "bin/patchelf"))
                        (ld-linux.so
                         ;; TODO: remove hardcoded linux-x86-64.
                         ;; (%current-target-system) is almost the right format...
                         (search-input-file inputs "lib/ld-linux-x86-64.so.2")))
                    (invoke patchelf "--set-interpreter" ld-linux.so "nomad"))))))))
    (inputs (list glibc))
    (native-inputs (list patchelf))
    (home-page "https://nomadproject.io/")
    (synopsis "A simple orchestrator to deploy and manage applications")
    (description "Nomad is an easy-to-use, flexible, and performant workload
orchestrator that can deploy a mix of microservice, batch, containerized, and
non-containerized applications.  Nomad is easy to operate and scale and has
native Consul and Vault integrations.")
    (license license:mpl2.0)))

(define-public hashicorp-levant-bin
  (package
    (name "hashicorp-levant")
    (version "0.3.3")
    (source
     (origin
       (method url-fetch/zipbomb)
       (uri (hashicorp-download-url "levant" version))
       (sha256 (base32 "0qnljbqhc3gj4d5lkc8s07w504yqvhc5x412pq2bkh7xk424q333"))))
    (build-system copy-build-system)
    (arguments
     `(#:install-plan '(("levant" "bin/"))))
    (home-page "https://github.com/hashicorp/levant")
    (synopsis "An open source templating and deployment tool for HashiCorp Nomad jobs")
    (description "Levant is an open source templating and deployment tool for
HashiCorp Nomad jobs that provides realtime feedback and detailed failure
messages upon deployment issues.")
    (license license:mpl2.0)))
