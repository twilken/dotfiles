(define-module (tw packages games)
  #:use-module ((srfi srfi-26) #:select (cut))
  #:use-module ((ice-9 string-fun) #:select (string-replace-substring))
  #:use-module (gnu packages algebra)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages build-tools)
  #:use-module (gnu packages check)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages cpp)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages elf)
  #:use-module (gnu packages fonts)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages game-development)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages guile)
  #:use-module (gnu packages image)
  #:use-module (gnu packages java)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages logging)
  #:use-module (gnu packages lua)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages pretty-print)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages sdl)
  #:use-module (gnu packages serialization)
  #:use-module (gnu packages speech)
  #:use-module (gnu packages textutils)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages version-control)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages xorg)
  #:use-module (guix build utils)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system copy)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system meson)
  #:use-module (guix build-system pyproject)
  #:use-module (guix build-system trivial)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix gexp)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix modules)
  #:use-module (guix packages)
  #:use-module (games build-system mojo)
  #:use-module (games gog-download)
  #:use-module (nonguix build-system binary)
  #:use-module ((nonguix licenses) #:prefix nonguix-license:))


;; Shenzhen I/O solitaire

(define-public szio-solitaire
  (package
    (name "szio-solitaire")
    (version "1.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://git.twilken.net/szio-solitaire")
                    (commit version)))
              (file-name (git-file-name name version))
              (sha256 (base32 "1s6kyf9l33kb46fwiqc3inyq3vnq107lk4a6f1hrzsrlgzshp3dz"))))
    (build-system pyproject-build-system)
    (arguments `(#:tests? #f   ; no tests defined
                 #:phases
                 ,#~(modify-phases %standard-phases
                      (add-before 'build 'set-env-version
                        (lambda _
                          (setenv "SETUPTOOLS_SCM_PRETEND_VERSION" #$version))))))
    (inputs (list python-pygame))
    (native-inputs (list python-wheel python-setuptools python-setuptools-scm))
    (home-page "https://cgit.twilken.net/szio-solitaire/about/")
    (synopsis "Solitaire game")
    (description "This solitaire game has the same rules as the one that comes
with the Shenzhen I/O game.")
    (license license:gpl3+)))


;; Simutrans

(define-public simutrans
  ;; Based off the Arch Linux package:
  ;; https://github.com/archlinux/svntogit-community/blob/packages/simutrans/trunk/PKGBUILD
  ;; I took the idea of a wrapper script from the Nix package:
  ;; https://github.com/NixOS/nixpkgs/blob/master/pkgs/games/simutrans/default.nix
  (package
    (name "simutrans")
    ;; Note: when updating, update paksets below to match.  They are released in tandem.
    (version "124.3")
    (source (origin
              (method url-fetch/zipbomb)
              (uri (let ((version/- (string-replace-substring version "." "-")))
                     (string-append "mirror://sourceforge/simutrans/simutrans/"
                                    version/- "/simutrans-src-" version/- ".zip")))
              ;; Use `guix hash -H sha256 <file>'.
              (sha256 (base32 "0sjhpk3bcqnr0bmg3xrdwdxj2jhq848pa8gif2q2spvjxdw0i676"))))
    (build-system gnu-build-system)
    (arguments
     `(#:make-flags
       '("VERBOSE=1" "OPTIMISE=1" "OSTYPE=linux" "BACKEND=sdl2" "MULTI_THREAD=1"
         "USE_ZSTD=1" "USE_FREETYPE=1" "USE_FLUIDSYNTH_MIDI=1" "PROG=simutrans")
       #:tests? #f           ; tests don't work as SDL2 can't find a video device
       #:test-target "test"  ; run "make test" for tests
       #:phases
       ,#~(modify-phases %standard-phases
            (add-before 'bootstrap 'dos2unix
              (lambda _
                (apply invoke #+(file-append dos2unix "/bin/dos2unix")
                       "-q" (find-files "."))))

            (replace 'configure
              (lambda _
                (copy-file "config.template" "config.default")))

            (replace 'install
              (lambda* (#:key outputs #:allow-other-keys)
                (let ((out (assoc-ref outputs "out")))
                  ;; Install "real" simutrans executable into libexec, as we need
                  ;; a wrapper script to be installed by `simutrans-pak-union'.
                  (install-file "build/default/simutrans" (string-append out "/libexec"))

                  ;; Install game data bundled with simutrans (not the pak).
                  (let ((data (string-append out "/share/games/simutrans")))
                    (mkdir-p data)
                    (copy-recursively "simutrans" data))

                  ;; Install extra helper files.
                  (install-file #+(local-file "files/simutrans.desktop")
                                (string-append out "/share/applications"))
                  (install-file #+(local-file "files/simutrans.appdata.xml")
                                (string-append out "/share/metainfo"))))))))

    (inputs (list `(,zstd "lib") freetype fluidsynth   ; technically optional
                  fontconfig zlib bzip2 libpng sdl2))  ; required
    (native-inputs (list autoconf automake pkg-config unzip))
    (home-page "https://www.simutrans.com/")
    (synopsis "Transportation simulation game")
    (description "Simutrans is a freeware and open-source transportation
simulator.  Your goal is to establish a successful transport company.
Transport passengers, mail and goods by rail, road, ship, and even air.
Interconnect districts, cities, public buildings, industries and tourist
attractions by building a transport network you always dreamed of.

@strong{Warning:} This package by itself does not include any paksets.
Install a @code{simutrans-with-pak*} package instead.")
    (license (license:non-copyleft
              "file://simutrans/license.txt"
              "This is Simutrans' custom artistic license."))))

(define-public simutrans-makeobj
  (package
    (inherit simutrans)
    (name "simutrans-makeobj")
    (arguments
     `(#:make-flags '("VERBOSE=1" "OPTIMISE=1" "OSTYPE=linux" "BACKEND=posix" "makeobj")
       #:tests? #f   ; there are no tests for makeobj
       #:phases
       ,#~(modify-phases %standard-phases
            (add-before 'bootstrap 'dos2unix
              (lambda _
                (apply invoke #+(file-append dos2unix "/bin/dos2unix")
                       "-q" (find-files "."))))
            (replace 'configure
              (lambda _
                (copy-file "config.template" "config.default")))
            (replace 'install
              (lambda* (#:key outputs #:allow-other-keys)
                (install-file "build/default/makeobj/makeobj"
                              (string-append (assoc-ref outputs "out") "/bin")))))))
    (inputs (list libpng))
    (synopsis "Addon compiler for Simutrans")
    (description "@code{makeobj} compiles images and configuration files into
addons for the Simutrans game, as @code{.pak} files.")))

(define-public simutrans-pak128
  ;; Commit 3b095fddd99d608f229aa15e07a43893486b5eaa is probably the release
  ;; commit for 2.10, but it's not tagged as such.
  ;; Note: for Simutrans >= 124.3.
  (let ((version "2.10")    ; last tagged version
        (revision "0")    ; number of commits since last tagged version
        (commit "3b095fddd99d608f229aa15e07a43893486b5eaa"))   ; likely commit of that version
    (package
      (name "simutrans-pak128")
      (version (git-version version revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://github.com/simutrans/pak128")
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256 (base32 "0s2l9axhiwq840z61sbaz974mcimrgkyidzgx5cvqnfniy2sm50r"))))
      (native-inputs (list simutrans-makeobj zip git))
      (build-system gnu-build-system)
      (arguments
       `(#:make-flags '("all")   ; the "all" target builds the pak and installs it
         #:parallel-build? #f    ; don't run "rm" in parallel with builds
         #:phases
         ,#~(modify-phases %standard-phases
              (delete 'bootstrap)
              (delete 'configure)
              (replace 'build
                (lambda* (#:key inputs outputs (make-flags '()) (parallel-build? #t)
                          #:allow-other-keys)
                  (let ((makeobj (search-input-file inputs "bin/makeobj"))
                        (out (assoc-ref outputs "out")))
                    ((assoc-ref %standard-phases 'build)
                     #:make-flags
                     (cons* (string-append "MAKEOBJ=" makeobj)
                            ;; Makefile adds /pak128 to DESTDIR.
                            (string-append "DESTDIR=" out "/share/games/simutrans")
                            (string-append "PAKID=pak128 " #$version " for " #$(package-version simutrans))
                            make-flags)
                     #:parallel-build? parallel-build?))))
              (delete 'check)
              (delete 'install))))
      (home-page "https://forum.simutrans.com/index.php?board=26.0")
      (synopsis "Graphical pak set for Simutrans, 128x128 tile size")
      (description "This is a self-contained set of assets for the Simutrans
game.  When Simutrans could only support 64px size graphics, work on pak128
already started.  This is the first pak set to feature a complex economy and
have a very wide variety of objects.  It contains roughly 7 times more graphic
data than pak64 and thus requires by far the largest amount of RAM and
processing power of all Simutrans sets.")
      (license license:artistic2.0))))

;; See also: `sdl-union' in (gnu packages sdl).
(define* (simutrans-pak-union name #:optional (paksets (list simutrans-pak128))
                              (base-game simutrans))
  "Return union package called NAME which is a union of BASE-GAME and PAKSETS.
If PAKSETS are not specified, simutrans is bundled with some default paksets."
  (package
    (name name)
    (version (package-version base-game))
    (source #f)
    (build-system trivial-build-system)
    (arguments
     `(#:builder
       ,(with-imported-modules
            (source-module-closure
             '((guix build union)
               (guix build utils)))
          #~(begin
              (use-modules ((srfi srfi-26) #:select (cut))
                           (guix build union)
                           (guix build utils))

              ;; Symlink the data files into place.  Must be done first.
              (union-build #$output (list #$base-game #$@paksets))

              ;; Install wrapper script to pass the right workdir to simutrans.
              ;; We can't use a `program-file' here since that would introduce
              ;; a circular dependency between that file and this package.
              (mkdir-p (string-append #$output "/bin"))
              (call-with-output-file (string-append #$output "/bin/simutrans")
                (cut format <> "#! ~a/bin/guile --no-auto-compile~%!#~%~s~%"
                     #$guile-3.0
                     `(apply execl ,(string-append #$output "/libexec/simutrans")
                             "simutrans" "-set_basedir"
                             ,(string-append #$output "/share/games/simutrans")
                             (cdr (command-line)))))
              (chmod (string-append #$output "/bin/simutrans") #o755)))))

    (inputs (cons* guile-3.0 base-game paksets))
    (synopsis "Union of Simutrans with paksets")
    (description
     "A union of the Simutrans game with some of its paksets.  A union is
required because Simutrans assumes that its paksets are installed in the same
directory as assets from the base game, such as music files.")
    (home-page (package-home-page base-game))
    (license (map package-license (cons base-game paksets)))))

(define-public simutrans/pak128
  (simutrans-pak-union "simutrans-with-pak128"
                       (list simutrans-pak128)))


;; Kerbal Space Program

;; $ guix download KSP-Breaking_Ground_Expansion-en-us-lin-1.7.1.zip
;; /gnu/store/mlrsbl99rjmx0ss7im9d26rnxg98mb04-KSP-Breaking_Ground_Expansion-en-us-lin-1.7.1.zip
;; 0k3agr1ycdk8cll54wn63r6hdzbjpcj7wsjl003wkpza410x4ja8

;; TODO: package Breaking Ground DLC

(define-public kerbal-space-program
  (package
    (name "kerbal-space-program")
    (version "1.12.5")
    (source (origin
              (method url-fetch)
              (uri (string-append "file://" (getenv "HOME")
                                  "/installers/games/ksp-linux-"
                                  version ".zip"))
              (sha256 (base32 "0rxh25wmx4bpxdjjmci77356j60d37w6p1rvh72m38z41fyyzq0a"))))
    (build-system binary-build-system)
    ;; Linux Installer instructions from KSP_linux/readme.txt:
    ;; * Uncompress the file provided
    ;; * Place the contents of the file in your KSP installation directory
    ;; * Run the script called dlc-mhe-[lang].command   ; this doesn't seem to exist...
    (supported-systems '("x86_64-linux"))
    (arguments
     `(#:patchelf-plan
       '(("KSPLauncher_Data/Plugins/x86_64/ScreenSelector.so"
          ("glibc" "gcc:lib" "mesa" "gtk+" "atk" "glib" "pango"
           "gdk-pixbuf" "cairo" "freetype" "fontconfig"))
         ("UnityPlayer.so"
          ;; Unity tries to load the following dynamically (even though they
          ;; don't show up in ldd output).  Not sure what it does on Wayland.
          ("glibc" "gcc:lib" "mesa" "libx11" "libxext" "libxcursor"
           "libxinerama" "libxi" "libxrandr" "libxscrnsaver" "libxxf86vm"
           "pulseaudio" "alsa-lib" "eudev"))
         ;; Mesa doesn't show up in ldd output, but is loaded by the launcher
         ;; and KSP itself.
         ,@(map (cut list <> '("glibc" "gcc:lib" "mesa"))
                '("KSPLauncher.x86_64"
                  "KSPLauncher_Data/MonoBleedingEdge/x86_64/libMonoPosixHelper.so"
                  "KSPLauncher_Data/MonoBleedingEdge/x86_64/libmonobdwgc-2.0.so"
                  "KSP.x86_64"
                  "KSP_Data/MonoBleedingEdge/x86_64/libMonoPosixHelper.so"
                  "KSP_Data/MonoBleedingEdge/x86_64/libmonobdwgc-2.0.so"
                  "KSP_Data/Plugins/libkeyboard.so"
                  "KSP_Data/Plugins/liblingoona.grammar.kerbal.so"
                  "KSP_Data/Plugins/libsteam_api.so")))
       #:install-plan
       '(("./" "share/kerbal-space-program/"))
       #:phases
       ,#~(modify-phases %standard-phases
            (add-after 'patchelf 'add-origin-to-rpath
              ;; KSP.x86_64 needs to be able to find UnityPlayer.so.
              (lambda _
                (use-modules (ice-9 popen))
                (let* ((patchelf #+(file-append patchelf "/bin/patchelf"))
                       (port (open-pipe* OPEN_READ patchelf "--print-rpath" "KSP.x86_64"))
                       (rpath (symbol->string (read port)))
                       (new-rpath (string-append rpath ":$ORIGIN")))
                  (close-pipe port)
                  (invoke patchelf "--set-rpath" new-rpath "KSP.x86_64"))))
            (add-after 'install 'wrap-game
              (lambda* (#:key inputs outputs #:allow-other-keys)
                (wrap-program (search-input-file
                               outputs "share/kerbal-space-program/KSP.x86_64")
                  #:sh (search-input-file inputs "/bin/sh")
                  `("LC_ALL" "" = ("C"))
                  `("PATH" ":" =
                    (;; KSP runs "sh -c 'setxkbmap -print'" on startup.
                     ,(string-append (assoc-ref inputs "bash-minimal") "/bin")
                     ,(string-append (assoc-ref inputs "setxkbmap") "/bin")
                     ;; Hopefully, this will allow opening web links in the main menu.
                     ,(string-append (assoc-ref inputs "xdg-utils") "/bin"))))))
            (add-after 'install 'wrap-launcher
              ;; The launcher calls out to xdg-open and uname for some actions.
              (lambda* (#:key inputs outputs #:allow-other-keys)
                (wrap-program (search-input-file
                               outputs "share/kerbal-space-program/KSPLauncher.x86_64")
                  #:sh (search-input-file inputs "/bin/sh")
                  `("PATH" ":" =
                    (,(string-append (assoc-ref inputs "xdg-utils") "/bin")
                     ,(string-append (assoc-ref inputs "coreutils") "/bin"))))))
            (add-after 'install 'install-overlay-launcher
              (lambda* (#:key inputs outputs #:allow-other-keys)
                (let ((out (assoc-ref outputs "out")))
                  (mkdir-p (string-append out "/bin"))
                  (call-with-output-file (string-append out "/bin/ksp")
                    (lambda (port)
                      (chmod port #o555)   ; make executable
                      (display (string-append "\
#!" (assoc-ref inputs "bash-minimal") "/bin/sh -e
ret=0 dir=$XDG_DATA_HOME/ksp-overlay
mkdir -p \"$dir/upper\" \"$dir/work\" \"$dir/merged\"
echo 'Mounting overlayfs' >&2
sudo mount -t overlay overlay \"$dir/merged\" -o \\
     \"lowerdir=" out "/share/kerbal-space-program,upperdir=$dir/upper,workdir=$dir/work,metacopy=on\"
# Make files writable in the overlay, so that KSP can overwrite the ones it needs to.
# This relies on metacopy=on for efficiency (so file data isn't copied to the upperdir on chown).
echo 'Adjusting permissions' >&2
sudo find \"$dir/merged\" -exec chown \"$(id -u):$(id -g)\" '{}' + -exec chmod u+w '{}' +
echo 'Running KSP' >&2
\"$dir/merged/KSP.x86_64\" \"$@\" || ret=$?
echo 'Unmounting overlayfs' >&2
sudo umount \"$dir/merged\"
exit $ret
") port)))))))))
    (inputs `(("glibc" ,glibc)
              ("gcc:lib" ,gcc "lib")
              ("mesa" ,mesa)
              ("gtk+" ,gtk+-2)
              ("atk" ,atk)
              ("glib" ,glib)
              ("pango" ,pango)
              ("gdk-pixbuf" ,gdk-pixbuf)
              ("cairo" ,cairo)
              ("freetype" ,freetype)
              ("fontconfig" ,fontconfig)
              ("libx11" ,libx11)
              ("libxext" ,libxext)
              ("libxcursor" ,libxcursor)
              ("libxinerama" ,libxinerama)
              ("libxi" ,libxi)
              ("libxrandr" ,libxrandr)
              ("libxscrnsaver" ,libxscrnsaver)
              ("libxxf86vm" ,libxxf86vm)
              ("pulseaudio" ,pulseaudio)
              ("alsa-lib" ,alsa-lib)
              ("eudev" ,eudev)
              ("bash-minimal" ,bash-minimal)
              ("coreutils" ,coreutils)
              ("xdg-utils" ,xdg-utils)
              ("setxkbmap" ,setxkbmap)))
    (native-inputs (list unzip))
    (home-page "https://www.kerbalspaceprogram.com/game/kerbal-space-program/")
    (synopsis "Rocketry simulation game")
    (description "Create and manage your own space program.  Build spacecraft,
fly them, and try to help the Kerbals fulfill their ultimate mission of
conquering space.  This is the original KSP, not the early-access KSP2.

Note: KSP seems to require a writable game directory (e.g., in order to write
its @samp{settings.cfg}).  This means that the game will not run properly from
@samp{/gnu/store}, and must be copied somewhere else first, for instance using
@samp{rsync -a $(guix build kerbal-space-program)/share/kerbal-space-program/
~/ksp/ && chmod -R u+w ~/ksp && ~/ksp/KSP.x86_64}.")
    (license (list (nonguix-license:undistributable
                    "https://www.take2games.com/eula/")
                   (license:non-copyleft
                    "file://LegalNotice.txt"
                    "Contains components licensed under various permissive licenses.")))))


;;; Oolite

(define-public oolite
  (package
    (name "oolite")
    (version "1.90")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/OoliteProject/oolite")
             (commit version)
             (recursive? #t)))
       (file-name (git-file-name name version))
       (sha256 (base32 "1fkirmgv303gq8mvg71zi66xfx11qm3ppqxyzirxwp983xjih9kw"))))
    (build-system gnu-build-system)
    (arguments `(#:phases
                 (modify-phases %standard-phases
                   (delete 'configure)  ; no ./configure
                   (replace 'build
                     (lambda _
                       (invoke "make" "-f" "Makefile" "-j" "release-deployment"))))))
    (inputs (list espeak))
    (native-inputs (list))
    (home-page "https://oolite.space/")
    (synopsis "Open-world space opera game")
    (description
     "Oolite is inspired by the 8-bit classic Elite, and many aspects of
gameplay will be familiar to players of that game.  In the tradition of
open-world games, there's no overall story: you can be a millionaire trader, a
veteran combateer, a feared pirate, a lonely miner, a notorious smuggler, or
all of them, or something else entirely, based on your own actions.

For those new to the game, the @url{https://oolite.space/starting, getting
started} page has some hints for beginners to see you safely through your
first flight, and suggests ways to continue in future.")
    (license #f)))


;;; Rail Route

(define-public gog-rail-route
  (package
    (name "gog-rail-route")
    (version "2.2.3")
    (source
     (origin
       (method gog-fetch)
       (uri "gogdownloader://rail_route/en3installer0")
       (file-name (format #f "rail_route_~a.sh"
                          (string-replace-substring version "." "_")))
       (sha256 (base32 "1z18c0l24fzyb8awynk7a32ix50b9x0wn3c0bb47142q5ilb3w3z"))))
    (supported-systems '("x86_64-linux"))
    (build-system mojo-build-system)
    (arguments
     `(#:patchelf-plan
       '(("UnityPlayer.so"
          ("gcc:lib" "dbus" "eudev" "mesa" "wayland"
           "libx11" "libxcursor" "libxext" "libxi" "libxinerama"
           "libxkbcommon" "libxrandr" "libxscrnsaver" "libxxf86vm"))
         ("Rail Route.x86" ("gcc:lib"))
         ("Rail Route_Data/MonoBleedingEdge/x86_64/libmonobdwgc-2.0.so" ("gcc:lib" "zlib"))
         ("Rail Route_Data/MonoBleedingEdge/x86_64/libMonoPosixHelper.so" ("zlib"))
         ("Rail Route_Data/Plugins/libfmodstudio.so" ("gcc:lib" "libstdc++" "pulseaudio"))
         ("Rail Route_Data/Plugins/libresonanceaudio.so" ("gcc:lib" "libstdc++" "pulseaudio")))
       #:phases
       ,#~(modify-phases %standard-phases
            ;; The main game executable looks for "UnityPlayer.so" in its
            ;; directory, so add an $ORIGIN entry to its rpath.
            (add-after 'patchelf 'add-origin-to-rpath
              (lambda _
                (use-modules (ice-9 rdelim) (ice-9 popen))
                (let* ((binary "data/noarch/game/Rail Route.x86")
                       (patchelf (open-pipe* OPEN_READ "patchelf" "--print-rpath" binary))
                       (rpath (read-line patchelf)))
                  (close-pipe patchelf)
                  (invoke "patchelf" "--set-rpath" (string-append "$ORIGIN:" rpath) binary)))))))
    (inputs
     `(("dbus" ,dbus)
       ("eudev" ,eudev)
       ("gcc:lib" ,gcc "lib")
       ("libstdc++" ,(make-libstdc++ gcc))
       ("libx11" ,libx11)
       ("libxcursor" ,libxcursor)
       ("libxext" ,libxext)
       ("libxi" ,libxi)
       ("libxinerama" ,libxinerama)
       ("libxkbcommon" ,libxkbcommon)
       ("libxrandr" ,libxrandr)
       ("libxscrnsaver" ,libxscrnsaver)
       ("libxxf86vm" ,libxxf86vm)
       ("mesa" ,mesa)
       ("pulseaudio" ,pulseaudio)
       ("wayland" ,wayland)
       ("zlib" ,zlib)))
    (home-page "https://railroute.eu/")
    (synopsis "Rail dispatching game with a schematic interface")
    (description "Enter the world of rail networks and become the dispatcher
of your dreams!  Rail Route is a management and tycoon game based on railroad
dispatching, offering relaxed, easy to understand (but hard to master)
gameplay.

Create the route of your dreams in a world where you set the pace, and embark
on an ever-expanding railway extravaganza.")
    (license (nonguix-license:undistributable
              "file://data/noarch/docs/End User License Agreement.txt"))))


;;; Minecraft server

;; See versions in:
;; https://piston-meta.mojang.com/mc/game/version_manifest.json
;; ...which links to:
;; https://piston-meta.mojang.com/v1/packages/111890b5a8c2fee9b77036f9f377b33df42c718a/1.20.6.json
;;
;; To get the latest version's server .jar URL and sha1 hash:
;; curl -fSsL "$(curl -fSsL 'https://piston-meta.mojang.com/mc/game/version_manifest.json' | jq -r '.latest.release as $latest | .versions[] | select(.id == $latest and .type == "release") | .url')" | jq -r '.downloads.server | [.url, .sha1] | .[]'

(define-public minecraft-server
  (package
    (name "minecraft-server")
    (version "1.21.4")
    (source
     (origin
       (method url-fetch)
       (file-name (string-append name "-" version ".jar"))
       (uri "https://piston-data.mojang.com/v1/objects/4707d00eb834b446575d89a61a11b5d548d8c001/server.jar")
       (sha256 (base32 "0n0k1zhpirx09w0gg2w3hlmsrh8whx51qabj8n273ip9145rfrhh"))))
    (build-system trivial-build-system)
    (arguments
     `(#:builder
       ,(with-imported-modules
            (source-module-closure '((guix build utils)))
          #~(begin
              (use-modules (guix build utils))
              (for-each (lambda (dir)
                          (mkdir-p (string-append #$output "/" dir)))
                        '("bin" "libexec"))
              (copy-file #$source (string-append #$output "/libexec/server.jar"))
              (call-with-output-file (string-append #$output "/bin/minecraft-server")
                (lambda (port)
                  (format port "#!~a/bin/guile --no-auto-compile~%!#~%" #$guile-3.0)
                  (write `(apply execl #$(file-append openjdk21 "/bin/java")
                                 "java" "-jar" ,(string-append #$output "/libexec/server.jar")
                                 (cdr (command-line)))
                         port)
                  (chmod port #o555)))))))
    (inputs (list openjdk21 guile-3.0))
    (home-page "https://www.minecraft.net/")
    (synopsis "Server for the well-known block building sandbox game")
    (description "Server software for Minecraft.")
    (license (nonguix-license:undistributable
              "https://www.minecraft.net/en-us/eula"))))

(define-public mcmap
  (package
    (name "mcmap")
    (version "3.0.3")
    (home-page "https://github.com/spoutn1k/mcmap")
    (source
     (origin
       (method git-fetch)
       (file-name (git-file-name name version))
       (uri (git-reference
             (url home-page)
             (commit (string-append "v" version))))
       (sha256 (base32 "05k119gb3ccp6qpq1g0msyc0avm8dz4g6ywhr1d67d5fi8d3sgjz"))))
    (build-system cmake-build-system)
    (arguments
     `(#:configure-flags '("-DTESTS_=1")   ; compile tests
       #:phases
       ,#~(modify-phases %standard-phases
            (add-after 'unpack 'unvendor
              (lambda _
                ;; The file must still exist, else CMake fails.  Just empty it.
                (truncate-file "src/include/CMakeLists.txt" 0)
                (symlink #+(file-append nlohmann-json "/include/nlohmann/json.hpp")
                         "src/include/json.hpp")))
            (replace 'check
              (lambda* (#:key tests? #:allow-other-keys)
                (when tests?
                  (invoke "bin/run_tests"))))
            (replace 'install
              (lambda* (#:key outputs #:allow-other-keys)
                (let ((bindir (string-append (assoc-ref outputs "out") "/bin")))
                  (for-each (lambda (file)
                              (install-file (string-append "bin/" file) bindir))
                            '("mcmap" "json2bson" "nbt2json"
                              "chunkPos" "extractChunk" "regionReader"))))))))
    (inputs (list libpng zlib fmt spdlog))
    (native-inputs (list nlohmann-json googletest))
    (synopsis "Create isometric renders of your Minecraft save file")
    (description "An isometric map visualizer for Minecraft worlds, generating
an image file or tiles for use in a Leaflet map.")
    (license license:gpl3)))
