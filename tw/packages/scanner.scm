(define-module (tw packages scanner)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages scanner)
  #:use-module (guix packages)
  #:use-module ((guix utils) #:select (substitute-keyword-arguments)))

(define-public sane-backends/airscan
  (package/inherit sane-backends
    (name "sane-airscan-backends")
    (inputs
     (let ((inputs (list-copy (package-inputs sane-backends))))
       (assq-remove! inputs "hplip")
       `(("sane-airscan" ,sane-airscan) ,@inputs)))
    (arguments
     (substitute-keyword-arguments (package-arguments sane-backends)
       ((#:phases phases)
        `(modify-phases ,phases
           (delete 'add-backends)   ; for hplip
           (delete 'install-hpaio)  ; for hplip
           (add-after 'install 'install-airscan
             (lambda* (#:key inputs outputs #:allow-other-keys)
               (let ((out (assoc-ref outputs "out"))
                     (airscan (assoc-ref inputs "sane-airscan")))
                 (for-each (lambda (file)
                             (symlink file (string-append out "/lib/sane/" (basename file))))
                           (find-files (string-append airscan "/lib/sane")))
                 (symlink (string-append airscan "/etc/sane.d/airscan.conf")
                          (string-append out "/etc/sane.d/airscan.conf"))
                 (symlink (string-append airscan "/etc/sane.d/dll.d/airscan")
                          (string-append out "/etc/sane.d/dll.d/airscan")))))))))
    (description "SANE stands for \"Scanner Access Now Easy\" and is an API
proving access to any raster image scanner hardware (flatbed scanner,
hand-held scanner, video- and still-cameras, frame-grabbers, etc.).  The
package contains the library and drivers, including the sane-airscan driver,
but not the hpaio backend provided by hplip.")))

;; TODO: is this needed or is a running ipp-usb daemon alone enough?
(define-public simple-scan/airscan
  (package/inherit simple-scan
    (inputs (modify-inputs (package-inputs simple-scan)
              (replace "sane-backends" sane-backends/airscan)))))
