(define-module (tw packages mail)
  #:use-module (gnu packages mail)
  #:use-module (gnu packages python)
  #:use-module (guix build-system copy)
  #:use-module (guix git-download)
  #:use-module (guix packages))

(define-public mutt_oauth2.py
  (package
    (name "mutt_oauth2.py")
    (version "20250113")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/neomutt/neomutt")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0laxwjb2liz6faf1lh95wdz65b4n6jf4x6lcvwa8zvqrps19ljyz"))))
    (inputs (list python))  ; It's a Python script!
    (build-system copy-build-system)
    (arguments
     '(#:install-plan '(("contrib/oauth2/mutt_oauth2.py" "bin/"))))
    (home-page (package-home-page neomutt))
    (synopsis "OAuth2 authentication script from NeoMutt")
    (description "Microsoft Exchange Online mail accounts require XOAUTH2
login.  This can be done with the @code{mutt_oauth2.py} script packaged with
NeoMutt, but the version of the script in Guix' NeoMutt package is too old, so
this package includes the latest script only.")
    (license (package-license neomutt))))
