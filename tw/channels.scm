(define-module (tw channels)
  #:use-module (gnu services)
  #:use-module (gnu services base)
  #:use-module (guix channels)
  #:use-module (guix gexp)
  #:use-module (guix modules))

(define-public %system-channels
  ;; Channel `tw' depends on `nonguix' and others.
  ;; See also `.guix-channel' in this repo.
  (cons* (channel
          (name 'tw)
          (url "https://git.twilken.net/dotfiles")
          (branch "master")
          (introduction
           (make-channel-introduction
            "f9036f78b4b4f4f35f52b3584dd5a3a747b498bf"
            (openpgp-fingerprint
             "53EC 3C06 8568 83DD 9235  5BC2 2FC7 8504 681F 69B0"))))
         %default-channels))

;; Nonguix substitute server's signing key.
;; From <https://substitutes.nonguix.org/signing-key.pub>.
(define-public %nonguix-signing-key
  (plain-file "nonguix-signing-key.pub"
    "(public-key (ecc (curve Ed25519) (q #C1FD53E5D4CE971933EC50C9F307AE2171A2D3B52C804642A7A35F84F3A4EA98#)))"))

;; Authorize other machines' Guix signing keys, for offloading.
(define %tw-signing-keys
  (list
   (plain-file "lud-signing-key.pub"
     "(public-key (ecc (curve Ed25519) (q #907985F2DEC4E15FAF29F4029FEADD266DD6563F7E9548160965C7E61EEDCA51#)))")
   (plain-file "vin-signing-key.pub"
     "(public-key (ecc (curve Ed25519) (q #752B01B2CC6E1730BFD8F2B4CF9B9D3D29A07B4B7763EACBD8DEEE9F46E96561#)))")
   (plain-file "btl-signing-key.pub"
     "(public-key (ecc (curve Ed25519) (q #9CF21E4E6F6B176B94209B30F9DFF6A026A7F265A31FADD5964A857DC4DE27EC#)))")
   (plain-file "lap-signing-key.pub"
     "(public-key (ecc (curve Ed25519) (q #DF75857BAD66DB4ED4F3DC6450C9E0923D3162435FDE281B536F40EA22FAD6CF#)))")
   (plain-file "frm-signing-key.pub"
     "(public-key (ecc (curve Ed25519) (q #E0E0A927949FF46DD5B7A63334BC168DC63D6C90D4F9AD6C071A4520B8B659A7#)))")))

(define-public %system-channel-services
  (list (simple-service 'nonguix guix-service-type
          (guix-extension
           (authorized-keys (list %nonguix-signing-key))
           (substitute-urls '("https://substitutes.nonguix.org"))))

        (simple-service 'offloading guix-service-type
          (guix-extension
           (authorized-keys %tw-signing-keys)))))
