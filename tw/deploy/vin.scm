(define-module (tw deploy vin)
  #:use-module (tw deploy)
  #:use-module (tw system vin))

(list (operating-system->machine %vin-system))
