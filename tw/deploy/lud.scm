(define-module (tw deploy lud)
  #:use-module (tw deploy)
  #:use-module (tw system lud))

(list (operating-system->machine %lud-system))
