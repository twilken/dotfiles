;; This is an operating system configuration file for a fairly minimal
;; "desktop" setup with i3 where the /home partition partition is
;; encrypted with LUKS.
;;
;; https://guix.gnu.org/manual/en/html_node/operating_002dsystem-Reference.html

(define-module (tw system lap)
  #:use-module (gnu)
  #:use-module (gnu bootloader grub)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages shells)
  #:use-module (gnu services admin)
  #:use-module (gnu services desktop)
  #:use-module (gnu services pm)
  #:use-module (gnu services shepherd)
  #:use-module (gnu services syncthing)
  #:use-module (gnu system locale)
  #:use-module (gnu system nss)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module ((guix utils)
                #:select (substitute-keyword-arguments))
  #:use-module ((nongnu packages linux)
                #:prefix nongnu:)   ; don't interfere with (gnu packages linux)
  #:use-module ((nongnu packages nvidia)
                #:prefix nongnu:)
  #:use-module ((nongnu services nvidia)
                #:prefix nongnu:)
  #:use-module ((nongnu system linux-initrd)
                #:prefix nongnu:)
  #:use-module (nonguix licenses)
  #:use-module (tw channels)
  #:use-module (tw services secrets)
  #:use-module (tw system))

(define efi-system-partition          ; /dev/nvme0n1p1
  (uuid "E04A-DF05" 'fat))
(define tilearch-root-partition       ; /dev/nvme0n1p2
  (uuid "197c7f98-c780-4017-9bf7-212175967fd0" 'btrfs))
(define guixsd-root-partition         ; /dev/nvme0n1p3
  (uuid "2169a07b-b7b6-4e3d-8e59-bf00d0baffc4" 'btrfs))
(define swap-partition                ; /dev/nvme0n1p4
  (uuid "2c78d507-5cd1-48da-ba8d-d1b01d6ff8f9"))
(define data-partition-outside-luks   ; /dev/sda1
  (uuid "43108ff6-b25a-4a53-b3f7-6d03ff3e0022"))
(define data-partition-inside-luks    ; /dev/mapper/data
  (uuid "826bb1a7-1069-46a5-897a-afa19272fbb0" 'btrfs))

(define-public %lap-system
  (operating-system
    (host-name "lap.twilken.net")
    (timezone "Europe/Paris")
    (locale "en_GB.utf8")
    (locale-definitions
     (list (locale-definition (name "en_GB.utf8") (source "en_GB"))
           (locale-definition (name "en_US.utf8") (source "en_US"))
           (locale-definition (name "fr_FR.utf8") (source "fr_FR"))))

    ;; Allow resolution of '.local' host names with mDNS.
    (name-service-switch %mdns-host-lookup-nss)

    ;; Choose UK English X11 keyboard layout.
    (keyboard-layout %british-keyboard)

    ;; Use the UEFI variant of GRUB with the EFI System
    ;; Partition mounted on /boot/efi.
    (bootloader
     (bootloader-configuration
      (bootloader grub-efi-bootloader)
      (targets '("/boot/efi"))
      ;; Note: keyboard-layout is ignored by non-grub bootloaders.
      (keyboard-layout keyboard-layout)
      (menu-entries
       (list
        (menu-entry
         (label "systemd-boot")
         (device efi-system-partition)
         (chain-loader "/EFI/systemd/systemd-bootx64.efi"))))))

    ;; Use non-free kernel to load non-free firmware (e.g. for wifi).
    ;; This needs to be linux-lts to allow building nvidia-module from nonguix.
    (kernel nongnu:linux-lts)
    (kernel-arguments
     (cons* ;;"nosplash"
      ;;"vt.global_cursor_default=0"
      ;;"video.use_native_backlight=1"
      ;; Needed for PRIME synchronization:
      ;; https://wiki.archlinux.org/title/PRIME#PRIME_synchronization
      ;; https://wiki.archlinux.org/title/NVIDIA#DRM_kernel_mode_setting
      "nvidia-drm.modeset=1"
      ;;"acpi_osi=\"!Windows 2015\""
      ;;"acpi_enforce_resources=lax"
      "modprobe.blacklist=nouveau"  ; prevent interference with nvidia
      %default-kernel-arguments))
    (initrd nongnu:microcode-initrd)
    (firmware (cons* nongnu:atheros-firmware   ; for atk10k/QCA6174/hw3.0 (wifi card)
                     nongnu:i915-firmware      ; for Intel GPU runtime power management etc
                     %base-firmware))

    ;; Specify a mapped device for the encrypted home partition.
    ;; The UUID is that returned by 'cryptsetup luksUUID'.
    (mapped-devices
     (list (mapped-device
            (source data-partition-outside-luks)
            (target "data")
            (type luks-device-mapping))))

    (swap-devices
     (list (swap-space (target swap-partition) (discard? #t))))

    (file-systems
     (cons* (file-system
              (device guixsd-root-partition)
              (mount-point "/")
              (flags '(no-atime))
              (options (alist->file-system-options
                        '("ssd" ("compress" . "zstd"))))
              (type "btrfs"))
            (file-system
              (device efi-system-partition)
              (mount-point "/boot/efi")
              (flags '(no-atime))
              (type "vfat"))
            (file-system
              (device data-partition-inside-luks)
              (mount-point "/home")
              (flags '(no-atime))
              (options (alist->file-system-options
                        '("ssd" ("compress" . "zstd")
                          ("subvol" . "home/guixsd"))))
              (type "btrfs")
              (dependencies mapped-devices))
            %base-file-systems))

    ;; Members of the wheel group are allowed to use sudo.
    (users (cons* (user-account
                   (name "timo")
                   (comment "Timo Wilken")
                   (group "users")
                   (supplementary-groups
                    '("wheel" "audio" "video" "docker" "adbusers"))
                   (shell (file-append zsh "/bin/zsh")))
                  %base-user-accounts))

    (sudoers-file (desktop-sudoers #:wayland? #f))

    ;; This is where we specify system-wide packages.
    ;; Proprietary NVIDIA driver. See its description for more required setup steps.
    (packages (list nongnu:nvidia-driver))

    ;; Use the "desktop" services, which include the X11
    ;; log-in service, networking with NetworkManager, and more.
    ;; See info '(guix)Services' for useful services.
    (services
     (cons*
      (service syncthing-service-type
        (syncthing-configuration
         (user "timo")))

      (service bluetooth-service-type)

      (service tlp-service-type)   ; TODO: configure properly

      ;; Disabled as it doesn't work with my hardware.
      ;; It always says "logging in with fingerprint failed" and blocks password login in gdm.
      ;; (service fprintd-service-type)

      (service nongnu:nvidia-service-type)  ; proprietary nvidia driver

      ;; For some reason, none of the NVIDIA things create this device file
      ;; automatically, even though the NVIDIA Xorg driver requires it (Xorg
      ;; fails to start otherwise).
      (simple-service 'create-nvidiactl shepherd-root-service-type
        (list
         (shepherd-service
          (documentation "Create /dev/nvidiactl.")
          (provision '(nvidiactl))
          (requirement '(nvidia))
          (one-shot? #t)
          (start #~(make-forkexec-constructor
                    (list #$(file-append coreutils "/bin/mknod")
                          "-m" "666" "/dev/nvidiactl" "c" "195" "255")))
          (stop #~(make-forkexec-constructor
                   (list #$(file-append coreutils "/bin/rm")
                         "-f" "/dev/nvidiactl"))))))

      (service unattended-upgrade-service-type
        (unattended-upgrade-configuration
         (schedule "0 21 * * *")       ; every night at 21:00, when the laptop is turned on
         (maximum-duration (* 40 60))  ; 40 minutes to allow for slow downloads
         (channels #~(@ (tw channels) %system-channels))
         (operating-system-expression
          #~(@ (tw system lap) %lap-system))
         (services-to-restart          ; TODO: list all Shepherd timers
          ;; Anything that won't cause disruption when restarting.
          '(unattended-upgrade syncthing-timo earlyoom thermald tlp wireguard-wg0))))

      ;; Set up a secrets config for WireGuard to extend.
      (service secrets-service-type
        (secrets-configuration
         ;; TODO: reencrypt for SSH key.
         (host-key "/etc/secrets.key")))  ; we have no SSH host keys, so use a custom key

      (enduser-system-services
       #:host-name host-name
       #:cores 8
       #:wireless-interface "wlp3s0"
       #:xorg-extra-modules (list nongnu:nvidia-driver)
       ;; TODO: run acpid so nvidia driver can connect?
       ;; See `guix show nvidia-driver'
       #:xorg-drivers '("nvidia" "modesetting"))))))

%lap-system
