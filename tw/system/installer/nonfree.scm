(define-module (tw system installer nonfree)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages package-management)
  #:use-module (gnu packages version-control)
  #:use-module (gnu services)
  #:use-module (gnu services base)
  #:use-module (gnu services ssh)
  #:use-module (gnu system)
  #:use-module (guix channels)
  #:use-module (guix gexp)
  #:use-module ((nongnu packages linux)
                #:prefix nongnu:)   ; don't interfere with (gnu packages linux)
  #:use-module ((nongnu system install)
                #:select (installation-os-nonfree))
  #:use-module ((nongnu system linux-initrd)
                #:prefix nongnu:)
  #:use-module (tw channels))

(operating-system
  (inherit installation-os-nonfree)
  (host-name "guixinstall")
  (timezone "Europe/Paris")
  (locale "en_GB.utf8")

  ;; Use non-free kernel to load non-free firmware (e.g. for wifi).
  ;; Enable MT7921 module for Mediatek MT7922 (AMD RZ616) WiFi card.
  ;; The MT7921E module is for the card connected via PCIe, which it is
  ;; (it's in an M.2 slot).  Alternatives are S (SDIO) and U (USB).
  (kernel (nongnu:corrupt-linux linux-libre #:configs '("CONFIG_MT7921E=m")))
  (initrd nongnu:microcode-initrd)
  (firmware (cons* nongnu:amdgpu-firmware nongnu:mediatek-firmware %base-firmware))

  (packages
   (cons* efibootmgr htop git
          (operating-system-packages installation-os-nonfree)))

  (services
   (modify-services (append %system-channel-services ((@@ (gnu system install) %installation-services)))
     ;; Set up an SSH server to facilitate remote installs.
     (openssh-service-type
      config =>
      (openssh-configuration
       (inherit config)
       (port-number 22)
       (%auto-start? #t)
       (permit-root-login #t)
       ;; The root account is passwordless, so make sure
       ;; a password is set before allowing logins.
       (allow-empty-passwords? #f)
       (password-authentication? #f)
       (authorized-keys
        `(("root" ,(local-file "../files/timo.pub"))))))

     ;; Add this channel, so we can install nonfree packages directly.
     (guix-service-type
      config =>
      (guix-configuration
       (inherit config)
       (channels %system-channels))))))
