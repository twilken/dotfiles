;; To build this image:
;; guix system image -t iso9660 --label="GUIX_CUSTOM_$(date -Idate)" tw/system/installer/free.scm

(define-module (tw system installer free)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages package-management)
  #:use-module (gnu packages version-control)
  #:use-module (gnu services)
  #:use-module (gnu services ssh)
  #:use-module (gnu system)
  #:use-module (gnu system install)
  #:use-module ((guix gexp) #:select (local-file))
  #:use-module ((srfi srfi-26) #:select (cut)))

(operating-system
  (inherit installation-os)
  (host-name "guixinstall")
  (timezone "Europe/Paris")
  (locale "en_GB.utf8")
  (packages
   (cons* efibootmgr htop git
          (operating-system-packages installation-os)))
  (services
   (cons*
    ;; Add an SSH server to facilitate remote installs.
    (service openssh-service-type
      (openssh-configuration
       (port-number 22)
       (%auto-start? #t)
       (permit-root-login #t)
       ;; The root account is passwordless, so make sure
       ;; a password is set before allowing logins.
       (allow-empty-passwords? #f)
       (password-authentication? #f)
       (authorized-keys
        `(("root" ,(local-file "../files/timo.pub"))))))
    ;; Make sure to remove the SSH service from the base services,
    ;; so we can override it fully.
    (filter
     (compose not (cut eq? openssh-service-type <>) service-kind)
     ((@@ (gnu system install) %installation-services))))))
