;; This is an operating system configuration file for a fairly minimal
;; "desktop" setup with i3 where the /home partition partition is
;; encrypted with LUKS.
;;
;; https://guix.gnu.org/manual/en/html_node/operating_002dsystem-Reference.html

(define-module (tw system frm)
  #:use-module (gnu)
  #:use-module (gnu bootloader grub)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages shells)
  #:use-module (gnu services admin)
  #:use-module (gnu services desktop)
  #:use-module (gnu services linux)
  #:use-module (gnu services pm)
  #:use-module (gnu services syncthing)
  #:use-module (gnu system locale)
  #:use-module (gnu system nss)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module ((guix utils)
                #:select (substitute-keyword-arguments))
  #:use-module ((nongnu packages linux)
                #:prefix nongnu:)   ; don't interfere with (gnu packages linux)
  #:use-module ((nongnu system linux-initrd)
                #:prefix nongnu:)
  #:use-module ((nonguix licenses)
                #:prefix nongnu:)
  #:use-module (tw channels)
  #:use-module (tw services secrets)
  #:use-module (tw system))

(define efi-system-partition          ; /dev/nvme0n1p1
  (uuid "D8C7-2624" 'fat))
(define root-partition                ; /dev/nvme0n1p2
  (uuid "62fb4710-33d1-4eaf-aaaa-43d16ab26a58" 'btrfs))

(define-public %frm-system
  (operating-system
    (host-name "frm.twilken.net")
    (timezone "Europe/Paris")
    (locale "en_GB.utf8")
    (locale-definitions
     (list (locale-definition (name "en_GB.utf8") (source "en_GB"))
           (locale-definition (name "en_US.utf8") (source "en_US"))
           (locale-definition (name "fr_FR.utf8") (source "fr_FR"))))

    ;; Allow resolution of '.local' host names with mDNS.
    (name-service-switch %mdns-host-lookup-nss)

    ;; Choose UK English X11 keyboard layout.
    (keyboard-layout %british-keyboard)

    ;; Use the UEFI variant of GRUB with the EFI System
    ;; Partition mounted on /boot/efi.
    (bootloader
     (bootloader-configuration
      (bootloader grub-efi-bootloader)
      (targets '("/boot/efi"))
      ;; Note: keyboard-layout is ignored by non-grub bootloaders.
      (keyboard-layout keyboard-layout)))

    ;; Use non-free kernel to load non-free firmware (e.g. for wifi).
    ;; Enable MT7921 module for Mediatek MT7922 (AMD RZ616) WiFi card.
    ;; The MT7921E module is for the card connected via PCIe, which it is
    ;; (it's in an M.2 slot).  Alternatives are S (SDIO) and U (USB).
    (kernel (nongnu:corrupt-linux linux-libre #:configs '("CONFIG_MT7921E=m")))
    (kernel-loadable-modules (list v4l2loopback-linux-module))
    ;; Fix screen flashing white: https://community.frame.work/t/39073
    (kernel-arguments (cons* "amdgpu.sg_display=0" %default-kernel-arguments))
    (initrd nongnu:microcode-initrd)
    (firmware (cons* nongnu:amdgpu-firmware nongnu:mediatek-firmware %base-firmware))

    (file-systems
     (cons* (file-system
              (device root-partition)
              (mount-point "/")
              (flags '(no-atime))
              (options (alist->file-system-options
                        '("ssd" ("compress" . "zstd"))))
              (type "btrfs"))
            (file-system
              (device efi-system-partition)
              (mount-point "/boot/efi")
              (flags '(no-atime))
              (type "vfat"))
            ;; Put /home in a subvolume for better accounting/snapshotting potential.
            (file-system
              (device root-partition)
              (mount-point "/home")
              (flags '(no-atime))
              (options (alist->file-system-options
                        '("ssd" ("compress" . "zstd")
                          ("subvol" . "home"))))
              (type "btrfs"))
            %base-file-systems))

    ;; Members of the wheel group are allowed to use sudo.
    (users (cons* (user-account
                   (name "timo")
                   (comment "Timo Wilken")
                   (group "users")
                   (supplementary-groups
                    '("wheel" "audio" "video" "docker" "adbusers"))
                   (shell (file-append zsh "/bin/zsh")))
                  %base-user-accounts))

    (sudoers-file (desktop-sudoers #:wayland? #t))

    ;; Use the "desktop" services, which include the X11
    ;; log-in service, networking with NetworkManager, and more.
    ;; See info '(guix)Services' for useful services.
    (services
     (cons*
      (service syncthing-service-type
        (syncthing-configuration
         (user "timo")))

      (service bluetooth-service-type)

      (service kernel-module-loader-service-type '("v4l2loopback"))

      ;; fprintd complains about missing firmware, but fwpud doesn't find any.
      ;; (service fprintd-service-type)

      (service unattended-upgrade-service-type
        (unattended-upgrade-configuration
         (schedule "0 21 * * *")       ; every night at 21:00, when the laptop is turned on
         (maximum-duration (* 40 60))  ; 40 minutes to allow for slow downloads
         (channels #~(@ (tw channels) %system-channels))
         (operating-system-expression
          #~(@ (tw system frm) %frm-system))
         (services-to-restart          ; TODO: list all Shepherd timers
          ;; Anything that won't cause disruption when restarting.
          '(unattended-upgrade syncthing-timo earlyoom tlp wireguard-wg0))))

      ;; Set up a secrets config for WireGuard to extend.
      (service secrets-service-type)

      (modify-services (enduser-system-services
                        #:host-name host-name
                        #:cores 12
                        #:wayland? #t
                        #:wireless-interface "wlp1s0")
        (delete thermald-service-type))))))

%frm-system
