;; This is an operating system configuration file for a fairly minimal
;; "desktop" setup with i3 where the /home partition partition is
;; encrypted with LUKS.
;;
;; https://guix.gnu.org/manual/en/html_node/operating_002dsystem-Reference.html

(define-module (tw system btl)
  #:use-module (gnu)
  #:use-module (gnu bootloader grub)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages shells)
  #:use-module (gnu services admin)
  #:use-module (gnu services desktop)
  #:use-module (gnu services pm)
  #:use-module (gnu services syncthing)
  #:use-module (gnu system locale)
  #:use-module (gnu system nss)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module ((nongnu packages linux)
                #:prefix nongnu:)   ; don't interfere with (gnu packages linux)
  #:use-module ((nongnu system linux-initrd)
                #:prefix nongnu:)
  #:use-module ((nonguix licenses)
                #:prefix nongnu:)
  #:use-module (tw channels)
  #:use-module (tw services secrets)
  #:use-module (tw system))

(define efi-system-partition          ; /dev/nvme1n1p1
  (uuid "D722-BEEE" 'fat))
(define root-partition                ; /dev/nvme1n1p2
  (uuid "0d47bc5c-de3c-47bb-b108-19b4a57bd2bc" 'btrfs))
(define data-partition                ; /dev/nvme0n1p1
  (uuid "cd4b7af3-f5ce-44a0-a986-3271078fb1d6" 'btrfs))

;; This may fix AMDGPU crashes in Minecraft with the following errors in dmesg:
;;   [drm:amdgpu_dm_plane_helper_prepare_fb [amdgpu]] *ERROR* Failed to pin framebuffer with error -22
;; and the following errors in the Minecraft log:
;;   amdgpu: amdgpu_cs_query_fence_status failed.
;;   amdgpu: The CS has been rejected (-125).
;; https://old.reddit.com/r/AMDHelp/comments/17yn72h/page_flip_fails_error22game_crash/
(define amdgpu-reduced-output-latency "\
Section \"OutputClass\"
    Identifier \"AMDgpu\"
    MatchDriver \"amdgpu\"
    Driver \"amdgpu\"
    Option \"EnablePageFlip\" \"off\"
    Option \"TearFree\" \"false\"
EndSection
")

(define-public %btl-system
  (operating-system
    (host-name "btl.twilken.net")
    (timezone "Europe/Paris")
    (locale "en_GB.utf8")
    (locale-definitions
     (list (locale-definition (name "en_GB.utf8") (source "en_GB"))
           (locale-definition (name "en_US.utf8") (source "en_US"))
           (locale-definition (name "fr_FR.utf8") (source "fr_FR"))))

    ;; Allow resolution of '.local' host names with mDNS.
    (name-service-switch %mdns-host-lookup-nss)

    ;; Choose UK English X11 keyboard layout.
    (keyboard-layout %british-keyboard)

    ;; Use the UEFI variant of GRUB with the EFI System
    ;; Partition mounted on /boot/efi.
    (bootloader
     (bootloader-configuration
      (bootloader grub-efi-bootloader)
      (targets '("/boot/efi"))
      ;; Note: keyboard-layout is ignored by non-grub bootloaders.
      (keyboard-layout keyboard-layout)))

    ;; Use non-free kernel to load non-free firmware (e.g. for wifi).
    ;; Enable MT7921 module for Mediatek MT7922 (AMD RZ616) WiFi card.
    ;; The MT7921E module is for the card connected via PCIe, which it is
    ;; (it's in an M.2 slot).  Alternatives are S (SDIO) and U (USB).
    (kernel (nongnu:corrupt-linux linux-libre #:configs '("CONFIG_MT7921E=m")))
    (kernel-arguments
     ;; Make printing work by blacklisting the broken "usblp" kernel module.
     (cons* "modprobe.blacklist=usblp" %default-kernel-arguments))
    (initrd nongnu:microcode-initrd)
    (firmware (cons* nongnu:amdgpu-firmware nongnu:mediatek-firmware %base-firmware))

    (file-systems
     (cons* (file-system
              (device root-partition)
              (mount-point "/")
              (flags '(no-atime))
              (options (alist->file-system-options
                        '("ssd" ("compress" . "zstd"))))
              (type "btrfs"))
            (file-system
              (device efi-system-partition)
              (mount-point "/boot/efi")
              (flags '(no-atime))
              (type "vfat"))
            ;; Put /home in a subvolume for better accounting/snapshotting potential.
            (file-system
              (device data-partition)
              (mount-point "/home")
              (flags '(no-atime))
              (options (alist->file-system-options
                        '("ssd" ("compress" . "zstd"))))
              (type "btrfs"))
            %base-file-systems))

    ;; Members of the wheel group are allowed to use sudo.
    (users (cons* (user-account
                   (name "timo")
                   (comment "Timo Wilken")
                   (group "users")
                   (supplementary-groups
                    '("wheel" "audio" "video" "docker" "adbusers"))
                   (shell (file-append zsh "/bin/zsh")))
                  %base-user-accounts))

    (sudoers-file (desktop-sudoers #:wayland? #f))

    ;; Use the "desktop" services, which include the X11
    ;; log-in service, networking with NetworkManager, and more.
    ;; See info '(guix)Services' for useful services.
    (services
     (cons*
      (service syncthing-service-type
        (syncthing-configuration
         (user "timo")))

      (service bluetooth-service-type)

      (service unattended-upgrade-service-type
        (unattended-upgrade-configuration
         (schedule "0 21 * * *")       ; every night at 21:00, when the PC is turned on
         (maximum-duration (* 40 60))  ; 40 minutes to allow for slow downloads
         (channels #~(@ (tw channels) %system-channels))
         (operating-system-expression
          #~(@ (tw system btl) %btl-system))
         (services-to-restart          ; TODO: list all Shepherd timers
          ;; Anything that won't cause disruption when restarting.
          '(unattended-upgrade syncthing-timo earlyoom wireguard-wg0))))

      (service guix-publish-service-type
        (guix-publish-configuration
         (host "0.0.0.0")
         (port 7240)      ; arbitrary high port
         (advertise? #t)
         ;; Disable compression to avoid breaking download progress bars (maybe?).
         ;; The local network is fast enough that compression doesn't matter, and it saves CPU.
         (compression '())))

      (modify-services (enduser-system-services
                        #:host-name host-name
                        #:cores 24
                        #:wireless-interface "wlp15s0"
                        #:xorg-extra-config (list amdgpu-reduced-output-latency))
        (delete thermald-service-type))))))

%btl-system
