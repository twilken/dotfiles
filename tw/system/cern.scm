;; This is an operating system configuration file for a fairly minimal
;; "desktop" setup with i3 where the /home partition partition is
;; encrypted with LUKS.
;;
;; https://guix.gnu.org/manual/en/html_node/operating_002dsystem-Reference.html

(define-module (tw system cern)
  #:use-module (gnu)
  #:use-module (gnu bootloader grub)
  #:use-module (gnu system locale)
  #:use-module (gnu system nss)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module ((nongnu packages linux) #:prefix nongnu:)
  #:use-module ((nongnu system linux-initrd) #:prefix nongnu:)
  #:use-module (tw channels)
  #:use-module (tw services wireguard)
  #:use-module (tw system))

(use-package-modules certs cups disk docker file-systems freedesktop gnome
                     kerberos linux mtools pulseaudio search shells
                     virtualization wm xorg)

(use-service-modules admin authentication avahi base cups dbus desktop docker
                     kerberos linux mcron networking shepherd ssh syncthing
                     virtualization vpn xorg)

(define efi-system-partition          ; /dev/nvme0n1p1
  (uuid "7E1B-8A5B" 'fat))
(define guixsd-root-partition         ; /dev/nvme0n1p2
  (uuid "933d1bec-14ec-4421-aa4d-93846c68f27c" 'btrfs))

(define-public %cern-system
  (operating-system
    (host-name "twilkenlaptop.cern.ch")
    (timezone "Europe/Zurich")
    (locale "en_GB.utf8")
    (locale-definitions
     (list (locale-definition (name "en_GB.utf8") (source "en_GB"))
           (locale-definition (name "en_US.utf8") (source "en_US"))
           (locale-definition (name "fr_FR.utf8") (source "fr_FR"))))

    ;; Use non-free kernel to load non-free firmware (e.g. for wifi).
    ;; Enable MT7921 module for Mediatek MT7922 (AMD RZ616) WiFi card.
    ;; The MT7921E module is for the card connected via PCIe, which it is
    ;; (it's in an M.2 slot).  Alternatives are S (SDIO) and U (USB).
    (kernel nongnu:linux)
    (kernel-loadable-modules (list v4l2loopback-linux-module))
    (initrd nongnu:microcode-initrd)
    (firmware (cons* nongnu:i915-firmware nongnu:ibt-hw-firmware nongnu:sof-firmware
                     nongnu:iwlwifi-firmware %base-firmware))

    ;; Allow resolution of '.local' host names with mDNS.
    (name-service-switch %mdns-host-lookup-nss)

    ;; Choose UK English X11 keyboard layout.
    (keyboard-layout %british-keyboard)

    ;; Use the UEFI variant of GRUB with the EFI System
    ;; Partition mounted on /boot/efi.
    (bootloader
     (bootloader-configuration
      (bootloader grub-efi-bootloader)
      (targets '("/boot/efi"))
      ;; Note: keyboard-layout is ignored by non-grub bootloaders.
      (keyboard-layout keyboard-layout)))

    (file-systems
     (cons* (file-system
              (device guixsd-root-partition)
              (mount-point "/")
              (flags '(no-atime))
              (options (alist->file-system-options
                        '("ssd" ("compress" . "zstd"))))
              (type "btrfs"))
            (file-system
              (device efi-system-partition)
              (mount-point "/boot/efi")
              (flags '(no-atime))
              (type "vfat"))
            %base-file-systems))

    ;; Members of the wheel group are allowed to use sudo.
    (users (cons* (user-account
                   (name "twilken")
                   (comment "Timo Wilken")
                   (group "users")
                   (supplementary-groups
                    '("wheel" "audio" "video" "docker" "libvirt"))
                   (shell (file-append zsh "/bin/zsh")))
                  %base-user-accounts))

    (sudoers-file (desktop-sudoers #:wayland? #t))

    (packages (cons* virt-manager %base-packages))

    ;; Use the "desktop" services, which include the X11
    ;; log-in service, networking with NetworkManager, and more.
    ;; See info '(guix)Services' for useful services.
    (services
     (cons*
      (service virtlog-service-type)
      (service libvirt-service-type
        (libvirt-configuration
         (listen-tls? #f) (listen-tcp? #f)))  ; allow UNIX socket access only

      (service bluetooth-service-type)

      (service kernel-module-loader-service-type '("v4l2loopback"))

      (service openntpd-service-type
        (openntpd-configuration
         ;; Use CERN time servers.  Fall back to default servers outside CERN
         ;; (in `servers' key, so not overwritten).
         (server '("ip-time-0.cern.ch"
                   "ip-time-1.cern.ch"
                   "ip-time-2.cern.ch"))))

      (modify-services (enduser-system-services
                        #:host-name host-name
                        #:cores 12
                        #:work-system? #t
                        #:wayland? #t
                        #:wireless-interface "wlp0s20f3")
        ;; openntpd works better.
        (delete ntp-service-type)
        (delete tw-wireguard-service-type))))))

%cern-system
