;; Package the following:
;; TODO: https://github.com/catppuccin/grub
;; TODO: https://github.com/catppuccin/tty

;; Manual installation needed?
;; https://github.com/catppuccin/firefox      ; for icecat
;; https://github.com/catppuccin/dark-reader  ; bundled with the extension, but needs to be selected

(define-module (tw theme)
  #:use-module ((gnu packages gnome)
                #:select (gnome-backgrounds))
  #:use-module ((gnu packages inkscape)
                #:select (inkscape))
  #:use-module (guix gexp)
  #:use-module ((guix modules)
                #:select (source-module-closure))
  #:use-module (tw packages catppuccin))

(define-public tw-background
  (computed-file "tw-background.png"
    #~(execl #+(file-append inkscape "/bin/inkscape") "inkscape" "-C" "-o" #$output
             #+(file-append gnome-backgrounds "/share/backgrounds/gnome/blobs-d.svg"))))

;; One of "latte" (light theme), "frappe", "macchiato", "mocha" (dark
;; themes); ordered brightest to darkest.
;; Set and use this at read time so that `local-file' gets a literal
;; argument. Anything else confuses it and causes it to search
;; relative to the working directory, not this file's directory.
(define-public catppuccin-theme-variant "mocha")
(define-public catppuccin-theme-accent "blue")

;; "Base" colour from the active Catppuccin theme (see
;; https://github.com/catppuccin/catppuccin).
(define-public catppuccin-background-color "1e1e2e")

(define-public catppuccin-gtk-theme
  (make-catppuccin-gtk-theme catppuccin-theme-variant
                             catppuccin-theme-accent))

(define-public catppuccin-cursors
  ;; The "dark" cursors look better than brightly-coloured ones, so ignore
  ;; `catppuccin-theme-accent' here.
  (make-catppuccin-cursors
   catppuccin-theme-variant
   (if (string=? "latte" catppuccin-theme-variant) "light" "dark")))

(define-public catppuccin-aerc
  (file-append catppuccin-aerc-theme
               (string-append "/share/catppuccin/aerc/catppuccin-"
                              catppuccin-theme-variant)))

(define-public catppuccin-newsboat
  (file-append catppuccin-newsboat-theme
               (if (string=? catppuccin-theme-variant "latte")
                   "/share/catppuccin/newsboat/latte"
                   "/share/catppuccin/newsboat/dark")))

(define-public catppuccin-dunstrc
  (file-append catppuccin-dunst-theme
               (string-append "/share/catppuccin/dunst/"
                              catppuccin-theme-variant ".conf")))

(define-public catppuccin-foot
  (file-append catppuccin-foot-theme
               (string-append "/share/catppuccin/foot/catppuccin-"
                              catppuccin-theme-variant ".ini")))

(define-public catppuccin-imv
  (file-append catppuccin-imv-theme
               (string-append "/share/catppuccin/imv/"
                              catppuccin-theme-variant ".config")))

(define-public catppuccin-kdeglobals
  (file-append catppuccin-kde-theme
               (string-append "/share/color-schemes/Catppuccin"
                              (string-upcase catppuccin-theme-variant 0 1)
                              ".colors")))

(define-public catppuccin-kitty-diff
  (file-append catppuccin-kitty-theme
               (string-append "/share/catppuccin/kitty/diff-"
                              catppuccin-theme-variant ".conf")))

(define-public catppuccin-kitty
  (file-append catppuccin-kitty-theme
               (string-append "/share/catppuccin/kitty/"
                              catppuccin-theme-variant ".conf")))

(define-public catppuccin-mpv
  (file-append catppuccin-mpv-theme
               (string-append "/share/catppuccin/mpv/"
                              catppuccin-theme-variant "/"
                              catppuccin-theme-accent ".conf")))

(define-public catppuccin-obs
  (file-append catppuccin-obs-theme "/share/catppuccin/obs"))

(define-public catppuccin-polybar
  (file-append catppuccin-polybar-theme
               (string-append "/share/catppuccin/polybar/"
                              catppuccin-theme-variant ".ini")))

(define-public catppuccin-rofi
  (computed-file "catppuccin.rasi"
    (with-imported-modules (source-module-closure
                            '((guix build utils)))
      #~(begin
          (use-modules (guix build utils))
          (copy-file #$(file-append catppuccin-rofi-theme
                                    (string-append
                                     "/share/catppuccin/rofi/catppuccin-"
                                     catppuccin-theme-variant ".rasi"))
                     #$output)
          (substitute* #$output
            (("JetBrainsMono Nerd Font 14") "Fira Sans 12")            ; default font
            (("border-col: #[0-9a-f]*;") "border-col: #585b70;"))))))  ; i3 border colour

(define-public catppuccin-swaylock
  (file-append catppuccin-swaylock-theme
               (string-append "/share/catppuccin/swaylock/"
                              catppuccin-theme-variant ".conf")))

(define-public catppuccin-vim
  (file-append catppuccin-vim-theme
               (string-append "/share/catppuccin/vim/colors/catppuccin_"
                              catppuccin-theme-variant ".vim")))

(define-public catppuccin-waybar
  (file-append catppuccin-waybar-theme
               (string-append "/share/catppuccin/waybar/"
                              catppuccin-theme-variant ".css")))

(define-public catppuccin-zathura
  (file-append catppuccin-zathura-theme
               (string-append "/share/catppuccin/zathura/catppuccin-"
                              catppuccin-theme-variant)))

(define-public catppuccin-qt5ct.conf
  (mixed-text-file "qt5ct.conf" "\
[Appearance]
color_scheme_path=" catppuccin-qt5ct-theme "/share/qt5ct/colors/Catppuccin-Mocha.conf
custom_palette=true
icon_theme=Papirus-Dark
standard_dialogs=default
style=Fusion

[Fonts]
fixed=\"Hermit,10,-1,5,50,0,0,0,0,0,Regular\"
general=\"Fira Sans,10,-1,5,50,0,0,0,0,0,Regular\"

[Interface]
activate_item_on_single_click=1
buttonbox_layout=3
cursor_flash_time=1000
dialog_buttons_have_icons=2
double_click_interval=400
gui_effects=General, FadeMenu, AnimateCombo, FadeTooltip, AnimateToolBox
keyboard_scheme=2
menus_have_icons=true
show_shortcuts_in_context_menus=true
stylesheets=@Invalid()
toolbutton_style=4
underline_shortcut=2
wheel_scroll_lines=3
"))
