(define-module (tw gexp)
  #:use-module (gnu packages guile)
  #:use-module (guix gexp)
  #:use-module (guix modules))

;; See also: `file-join' in (gnu home services ssh).
(define-public (combined-text-file name . files)
  "A file which is the concatenation of the contents of other files."
  ;; We need to use #$output for `computed-file' to work.
  ;; Of course this isn't documented anywhere!
  (computed-file name
    (with-imported-modules (source-module-closure
                            '((guix build utils)))  ; for `dump-port'
      #~(begin
          (use-modules (guix build utils))
          (call-with-output-file #$output
            (lambda (oport)
              (for-each (lambda (in-file)
                          (call-with-input-file in-file
                            (lambda (iport)
                              (dump-port iport oport))))
                        '#$files)))))))

(define-public (json-file name value)
  "A file called NAME containing the JSON-serialised VALUE."
  (computed-file name
    (with-extensions (list guile-json-4)
      #~(call-with-output-file #$output
          (lambda (port)
            ((@ (json) scm->json) '#$value port #:unicode #t))
          #:encoding "UTF-8"))))
