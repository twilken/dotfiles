#!/usr/bin/env -S bash -e

enc () {
  # Overwriting an existing encrypted file will change its content, even if
  # its decrypted content does not change, so leave it alone.
  local destination=$1; shift
  [ -e "$destination" ] && return 0
  local age_recipients=()
  while [ $# -gt 0 ]; do
    recipient=$1; shift
    hostkey=$(awk '$1 ~ /^\[?'"$recipient"'\.(twilken\.net|local|fritz\.box)\]?(:[0-9]+)?$/ && $2 == "ssh-ed25519" { print $2, $3 }' ~/.ssh/known_hosts)
    age_recipients+=(-r "${hostkey:?No hostkey found for $recipient}")
  done
  mkdir -p "$(dirname "$destination")"
  age -e -a -o "$destination" "${age_recipients[@]}"
}

store () {
  local recipient=$1 key=$2 destination=$3
  # Overwriting an existing encrypted file will change its content, even if
  # its decrypted content does not change, so leave it alone.
  [ -e "$destination" ] && return 0
  pass show "$key" > /dev/null || return 1
  pass show "$key" | head -1 | enc "$destination" "$recipient"
}

store_restic () {
  store "$1" "computers/vin/restic-repos/$2" "tw/system/files/restic/$2.enc"
}

# Wireguard secret keys and pre-shared keys.
for host in lap lud vin frm btl; do
  store "$host" "computers/wireguard/private/$host" "tw/services/files/wireguard/$host.key.enc"
  for host2 in lap lud vin pi3 fp4 frm btl; do
    [ $host = $host2 ] && continue
    store "$host" "computers/wireguard/preshared/$host-$host2" "tw/services/files/wireguard/$host-$host2.psk.enc"
  done
done

# Restic repository passwords.
for repo in timo-laptop timo-framework timo-battleship timo-phone timo-sync; do
  store_restic vin $repo
done

store_restic lud lud-paperless
store_restic lud lud-nextcloud
store_restic lud lud-minecraft
store_restic lud lud-grafana
store_restic lud lud-git
store_restic lud lud-electricity-conso-db

enc tw/system/files/mythic-dns.scm.enc lud << EOF
"$(pass show www/mythic-beasts/dns-lud | sed -rn '/^username: /s///p')"  ; username
"$(pass show www/mythic-beasts/dns-lud | head -1)"  ; password
EOF

enc tw/system/files/grafana-metrics-credentials.enc lud << EOF
GF_METRICS_BASIC_AUTH_USERNAME=$(pass show computers/lud/grafana/metrics | sed -rn '/^username: /s///p')
GF_METRICS_BASIC_AUTH_PASSWORD=$(pass show computers/lud/grafana/metrics | head -1)
EOF

enc tw/system/files/paperless-secret-key.enc lud << EOF
PAPERLESS_SECRET_KEY=$(pass show computers/lud/paperless/secret-key | head -1)
EOF

enc tw/services/files/conso.json.enc lud << EOF
{"prm": "$(pass www/conso-api | sed -rn '/^prm: /s///p')", "api-token": "$(pass www/conso-api | head -1)"}
EOF

enc tw/services/files/nullmailer-remotes.enc lud vin << EOF
# https://www.mythic-beasts.com/support/hosting/mail/client#outgoing
smtp-auth.mythic-beasts.com smtp port=587 starttls \
  user='$(pass www/mythic-beasts/email/cron | sed -rn '/^username: /s///p')' \
  pass='$(pass www/mythic-beasts/email/cron | head -1)'
EOF
