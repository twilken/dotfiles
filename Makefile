.PHONY: secrets test tw/system/%.test tw/home/%.test images tw/system/installer/%.image

systems = $(wildcard tw/system/*.scm)
homes = $(wildcard tw/home/*.scm)
installers = $(wildcard tw/system/installer/*.scm)

test: $(homes:.scm=.test) $(systems:.scm=.test)
images: $(installers:.scm=.image)

tw/home/%.test: tw/home/%.scm
	guix home build -nL $(CURDIR) $<

tw/system/%.test: tw/system/%.scm
	guix system build -nL $(CURDIR) $<

tw/system/installer/%.image: tw/system/installer/%.scm
	guix system image -L $(CURDIR) -t iso9660 --label=GUIX_$$(date -Idate) $<

secrets: regenerate-secrets.sh
	$(CURDIR)/$<
