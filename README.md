# Timo's Dotfiles

## Setting up IceCat

In addition to customising any settings in `about:preferences`, set the following in `about:config`:

- `dom.webnotifications.enabled` to `true`, to enable desktop notifications
- [`dom.event.clipboardevents.enabled` to `true`](https://support.mozilla.org/en-US/questions/1119481), to fix copy-pasting in Google Docs
- `network.negotiate-auth.trusted-uris` to `cern.ch`, for Kerberos log-in support in CERN SSO
- [`identity.fxaccounts.enabled` to `true` and temporarily, `privacy.resistFingerprinting` to `false`](https://old.reddit.com/r/GUIX/comments/eqn3ey/) to use Firefox Sync ([log in here](https://accounts.firefox.com/signin?service=sync&context=fx_desktop_v3&entrypoint=menupanel))
